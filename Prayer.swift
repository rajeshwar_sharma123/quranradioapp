//
//  Prayer.swift
//  QuranRadio
//
//  Created by Daffomacbook-25 on 23/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation


class Prayer{
    
    let prayerName:String!
    var PrayerTime:String!
    var prayerTime24:String?
    
    
    init(name:String, time:String){
        self.PrayerTime = time
        self.prayerName = name
        self.prayerTime24 = ""
    }
    
    
    init(){
        prayerName = ""
        PrayerTime = ""
        prayerTime24 = ""
    }
    
   
}