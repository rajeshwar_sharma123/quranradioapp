//
//  QRAzkarCategoryModel.swift
//  QuranRadio
//
//  Created by Jenkins_New on 6/24/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRAzkarCategoryModel: NSObject {

    var id: String?
    var category_en: String?
    var category_ar: String?
    
    init(fromDictionary dict: NSDictionary)
    {
        id = dict["id"] as? String
        category_en = dict["azkar_category_en"] as? String
        category_ar = dict["azkar_category_ar"] as? String
    }
}
