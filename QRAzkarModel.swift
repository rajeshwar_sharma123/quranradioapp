//
//  QRAzkarModel.swift
//  QuranRadio
//
//  Created by Jenkins_New on 6/24/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRAzkarModel: NSObject {

    var id: String?
    var category_en: String?
    var category_ar: String?
    var start: String?
    var dua: String?
    var benifits_en: String?
    var benifits_ar: String?
    var count: Int?
    var audioUrl: String?
    var azkar_benifits_details_en: String?
    var azkar_benifits_details_ar: String?
    
    //
    
    init(fromDictionary dict: NSDictionary)
    {
        id = dict["id"] as? String
        category_en = dict["azkar_category_en"] as? String
        category_ar = dict["azkar_category_ar"] as? String
        start = dict["azkar_start"] as? String
        dua = dict["azkar_dua"] as? String
        benifits_en = dict["azkar_benifits_en"] as? String
        benifits_ar = dict["azkar_benifits_ar"] as? String
        
        azkar_benifits_details_en = dict["azkar_benifits_details_en"] as? String
        azkar_benifits_details_ar = dict["azkar_benifits_details_ar"] as? String

        
        count = Int( dict["azkar_count"] as! String)
        audioUrl = dict["azkar_audio"] as? String
        
    }
}


