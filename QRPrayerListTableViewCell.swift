//
//  QRPrayerListTableViewCell.swift
//  QuranRadio
//
//  Created by Daffomacbook-25 on 23/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit


protocol prayerNotificationDelegate {
    func prayerAlamTapped(index:Int)
}


class QRPrayerListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPrayerName: UILabel!
    @IBOutlet weak var btnPrayerAlarm: UIButton!
    @IBOutlet weak var lblPrayerTime: UILabel!
    
    var delegate:prayerNotificationDelegate!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func resetCell() {
        btnPrayerAlarm.tag = -1
       
    }
    
    
    @IBAction func prayerAlarmAction(sender: UIButton) {
        
        
        self.delegate.prayerAlamTapped(sender.tag)
    }
    
    
    func setCell(name:String, time:String, index: Int, isAlarmed:Bool, isBold:Bool){
        resetCell()
        
        if !isBold{
            self.lblPrayerName.text = Localization(name)
            self.lblPrayerTime.text = get12HourFormat(getDateFromTime(time))
            self.btnPrayerAlarm.tag = index
            
            if isAlarmed {
                self.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_red"), forState: .Normal)
            } else {
                self.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_grey"), forState: .Normal)
            }
        }else{
            
            if isPad{
                var myMutableString = NSMutableAttributedString()
                
                myMutableString = NSMutableAttributedString(
                    string: Localization(name),
                    attributes: [NSFontAttributeName:UIFont(name: "ProximaNova-Bold", size: 20)!])
                
                self.lblPrayerName.attributedText = myMutableString
                
                myMutableString = NSMutableAttributedString(
                    string: get12HourFormat(getDateFromTime(time)),
                    attributes: [NSFontAttributeName:UIFont(name: "ProximaNova-Bold", size: 20)!])
                
                self.lblPrayerTime.attributedText = myMutableString
                self.btnPrayerAlarm.tag = index
                
                if isAlarmed {
                    self.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_red"), forState: .Normal)
                } else {
                    self.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_grey"), forState: .Normal)
                }
            }else{
                var myMutableString = NSMutableAttributedString()
                
                myMutableString = NSMutableAttributedString(
                    string: Localization(name),
                    attributes: [NSFontAttributeName:UIFont(name: "ProximaNova-Bold", size: 17)!])
                
                self.lblPrayerName.attributedText = myMutableString
                
                myMutableString = NSMutableAttributedString(
                    string: get12HourFormat(getDateFromTime(time)),
                    attributes: [NSFontAttributeName:UIFont(name: "ProximaNova-Bold", size: 17)!])
                
                self.lblPrayerTime.attributedText = myMutableString
                self.btnPrayerAlarm.tag = index
                
                if isAlarmed {
                    self.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_red"), forState: .Normal)
                } else {
                    self.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_grey"), forState: .Normal)
                }
            }
            
            
            
        }
        
        
        
        
    }
    
    func boldFontForLabel(label: UILabel) {
        let currentFont: UIFont = label.font
        print(currentFont.pointSize)
        let newFont: UIFont = UIFont(name: "ProximaNova-Bold", size: 30)!
        label.font = newFont
    }
    
    func getDateFromTime(timeString:String)->NSDate{
        
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MM yyyy"
         dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        let currentDateString = dateFormatter.stringFromDate(NSDate())
        print(currentDateString)
        let dateString = "\(currentDateString) \(timeString)"
        print(dateString)
        dateFormatter.dateFormat = "dd MM yyyy hh:mm a"
         dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        print(dateFormatter.dateFromString(dateString))
        return dateFormatter.dateFromString(dateString)!
    }
    
    func get12HourFormat(date:NSDate)->String{
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "hh:mm a"
         dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        dateFormatter.AMSymbol = "AM"
        dateFormatter.PMSymbol = "PM"
        let currentDateString = dateFormatter.stringFromDate(date)
        print(currentDateString)
        return currentDateString
        
    }
}
