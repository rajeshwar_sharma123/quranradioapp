//
//  AppDelegate.swift
//  QuranRadio
//
//  Created by daffolapmac on 19/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//  Pradeep commit in Dev

import UIKit
import GoogleMaps
import MediaPlayer
import Fabric
import Crashlytics
import AVFoundation
import MediaPlayer

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var currentProofCallback: (Bool -> ())?
    var audioPlayer : AVAudioPlayer!
   
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        setDefaultLanguage()

        Fabric.with([Crashlytics.self])

        sleep(2)
        let notificationSettings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
        if NSUserDefaults.standardUserDefaults().objectForKey("switchValue") == nil{
             NSUserDefaults.standardUserDefaults().setObject("1", forKey: "switchValue")
        }
        

        
        self.setUpAudioForLockScreen()
        if let launchOptions = launchOptions, notification = launchOptions[UIApplicationLaunchOptionsLocalNotificationKey] {
            print("Launched from notification: \(notification)")
        }
        
        
        for fontFamily in UIFont.familyNames() {
            
            print("Font family name = \(fontFamily as String)");
            
            for fontName in UIFont.fontNamesForFamilyName(fontFamily as String) {
                
                print("- Font name = \(fontName)");
                
            }
            
            print("\n");
            
        }
        //CPDFontAM
        GMSServices.provideAPIKey(Google_API_Key)
    
        if #available(iOS 9.0, *) {
            UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.ForceLeftToRight
        } else {
            // Fallback on earlier versions
        }
        
        
        ProgramScheduleManager.deletePastProgramScheduleFromDataBase()
        setRootViewController()
        
        let notification = launchOptions?[UIApplicationLaunchOptionsLocalNotificationKey] as! UILocalNotification!
        if (notification != nil) {
            // Do your stuff with notification
            let dict = notification.userInfo! as NSDictionary
            
            if dict.valueForKey("tag") as! Int == 1 {
               NSUserDefaults.standardUserDefaults().setObject("1", forKey: "tag")
                
            }
    }

        return true
    }
    
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        let dict = notification.userInfo! as NSDictionary
        
        if  application.applicationState == .Active {
            if dict.valueForKey("tag") as! Int != 3 {
                if dict.valueForKey("tag") as! Int == 1 {
               //It plays default system sound
                   AudioServicesPlaySystemSound(1315)

                }else if dict.valueForKey("tag") as! Int == 2 {
                    self.playAzanAudio(notification.soundName!.componentsSeparatedByString(".")[0])
                }
                

                 showAlertWithMessage("", andTitle: Localization(dict.valueForKey("prayerName") as! String))
                
            }
      }
        
        if dict.valueForKey("tag") != nil{
        
        if dict.valueForKey("tag") as! Int == 1 {
          print("3 mnt before")
          switchToViewController()
        }else if dict.valueForKey("tag") as! Int == 3 {
            removeProgramScheduleNotificationFromDataBase(dict.valueForKey("dayId") as! Int, programmeID: dict.valueForKey("programmeId") as! Int)
            
            }
        
        }
        
       
    }

    
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        let controller = QRUtilities.getCurrentViewController().navigationController?.viewControllers
        
        for con in controller! {
            if con.isKindOfClass(QRHomeViewController){
                for view in con.view.subviews{
                    if view.isKindOfClass(QRAudioStreamView){
                        view.becomeFirstResponder()
                        break
                    }
                }
            }
        }
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if QRAudioStreamView.sharedInstanceQRAudioStreamView().isPlaying{
            QRAudioStreamView.sharedInstanceQRAudioStreamView().playRadio()
        }else{
            QRAudioStreamView.sharedInstanceQRAudioStreamView().pauseRadio()
        }
        
        
        ProgramScheduleManager.deletePastProgramScheduleFromDataBase()
        if (UserDefault.objectForKey("k_song_status") != nil){
            
            if UserDefault.objectForKey("k_song_status") as! Bool{
                QRAudioPlayer.sharedInstance.play()
            }
            else{
                QRAudioPlayer.sharedInstance.pause()
            }
            
            
        }
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func setRootViewController(){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        // rootViewController
        let homeViewController = mainStoryboard.instantiateViewControllerWithIdentifier("QRHomeViewController") as! QRHomeViewController
        
        // navigationController
        let navigationController = UINavigationController(rootViewController: homeViewController)
        
        //navigationController.navigationBarHidden = true // or not, your choice.
        
        // self.window
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        self.window!.rootViewController = navigationController
        
        self.window!.makeKeyAndVisible()
    }
       
    /**
     SetUp to play audio in background
     */
    func setUpAudioForLockScreen()
    {
        let audioSession = AVAudioSession.sharedInstance()
        
        do
        {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setActive(true)
        }
        catch
        {
            print(error)
        }
        
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        
        let albumArt = MPMediaItemArtwork(image: UIImage(named: "logo")!)
        
        let infoCenter = MPNowPlayingInfoCenter.defaultCenter()
        infoCenter.nowPlayingInfo = [
            MPMediaItemPropertyTitle:"Quran Radio",
            MPMediaItemPropertyArtwork: albumArt,
            MPNowPlayingInfoPropertyPlaybackQueueCount: NSNumber(integer: 0)
        ]
        
        
    }
    
    
  
    func switchToViewController() {
        if !QRUtilities.getCurrentViewController().isKindOfClass(QRGooglePlacesViewController){
            if window != nil {
                let navigationController: UINavigationController = (self.window!.rootViewController as! UINavigationController)
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let controller: QRGooglePlacesViewController = (mainStoryboard.instantiateViewControllerWithIdentifier("QRGooglePlacesViewController") as! QRGooglePlacesViewController)
                navigationController.pushViewController(controller, animated: false)
            }
        }
    }
    
    
    func showAlertWithMessage(message:String, andTitle title:String ){
        
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: Localization("Ok"), style: UIAlertActionStyle.Default)
        { action -> Void in
            // Put your code here
            if self.audioPlayer != nil{
            self.audioPlayer.stop()
            }
    
        })
        
        
        QRUtilities.getCurrentViewController().presentViewController(alert, animated: true, completion: nil)
        
    }
 
    func removeProgramScheduleNotificationFromDataBase(dayId: Int, programmeID: Int) {
        
        ProgramScheduleManager.removeProgramSchedule(dayId, programmeID: programmeID)
        
        
    }
    //Function to play azan sound when app is in Active State
    func playAzanAudio(soundName:String)
    {
        do
        {
            let url = NSBundle.mainBundle().URLForResource(soundName, withExtension: "wav")
            let audioData = NSData(contentsOfURL: url!)
            if(url != nil)
            {
                try audioPlayer = AVAudioPlayer(data: audioData!)
                audioPlayer.play()
                
            }
            
        }
        catch
        {
            print(error)
        }
    }
    
   func setDefaultLanguage()
   {
    let languages = Localisator.sharedInstance.getArrayAvailableLanguages()
    
    let currentLanguage =  QRUtilities.getPrefferedLanguage()
    if(currentLanguage == "")
    {
        SetLanguage(languages[2])
        QRUtilities.setPrefferedLanguage(Arabic)
        return
    }
    }
}

