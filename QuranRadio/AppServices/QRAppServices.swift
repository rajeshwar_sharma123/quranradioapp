//
//  QRAppServices.swift
//  QuranRadio
//
//  Created by daffolapmac on 20/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
import Alamofire
import GoogleMaps
class QRAppServices {
    

class func getServiceRequest(
        urlString urlString: String ,
                  successBlock:(responseData: AnyObject)->(),
                  errorBlock:(errorMessage: NSError)->()
        )
    {
        let request = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        request.HTTPMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        Alamofire.request(request)
            .responseJSON { response in
                
                switch response.result {
                    
                case .Failure(let error):
                    
                    errorBlock(errorMessage: error)
                    
                case .Success(let responseObject):
                    
                   print(responseObject)
                    
                    successBlock(responseData: responseObject)
                }
                
        }
        
    }
    
    
    class func performPostRequest(
        urlString: String,
        requestData: AnyObject,
        successBlock:(request: AnyObject?, response: AnyObject?, headers:AnyObject? )->(),
        errorBlock:(errorMessage :String,response :AnyObject?, headers:AnyObject?)->()
        )
    {
        Alamofire.request(.POST , urlString, parameters: requestData as? [String : AnyObject])
            .responseJSON() { response in
                
                if(response.response?.statusCode >= 200 && response.response?.statusCode <= 204 && response.result.isSuccess)
                {
                    let responseData = response.result.value
                    let headersData = response.response!.allHeaderFields as NSDictionary
                    successBlock(request:response.request, response: responseData, headers:headersData)
                }
                else if(response.result.isFailure)
                {
                    let msg = ErrorUtility.getErrorMessageForResponse(response.response, error: response.result.error)
                    
                    errorBlock(errorMessage: msg, response: response.response, headers: response.response?.allHeaderFields)
                }
                
        }
    }
    
    
    class func performGetRequest
        (
        serviceURL: String,
        requestData:[String : AnyObject],
        successBlock :(response :AnyObject, headers:AnyObject)->Void,
        errorBlock:(errorMessage :String,response :AnyObject?, headers:AnyObject?)->Void
        ){
        Alamofire.request(.GET,serviceURL,parameters:requestData)
            .responseJSON() { response in
                if(response.response?.statusCode >= 200 && response.response?.statusCode <= 204&&response.result.isSuccess){
                    let responseData = response.result.value
                    let headersData = response.response!.allHeaderFields
                    successBlock(response: responseData!, headers:headersData)
                }
                else if(response.result.isFailure)
                {
                    let msg = ErrorUtility.getErrorMessageForResponse(response.response, error: response.result.error)
                    
                    errorBlock(errorMessage: msg, response: response.response, headers: response.response?.allHeaderFields)
                }
                
        }
        
    }
    
    class func performGetRequestWithoutParameter(
        serviceURL: String,
        successBlock :(response :AnyObject, headers:AnyObject)->Void,
        errorBlock:(errorMessage :String,response :AnyObject?, headers:AnyObject?)->Void
        ){
        Alamofire.request(.GET,serviceURL)
            .responseJSON() { response in
                if(response.response?.statusCode >= 200 && response.response?.statusCode <= 204&&response.result.isSuccess){
                    let responseData = response.result.value
                    let headersData = response.response!.allHeaderFields
                    
                    successBlock(response: responseData!, headers:headersData)
                }
                else if(response.result.isFailure)
                {
                    let msg = ErrorUtility.getErrorMessageForResponse(response.response, error: response.result.error)
                    
                    errorBlock(errorMessage: msg, response: response.response, headers: response.response?.allHeaderFields)
                }
                
        }
    }
    
    
    
    
    
    
    
    
    
    

    
    
    class func getPlacesWithLocation(location:CLLocation,placeType:String,radius:NSInteger,
                  successBlock:(responseData: AnyObject)->(),
                  errorBlock:(errorMessage: NSError)->()
        )
    {
        
        let urlStr :String = "\(kPlaceUrl)location=\(location.coordinate.latitude),\(location.coordinate.longitude)&radius=\(radius)&type=\(placeType)&key=\(Google_Browser_Key)"
        let request = NSMutableURLRequest(URL: NSURL(string: urlStr)!)
        request.HTTPMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        Alamofire.request(request)
            .responseJSON { response in
                
                switch response.result {
                    
                case .Failure(let error):
                    
                    errorBlock(errorMessage: error)
                    
                case .Success(let responseObject):
                    
                    print(responseObject)
                    
                    successBlock(responseData: responseObject)
                }
                
        }
        
    }

    
}