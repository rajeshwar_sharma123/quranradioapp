//
//  QRRadioProgramModel.swift
//  QuranRadio
//
//  Created by daffolapmac on 17/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation

import UIKit

class QRRadioProgramModel: NSObject
    
{
    
    var programNameEnglish : String = ""
    var programNameArabic : String = ""
    var programPicture : String = ""
    var program_archive_id: String = ""
    
     init(fromDictionary dict: NSDictionary)
        
    {
        
        program_archive_id = dict["program_archive_id"] as! String
        programNameEnglish = dict["program_name_en"] as! String
        programNameArabic = dict["program_name_ar"] as! String
        programPicture = dict["program_picture"] as! String
        
        
    }
    
}