//
//  UIScrollView.swift
//  QuranRadio
//
//  Created by daffolapmac on 01/07/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
import UIKit
extension UIScrollView {
    var currentPage:Int{
        return Int((self.contentOffset.x+(0.5*self.frame.size.width))/self.frame.width)+1
    }
}