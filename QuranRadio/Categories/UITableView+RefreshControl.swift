//
//  UITableView+RefreshControl.swift
//  AlOula
//
//  Created by Vishwas Singh on 20/04/16.
//  Copyright © 2016 Daffodil iPhone. All rights reserved.
//

import Foundation

extension UITableView
{
    private struct AssociatedKeys {
        static var refreshControl:UIRefreshControl?
    }
    
    var refreshControl: UIRefreshControl? {
        get
        {
            return objc_getAssociatedObject(self, &AssociatedKeys.refreshControl) as? UIRefreshControl
        }
        set
        {
            if let newValue = newValue
            {
                objc_setAssociatedObject(self, &AssociatedKeys.refreshControl, newValue as UIRefreshControl?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    
    
    
    func enablePullToRefreshWithTarget(target: AnyObject, selector: Selector)
    {
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(target, action: selector, forControlEvents: .ValueChanged)
       
        self.addSubview(self.refreshControl!)
    }
    func reomveEmptyRows(){
        tableFooterView = UIView()
    }
}


