//
//  UIViewController+Utility.swift
//  QuranRadio
//
//  Created by daffolapmac on 25/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
import UIKit
import JKNotificationPanel

extension UIViewController{
    
    func showErrorMessage(message :String)
    {
        let panel = JKNotificationPanel()
        let view = panel.defaultView(.FAILED, message: message)
        view.setColor(UIColor.redColor())
        panel.showNotify(withView: view, belowNavigation: self.navigationController!)
        
    }
   
    func showSuccessMessage(message :String)
    {
        JKNotificationPanel().showNotify(withStatus: .SUCCESS, belowNavigation: self.navigationController!, message: message)
    }
    
    func setNavigationBarColor(){
        navigationController!.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        navigationController!.navigationBar.shadowImage = UIImage()
        navigationController!.navigationBar.translucent = true
        navigationController!.view.backgroundColor = UIColor.whiteColor()
    }
    
    func showAlertWithMessage(message:String, andTitle title:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
//        var ok = "Ok"
//        if(QRUtilities.getPrefferedLanguage() == Arabic)
//        {
//            ok = "حسنا"
//        }
        
        alert.addAction(UIAlertAction(title: Localization("Ok"), style: UIAlertActionStyle.Default, handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }

}