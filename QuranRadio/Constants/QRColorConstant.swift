//
//  QRColorConstant.swift
//  QuranRadio
//
//  Created by daffolapmac on 25/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
import UIKit
func ColorFromHexaCode(rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

//************************ Hexa Color Code ***************


let k_ContactNumRedColor                 = 0xC41632 as UInt
let k_Red_Color                          = 0xC41632 as UInt
let k_Bright_Red_Color                   = 0xB50026 as UInt
let k_Slider_Track_Color                 = 0x92182B as UInt
let k_Slider_Progress_Color              = 0x401C20 as UInt 






