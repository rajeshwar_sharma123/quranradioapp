//
//  QRConfig.swift
//  QuranRadio
//
//  Created by daffolapmac on 20/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation


//**************** Base Url *********************************
//Base Url
let k_Base_Url = "http://flagshippro.com/apps/dubai_quraan/API"


//End points
let Url_Pdf_link  = "http://flagshippro.com/apps/dubai_quraan/API/quraanLinkPDF"
let Url_Program_Schedule = "/schedules"
let k_Url_Archive_List = "/archives"
let k_Url_Program_Archive_List = "/program_archives"

let Url_Azkar_List = "\(k_Base_Url)/azkar"
let Url_Azkar_Categories = "\(k_Base_Url)/azkar_categories"



// *******************************  Basic Config Constants ****************************
let kPlaceUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
let kRadius = 5000
let Google_API_Key = "AIzaSyBYEQ40r1zCWKCT8RSz620jf9eWOPOu5pI"
let Google_Browser_Key = "AIzaSyDxHWPxE9wLGc6QIyzJkYAqLt1R2pFbLXc"





let LAST_DATE_FETCHED_PRAYER_TIME = "lastDateFetchedData"
let kError_NoInternetConnection = "الرجاء التحقق من اتصال الإنترنت الخاص بك !"

