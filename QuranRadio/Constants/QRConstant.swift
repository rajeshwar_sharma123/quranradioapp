//
//  QRConstant.swift
//  QuranRadio
//
//  Created by daffolapmac on 25/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
import UIKit

// ***********************************  Basic String Constants ****************************************

let UserDefault = NSUserDefaults.standardUserDefaults()
let Language = "Language"
let English = "English"
let Arabic = "Arabic"
let k_Song_Duration  = "SongDuration"

// **********************************  Check for Device ************************



let deviceIdiom = UIScreen.mainScreen().traitCollection.userInterfaceIdiom
let screenWidth = UIScreen.mainScreen().bounds.width
let screenHeight = UIScreen.mainScreen().bounds.height
let isPhone = UIDevice.currentDevice().userInterfaceIdiom == .Phone
let isPad = UIDevice.currentDevice().userInterfaceIdiom == .Pad

let kPrefferedLanguage = 2 as Int
let kEnglish = 1 as Int
let kArabic = 2 as Int



