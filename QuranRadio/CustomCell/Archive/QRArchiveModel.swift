//
//  QRArchiveModel.swift
//  QuranRadio
//
//  Created by daffolapmac on 17/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//


import Foundation
import UIKit
class QRArchiveModel: NSObject
{
    var audioboom_id : String = ""
    var title : String = ""
    var date : String = ""
    var mp3 : String = ""
    var duration : String = ""
    var wave_img : String = ""
    var program_archive_id : String = ""
    
    init(fromDictionary dict: NSDictionary)
    {
        
        audioboom_id = dict["audioboom_id"] as! String
        title = dict["title"] as! String
        date = dict["date"] as! String
        mp3 = dict["mp3"] as! String
        wave_img = dict["wave_img"] as! String
        duration = dict["duration"] as! String
        program_archive_id = dict["program_archive_id"] as! String
        
    }
}
