//
//  QRArchiveTableViewCell.swift
//  QuranRadio
//
//  Created by daffolapmac on 17/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRArchiveTableViewCell: UITableViewCell {

    @IBOutlet weak var programPicture: UIImageView!
    @IBOutlet weak var programName: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        self.programPicture.layer.cornerRadius = 10.0
        self.programPicture.layer.masksToBounds = true
        //self.presenterName.textColor = UIColor.redColor()
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
