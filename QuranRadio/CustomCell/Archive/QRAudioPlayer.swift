//
//  QRAudioPlayer.swift
//  QuranRadio
//
//  Created by daffolapmac on 17/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import AVFoundation

class QRAudioPlayer: NSObject
{
    var player: AVPlayer!
    var timer:NSTimer!
    var playerItem: AVPlayerItem!
    class var sharedInstance: QRAudioPlayer {
        struct Static {
            static let instance = QRAudioPlayer()
        }
        return Static.instance
    }
    
    
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    //MARK:-  Function to play audio from url
    
    func playAudioWithParentViewController(parentVC: UIViewController, cell : UITableViewCell, urlForSong : String)
    {
        parentVC.view.showLoaderWithMessage("Buffering...")
        
        //AOUtilities.sharedInstanceAOFooterView().pauseRadio()
        QRAudioStreamView.sharedInstanceQRAudioStreamView().pauseRadio()
        if(self.player != nil)
        {
            self.player.pause()
            self.player = nil
        }
        
        let url = urlForSong.urlByAddingPercentEncoding()
        let asset: AVURLAsset = AVURLAsset(URL: url, options: nil)
        let keys: [String] = ["playable"]
        asset.loadValuesAsynchronouslyForKeys(keys, completionHandler: {() -> Void in
            
            self.playerItem = AVPlayerItem.init(asset: asset)
            self.player = AVPlayer(playerItem: self.playerItem)
            self.play()
            
            self.player.addPeriodicTimeObserverForInterval(CMTime(value: 1, timescale: 3), queue: nil, usingBlock: { (time) -> Void in
                
                if((self.player?.currentItem?.asset.duration.isValid) != nil)
                {
                    parentVC.view.hideLoader()
                }
            })
            
            
            NSNotificationCenter.defaultCenter().addObserver(parentVC, selector: Selector("chkPlaying:"), name: AVPlayerItemPlaybackStalledNotification, object: self.playerItem)
            NSNotificationCenter.defaultCenter().addObserver(parentVC, selector: Selector("finishedPlaying:"), name: AVPlayerItemDidPlayToEndTimeNotification, object: self.playerItem)
            
           /* if(cell.isKindOfClass(AOTopSongsTableViewCell))
            {
                let topSongsCell  = cell as? AOTopSongsTableViewCell
                topSongsCell?.bindProgressData()
            }
            else */
            if(cell.isKindOfClass(QRProgramsNowCell))
            {
                let programsNowCell  = cell as? QRProgramsNowCell
                programsNowCell?.bindProgressData(programsNowCell!)
            }
            
           

        })
    }
    
    
    
    func play()
    {
        if(!NetworkConnectivity.isConnectedToNetwork())
        {
            QRUtilities.getCurrentViewController().showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
            return
        }
        
        if(self.player != nil)
        {
            self.player.play()
           // AOUtilities.sharedInstanceAOFooterView().pauseRadio()
            QRAudioStreamView.sharedInstanceQRAudioStreamView().pauseRadio()
        }
    }
    
    func pause()
    {
        if(self.player != nil)
        {
            self.player.pause()
           // AOUtilities.sharedInstanceAOFooterView().pauseRadio()
            QRAudioStreamView.sharedInstanceQRAudioStreamView().pauseRadio()
        }
    }
    
    func stop()
    {
        self.player = nil
    }
    
    
}

