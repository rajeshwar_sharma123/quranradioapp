//
//  QRProgramsNowCell.swift
//  QuranRadio
//
//  Created by daffolapmac on 28/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import AVFoundation

protocol QRProgramsNowCellDelegate : NSObjectProtocol {
    func updateSongProgressInfo(progressTime:String,progress:Float)
}


class QRProgramsNowCell: UITableViewCell {

    weak var delegate: QRProgramsNowCellDelegate?
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    weak var  cell1 : QRProgramsNowCell!
    var checkUpdate = false
    @IBOutlet weak var playButton: UIButton!
    var timer:NSTimer!
    
    @IBOutlet var programNowLabel: UILabel!
    @IBOutlet weak var songProgressLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    var url:String!
    //var indexPath : NSIndexPath!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var songDurationLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        progressBar.transform = CGAffineTransformMakeScale(1, 5)
        progressBar.layer.cornerRadius = 10
        progressBar.clipsToBounds = true
        songDurationLabel.textColor = UIColor.whiteColor()
        songProgressLabel.textColor = UIColor.whiteColor()
        progressBar.trackTintColor = ColorFromHexaCode(k_Slider_Track_Color)
        progressBar.progressTintColor = ColorFromHexaCode(k_Slider_Progress_Color)
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    //MARK:-  Function to update progress
    func bindProgressData(cell:QRProgramsNowCell)
    {
        cell1 = cell
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.timer = NSTimer(timeInterval: 1, target: self, selector: #selector(QRProgramsNowCell.updateTime), userInfo: nil, repeats: true)
            NSRunLoop.mainRunLoop().addTimer(self.timer, forMode: NSRunLoopCommonModes)
            self.songDurationLabel.text = String(self.getTime(Float(CMTimeGetSeconds(QRAudioPlayer.sharedInstance.playerItem.asset.duration))))
            UserDefault.setObject(self.songDurationLabel.text, forKey: k_Song_Duration)
            
        }
    }
    
    //MARK:-  Function to update song progress time
    func updateTime() {
        
        let currentTime = self.getTime(Float(CMTimeGetSeconds(QRAudioPlayer.sharedInstance.playerItem.currentTime())))
        let initial = Int(CMTimeGetSeconds(QRAudioPlayer.sharedInstance.playerItem.currentTime()))
        let maximum = CMTimeGetSeconds(QRAudioPlayer.sharedInstance.playerItem.asset.duration)
        let prog = Float(initial)/Float(maximum)
        progressBar.progress = prog
        songProgressLabel.text = currentTime
        delegate?.updateSongProgressInfo(songProgressLabel.text!,progress: progressBar.progress)
    }
    
    
    
    //MARK:-  Function to convert time to string
    func getTime(timeToConvert: Float)-> String{
        
        if !timeToConvert.isNaN
        {
            let currentTime = Int(timeToConvert)
            let hours = currentTime/(60*60)
            if hours > 0
            {
                let minutes = (currentTime/60)%60
                let seconds = currentTime%60
                let formattedTimeString = NSString(format: "%02d:%02d:%02d", hours,minutes,seconds) as String
                return  formattedTimeString
            }
            else
            {
                
                let minutes = currentTime/60
                let seconds = currentTime - minutes * 60
                let formattedTimeString = NSString(format: "%02d:%02d", minutes,seconds) as String
                return  formattedTimeString
            }
        }
        else
        {
            
            return "00:00"
        }
        
    }
    
    
    
    

}
