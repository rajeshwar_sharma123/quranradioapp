//
//  QRAzkarTableViewCell.swift
//  QuranRadio
//
//  Created by Jenkins_New on 6/24/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRAzkarTableViewCell: UITableViewCell {

    @IBOutlet var infoButton: UIButton!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var duaLabel: UILabel!
    @IBOutlet weak var benefitsLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    
    var azkar: QRAzkarModel?
   // var isPlaying = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func bindDataWithAzkar(azkar: QRAzkarModel)
    {
       
        self.azkar = azkar
       
        startLabel.attributedText = getAttributedText(azkar.start!, lineSpacing: 7)
        
        duaLabel.attributedText = getAttributedText(azkar.dua!, lineSpacing: 7)
        
        countLabel.text = " \(Localization("k_Count")) : \(azkar.count!) "
        
        if(QRUtilities.getPrefferedLanguage() == English)
        {
            benefitsLabel.attributedText =  getAttributedText(azkar.benifits_en!, lineSpacing: 7)
        }
        else
        {
            benefitsLabel.attributedText = getAttributedText(azkar.benifits_ar!, lineSpacing: 7)
        }
        benefitsLabel.adjustsFontSizeToFitWidth = false
        benefitsLabel.lineBreakMode = .ByTruncatingTail
        setLabelAllignment()
    }
    
    func getAttributedText(strTxt:String,lineSpacing:CGFloat)-> NSMutableAttributedString{
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        
        let attrString = NSMutableAttributedString(string: strTxt)
        attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, attrString.length))

        return attrString
    }
    
    func setLabelAllignment(){
        if(QRUtilities.getPrefferedLanguage() == Arabic)
        {
            startLabel.textAlignment = .Right
            duaLabel.textAlignment = .Right
            benefitsLabel.textAlignment = .Right
        }
    }
    //MARK:- IBAction Methods
    @IBAction func downloadButtonTapped(sender: AnyObject)
    {
        let firstActivityItem = "\(startLabel.text!) \n \(duaLabel.text!) \n \(benefitsLabel.text!)"
       
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [firstActivityItem], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = (sender as! UIButton)
        
        QRUtilities.getCurrentViewController().presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func playButtonTapped(sender: AnyObject)
    {
        
    }
    
    
    
}
