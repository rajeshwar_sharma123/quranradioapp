//
//  QRProgramListModel.swift
//  QuranRadio
//
//  Created by daffolapmac on 15/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
class QRProgramListModel: NSObject
{
    var allDayPrograms = [AnyObject]()
    var sundayPrograms = [QRProgramModel]()
    var mondayPrograms = [QRProgramModel]()
    var tuesdayPrograms = [QRProgramModel]()
    var wednesDayPrograms = [QRProgramModel]()
    var thursdayPrograms = [QRProgramModel]()
    var fridayPrograms = [QRProgramModel]()
    var saturdayPrograms = [QRProgramModel]()
    
    init(fromDictionary dict: NSDictionary)
        
    {
        var programs = dict["Sunday"] as! Array<NSDictionary>
        
        for program in programs
            
        {
            sundayPrograms.append(QRProgramModel(fromDictionary: program))
            
        }
        
        
        programs = dict["Monday"] as! Array<NSDictionary>
        
        for program in programs
        {
            
            mondayPrograms.append(QRProgramModel(fromDictionary: program))
            
        }
        
        programs = dict["Tuesday"] as! Array<NSDictionary>
        for program in programs
        {
            tuesdayPrograms.append(QRProgramModel(fromDictionary: program))
        }
        
        
        programs = dict["Wednesday"] as! Array<NSDictionary>
        for program in programs
        {
            wednesDayPrograms.append(QRProgramModel(fromDictionary: program))
        }
        
        
        programs = dict["Thursday"] as! Array<NSDictionary>
        for program in programs
        {
            thursdayPrograms.append(QRProgramModel(fromDictionary: program))
        }
        
        programs = dict["Friday"] as! Array<NSDictionary>
        for program in programs
        {
            fridayPrograms.append(QRProgramModel(fromDictionary: program))
        }
        
        programs = dict["Saturday"] as! Array<NSDictionary>
        for program in programs
        {
            saturdayPrograms.append(QRProgramModel(fromDictionary: program))
        }
        
        allDayPrograms.append(sundayPrograms)
        allDayPrograms.append(mondayPrograms)
        allDayPrograms.append(tuesdayPrograms)
        allDayPrograms.append(wednesDayPrograms)
        allDayPrograms.append(thursdayPrograms)
        allDayPrograms.append(fridayPrograms)
        allDayPrograms.append(saturdayPrograms)
    }
    
}
