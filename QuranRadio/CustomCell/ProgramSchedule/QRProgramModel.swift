//
//  QRProgramModel.swift
//  QuranRadio
//
//  Created by daffolapmac on 15/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
class QRProgramModel: NSObject
{
    var programmeId: Int?
    var programNameArabic: String!
    var programNameEnglish: String!
    var image: String!
    var startTime: String!
    var endTime: String!
    var dayId:Int?
    var presenterName: String?
    var imageUrl: String?
    var notificationDate: NSDate?
    
    override init() {
        
    }
    init(fromDictionary dict: NSDictionary)
    {
        programmeId = Int( dict["program_id"] as! String)
        programNameArabic = dict["program_name_ar"] as! String
        programNameEnglish = dict["program_name_en"] as! String
        image = dict["program_picture"] as! String
        startTime = dict["schedule_start"] as! String
        endTime = dict["schedule_end"] as! String
        
       
    }
}
