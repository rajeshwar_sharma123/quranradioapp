//
//  QRProgramScheduleCell.swift
//  QuranRadio
//
//  Created by daffolapmac on 15/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import MGSwipeTableCell
class QRProgramScheduleCell: UITableViewCell {

    @IBOutlet var btnReminder: UIButton!
    @IBOutlet var lblTime: QRLabel!
    @IBOutlet var lblProgramName: UILabel!
    @IBOutlet var programImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
