//
//  QRSettingsTableViewCell.swift
//  QuranRadio
//
//  Created by Jenkins_New on 6/23/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         if screenWidth != 320{
            var x : CGFloat = 12.0
            if Localisator.sharedInstance.currentLanguage == "English_en"
            {
                x = screenWidth-12-58
            }
            switchButton.translatesAutoresizingMaskIntoConstraints = true
            switchButton.frame = CGRectMake(x, switchButton.frame.origin.y, 58,  26) ;
        }
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   
    func bindDataWithIndexPath(indexPath: NSIndexPath, currentLanguage: String)
    {
        var title = ""
        var imageName = ""
        
        if(indexPath.row == 0)
        {
            if(currentLanguage == English)
            {
                title = "Language"
                imageName = "ic_button_eng"
            }
            else
            {
                title = "اللغات"
                imageName = "ic_button_ara"
            }
        }
        else if(indexPath.row == 1)
        {
           let switchValue = NSUserDefaults.standardUserDefaults().objectForKey("switchValue") as! String
            if(currentLanguage == English)
            {
                title = "Azan Audio"
                if(switchValue == "0")
                {
                    imageName = "ic_button_off"
                }
                else
                {
                    imageName = "ic_button_on"
                }
            }
            else
            {
                title = "صوت الآذان"
                if(switchValue == "0")
                {
                    imageName = "ic_button_off"
                }
                else
                {
                    imageName = "ic_button_on"
                }
            }
        }
        
    
        titleLabel.text = title
        switchButton.tag = indexPath.row
        switchButton.setBackgroundImage(UIImage(named: imageName), forState: .Normal)
    }
    
}
