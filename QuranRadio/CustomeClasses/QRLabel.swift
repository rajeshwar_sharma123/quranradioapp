//
//  QRLabel.swift
//  QuranRadio
//
//  Created by daffolapmac on 26/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRLabel: UILabel {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    @IBInspectable var iPhoneSize:CGFloat = 15 {
        didSet {
            if isPhone() {
                overrideFontSize(iPhoneSize)
            }
        }
    }
    
    @IBInspectable var iPadSize:CGFloat = 25 {
        didSet {
            if isPad() {
                overrideFontSize(iPadSize)
            }
        }
    }
    
    func isPhone() -> Bool {
         return UIDevice.currentDevice().userInterfaceIdiom == .Phone
       
    }
    
    func isPad() -> Bool {
         return UIDevice.currentDevice().userInterfaceIdiom == .Pad
       
    }
    
    func overrideFontSize(fontSize:CGFloat){
        let currentFontName = self.font.fontName
        if let calculatedFont = UIFont(name: currentFontName, size: fontSize) {
            self.font = calculatedFont
        }
    }
   
    
    @IBInspectable var fontFamily: String = "CPDFontAM"
    
    override func awakeFromNib() {
        
        
        if isPhone() {
           self.font = UIFont(name:self.fontFamily, size: self.iPhoneSize)
            
        }
        else{
          
            self.font = UIFont(name:self.fontFamily, size: self.iPadSize)
        }
         
    }

}
