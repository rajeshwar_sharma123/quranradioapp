//
//  TappableSlider.swift
//  SliderDemo
//
//  Created by Daffomac-23 on 7/1/16.
//  Copyright © 2016 Daffodil Software Limited. All rights reserved.
//

import UIKit

class TappableSlider: UISlider {

    
    
    override func drawRect(rect: CGRect) {
        
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: "sliderTapped:")
        self.addGestureRecognizer(tapGesture)
    }
    
    
    override func trackRectForBounds(bounds: CGRect) -> CGRect {
        
        let frame = CGRectMake(bounds.origin.x, bounds.origin.y+13, bounds.size.width,bounds.size.height-22)

        
        return frame
    }
    
    func sliderTapped(gesture: UIGestureRecognizer)
    {
        let slider = gesture.view as! UISlider
        
        if(slider.highlighted)
        {
            return
        }
        
        let point = gesture.locationInView(slider)
        let percentage = Float(point.x / slider.bounds.size.width)
        let delta = percentage * (slider.maximumValue - slider.minimumValue)
        let value = slider.minimumValue + delta;
        
        slider.setValue(value, animated: true)
        
        slider.sendActionsForControlEvents(.ValueChanged)
    }
    
}
