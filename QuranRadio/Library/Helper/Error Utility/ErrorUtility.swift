//
//  ErrorUtility.swift
//  QuranRadio
//
//  Created by Daffomacbook-25 on 20/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation


import UIKit
import Foundation

class ErrorUtility {
    
    static let instance = ErrorUtility();
    class func sharedInstance() -> ErrorUtility
    {
        return instance;
    }
    
    
    
    
    class func getErrorMessageForResponse(response: NSHTTPURLResponse?, error: NSError?)-> String
    {
        var errorCode: Int!
        
        if (response != nil)
        {
            errorCode = response!.statusCode
        }
        else
        {
            errorCode = error!.code;
        }
        
        if (errorCode == -1004) && (error?.userInfo["_kCFStreamErrorDomainKey"]?.longValue == 1) && (error?.userInfo["_kCFStreamErrorCodeKey"]?.integerValue == 51)
        {
            let errorString = Localization("No_Internet_Connection")
            return errorString//ErrorMessage.InternetIssue.rawValue
        }
        
        return self.getErrorMessageForErrorCode(errorCode)
    }
    
    
    class func getErrorMessageForErrorCode(errorcode: Int)-> String
    {
        var errorString = ""
        
        switch (errorcode)
        {
            
        case ErrorCode.BadRequest.rawValue :
            errorString = ErrorMessage.BadRequest.rawValue
            
        case ErrorCode.CannotFindHost.rawValue :
            errorString = ErrorMessage.CannotFindHost.rawValue
            
        case ErrorCode.UnAuthorized.rawValue :
            errorString = ErrorMessage.UnAuthorized.rawValue
            
        case ErrorCode.Forbidden.rawValue :
            errorString = ErrorMessage.Forbidden.rawValue
            
        case ErrorCode.NotFound.rawValue :
            errorString = ErrorMessage.NotFound.rawValue
            
        case ErrorCode.ParameterMissing.rawValue :
            errorString = ErrorMessage.ParameterMissing.rawValue
            
        case ErrorCode.UserAlreadyExists.rawValue :
            errorString = ErrorMessage.UserAlreadyExists.rawValue
            
        case ErrorCode.NotModified.rawValue :
            errorString = ErrorMessage.NotModified.rawValue
            
        case ErrorCode.RequestStopped.rawValue :
            errorString = ErrorMessage.RequestStopped.rawValue
            
        case ErrorCode.NoInternetConnection.rawValue :
            errorString = Localization("No_Internet_Connection")// ErrorMessage.InternetIssue.rawValue
            
        default :
            errorString = Localization("Server_Error")// ErrorMessage.UnkownError.rawValue
            
        }
        
        return errorString;
    }
    
}

enum ErrorCode: Int
{
    
    case BadRequest = 400
    case UnAuthorized = 401
    case Forbidden = 403
    case NotFound = 404
    case ParameterMissing = 406
    case UserAlreadyExists = 409
    case ServerError = 500
    case NotModified = 304
    case NoInternetConnection = -1009
    case CannotFindHost = -1003
    case CannotConnectToHost = -1004
    case NetworkConnectionLost = -1005
    case RequestStopped = -999
}

enum ErrorMessage: String
{
    
    
    case BadRequest = "Bad Request"
    case CannotFindHost = "Cannot find host"
    case UnAuthorized = "UnAuthorized"
    case Forbidden = "Forbidden"
    case NotFound = "Not Found"
    case ParameterMissing = "Parameter Missing"
    case UserAlreadyExists = "User already exists"
    case NotModified = "Not Modified"
    case RequestStopped = "Request Stopped"
    case InternetIssue = "Internet connection error"
    case UnkownError = "Server Error, Please try later"
}
