//
//  QRStringExtension.swift
//  QuranRadio
//
//  Created by Daffomacbook-25 on 20/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
import UIKit

extension String
{
    func getDateFormTimeString() -> String
    {
        
        let dateFormatter = NSDateFormatter()
        let timeSpittedArray = self.characters.split{$0 == ":" || $0 == " "}.map(String.init)
        if timeSpittedArray.count == 2
        {
            dateFormatter.dateFormat = "HH:mm"
             dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        }
        else
        {
            dateFormatter.dateFormat = "HH:mm:ss"
             dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        }
        
        dateFormatter.timeZone = NSTimeZone.systemTimeZone()
        
        let datePrayer = dateFormatter.dateFromString(self)
        
        dateFormatter.dateFormat = "hh:mm a"
         dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        
        let dateStringPrayer = dateFormatter.stringFromDate(datePrayer!)
        
        return dateStringPrayer
        //    let timeSpittedArray = self.characters.split{$0 == ":" || $0 == " "}.map(String.init)
        //    debugPrint("splitted array: \(timeSpittedArray)")
        //    var timeString:String!
        //    if timeSpittedArray.count >= 2
        //    {
        //      var hr = Int(timeSpittedArray[0])!
        //      if(hr > 12)
        //      {
        //        hr = hr - 12
        //        timeString = "\(hr):\(timeSpittedArray[1]) PM"
        //      }
        //      else
        //      {
        //        timeString = "\(hr):\(timeSpittedArray[1]) AM"
        //      }
        //    }
        //    return timeString
    }
    func getMinutesFromTimeString() -> Int
    {
        
        var totalMinutes = 0
        let timeSpittedArray = self.characters.split{$0 == ":" || $0 == " "}.map(String.init)
        debugPrint("splitted array: \(timeSpittedArray)")
        
        if timeSpittedArray.count >= 2
        {
            let hr = Int(timeSpittedArray[0])!
            let min = Int(timeSpittedArray[1])!
            totalMinutes = hr*60 + min
        }
        return totalMinutes
    }
    
    func dynamicSizeForWidth(width: CGFloat, font: UIFont) -> CGSize
    {
        let label: UILabel = UILabel()
        label.numberOfLines = 0
        label.text = self
        label.font = font
        if (self == "")
        {
            // for minimum one line
            label.text = "Test"
        }
        let newSize: CGSize = label.sizeThatFits(CGSizeMake(width, CGFloat.max))
        
        return newSize
    }
    
    func trimmedString()->String
    {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
    
    
    var length: Int{
        return self.characters.count
    }
    
    
    
    func isValidEmail() -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluateWithObject(self)
    }
    func urlByAddingPercentEncoding()-> NSURL
    {
        let urlString = self.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        let url = NSURL(string:urlString)
        
        return url!
    }

    
}