//
//  UIView+CornerRadius.swift
//  IBDesignableExample
//
//  Created by Vishwas Singh on 01/05/16.
//  Copyright © 2016 daffodil. All rights reserved.
//

import UIKit

@IBDesignable
extension UIView {
    
    
    //MARK:- Border related
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor(CGColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue.CGColor
        }
    }

    
    //MARK:- Shadow related
    
    @IBInspectable var shadow: Bool {
        get {
            return self.shadow
        }
        set {
            if(newValue)
            {
                layer.shadowRadius = 10
                layer.shadowOpacity = 0.15
            }
        }
    }

    /* The shadow offset. Defaults to (0, -3). Animatable. */
    @IBInspectable var shadowOffset: CGPoint {
        set {
            layer.shadowOffset = CGSize(width: newValue.x, height: newValue.y)
        }
        get {
            return CGPoint(x: layer.shadowOffset.width, y:layer.shadowOffset.height)
        }
    }

    
   
    
}