//
//  QRUtilities.swift
//  QuranRadio
//
//  Created by Daffomacbook-25 on 30/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation

class QRUtilities: NSObject {
    
    class func getCurrentDayIndex() -> Int
    {
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE"
         dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        let currentDay: String = dateFormatter.stringFromDate(NSDate())
        let days: [String] = NSArray(objects:"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday") as! [String]
        let dayIndex: Int = days.indexOf(currentDay)!
        
        return dayIndex
    }
    
    class func getCurrentViewController() -> UIViewController
    {
        var topViewController: UIViewController
        let rootVC: UIViewController = (UIApplication.sharedApplication().keyWindow?.rootViewController)!
        
        if (rootVC.isKindOfClass(UINavigationController))
        {
            let navigationVC: UINavigationController = (rootVC as! UINavigationController)
            topViewController = navigationVC.topViewController!
        }
        else
        {
            topViewController = rootVC
        }
        
        return topViewController
    }
    
    class func setPrefferedLanguage(name:String){
        UserDefault.setObject(name, forKey: Language)
        UserDefault.synchronize()
        
    }
    
    class func getPrefferedLanguage()->String{
        if UserDefault.objectForKey(Language) != nil{
           return UserDefault.objectForKey(Language) as! String
        }
        return ""
        
    }
  
       
    
    
    

}
