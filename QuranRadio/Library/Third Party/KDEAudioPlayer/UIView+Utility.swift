//
//  UIView+Utility.swift
//  AlOula
//
//  Created by Vishwas Singh on 09/04/16.
//  Copyright © 2016 Daffodil iPhone. All rights reserved.
//

import UIKit
import MBProgressHUD

var spinner: UIActivityIndicatorView!

extension UIView
{
    
    // MARK:- Loader Methods
    func showLoaderWithMessage(message: String)
    {
        let hud: MBProgressHUD = MBProgressHUD.showHUDAddedTo(self, animated: true)
        hud.label.text = message
    }
    
    func showLoader()
    {
        MBProgressHUD.showHUDAddedTo(self, animated: true)
    }
    
    func hideLoader()
    {
        MBProgressHUD.hideAllHUDsForView(self, animated: true)
    }
    
    func showToastWithMessage(msg: String)
    {
        let hud: MBProgressHUD = MBProgressHUD.showHUDAddedTo(self, animated: true)
        hud.mode = .Text
        hud.labelText = msg
        hud.margin = 10.0
        hud.removeFromSuperViewOnHide = true
        hud.hide(true, afterDelay: 20)
    }
    
    func showSpinner() {
        spinner = UIActivityIndicatorView(activityIndicatorStyle: .White)
        spinner.frame = CGRectMake(self.frame.size.width / 2 - 15, self.frame.size.height / 2 - 15, 30, 30)
        spinner.startAnimating()
        self.addSubview(spinner)
        self.bringSubviewToFront(spinner)
    }
    
    func showSpinnerWithUserInteractionDisabled(shouldDisableUserInteraction: Bool) {
        if shouldDisableUserInteraction
        {
            self.alpha = 0.8
            self.userInteractionEnabled = false
        }
      
        self.hideSpinner()
        
        spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        spinner.frame = CGRectMake(self.frame.size.width/2-15, self.frame.size.height/2-15, 30, 30)
        spinner.startAnimating()
      //  spinner.color = UIColor.redColor()
        
        self.addSubview(spinner)
        self.bringSubviewToFront(spinner)
    }
    
    func hideSpinner()
    {
        self.userInteractionEnabled = true;
        self.alpha = 1.0;
        if(spinner != nil)
        {
            spinner.removeFromSuperview()
            spinner = nil
        }
        
    }

    
    
    
}