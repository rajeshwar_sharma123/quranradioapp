//
//  QRStripBarCell.h
//  QuranRadio
//
//  Created by daffolapmac on 02/08/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QRStripBarCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@end
