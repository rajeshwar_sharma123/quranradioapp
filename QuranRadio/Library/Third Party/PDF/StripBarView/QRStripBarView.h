//
//  QRStripBarView.h
//  QuranRadio
//
//  Created by daffolapmac on 02/08/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReaderThumbView.h"
#import "ReaderDocument.h"
@protocol QRStripBarViewDelegate <NSObject>

- (void)stripBarClickedAtIndex:(NSInteger)index;

@end

@interface QRStripBarView : UIView<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate>
@property (nonatomic,strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic) NSInteger currentPage;
@property (nonatomic) NSInteger count;
@property(nonatomic,strong) NSMutableDictionary *imageDic;
@property(nonatomic,weak) id<QRStripBarViewDelegate> delegate;
- (void)scrollCollectionViewAtIndex:(NSInteger)index;
+ (id)sharedManager;
- (void)setCount:(NSInteger )count andReaderDocument:(ReaderDocument *)readerDocument;
- (void)hideStrip;
- (void)showStrip;
@end

#pragma mark -

//
//	ReaderPagebarThumb class interface
//

@interface ReaderPagebarThumb1 : ReaderThumbView

- (instancetype)initWithFrame:(CGRect)frame small:(BOOL)small;

@end
