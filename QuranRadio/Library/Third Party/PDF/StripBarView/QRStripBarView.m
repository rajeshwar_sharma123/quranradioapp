//
//  QRStripBarView.m
//  QuranRadio
//
//  Created by daffolapmac on 02/08/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

#import "QRStripBarView.h"
#import "QRStripBarCell.h"
#import "ReaderThumbCache.h"
#define PREVIEW_LARGE_WIDTH 40
#define PREVIEW_LARGE_HEIGHT 50
@implementation QRStripBarView
{
    ReaderPagebarThumb1 *pageThumbView;
    ReaderDocument *document;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


+ (id)sharedManager {
    static QRStripBarView *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {
       self = [[[NSBundle mainBundle] loadNibNamed:@"QRStripBarView" owner:self options:nil] objectAtIndex:0];
        _imageDic = [[NSMutableDictionary alloc]init];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    self.count = 0;
    self.currentPage = 0;
    self.layer.cornerRadius = 7;
    self.clipsToBounds = YES;
    [self.collectionView registerNib:[UINib nibWithNibName:@"QRStripBarCell" bundle: nil] forCellWithReuseIdentifier:@"QRStripBarCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"QRStripBarSelectedCell" bundle: nil] forCellWithReuseIdentifier:@"QRStripBarSelectedCell"];
}
- (UIImage*)getPreviewImagesForStripBar:(NSInteger)page
{
           
        CGSize size = CGSizeMake(PREVIEW_LARGE_WIDTH, PREVIEW_LARGE_HEIGHT);
        NSURL *fileURL = document.fileURL;
        NSString *guid = document.guid;
        NSString *phrase = document.password;
        ReaderThumbRequest *request = [ReaderThumbRequest newForView:pageThumbView fileURL:fileURL password:phrase guid:guid page:page size:size];
        UIImage *image = [[ReaderThumbCache sharedInstance] thumbRequest:request priority:YES];
        UIImage *thumb = ([image isKindOfClass:[UIImage class]] ? image : nil);
        return thumb;
}

- (void)setCount:(NSInteger )count andReaderDocument:(ReaderDocument *)readerDocument{
    self.count = count;
    self.currentPage = count - 1;
    document = readerDocument;
}

#pragma mark- COllectionView Delegate & DataSource Method
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    QRStripBarCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"QRStripBarCell" forIndexPath:indexPath];
   
    if (indexPath.row == self.currentPage){
        cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"QRStripBarSelectedCell" forIndexPath:indexPath];
        cell.imgView.layer.borderWidth = 1;
        cell.imgView.layer.borderColor = [UIColor redColor].CGColor;
    }
    UIImage *imgPreview = [_imageDic objectForKey:[NSString stringWithFormat:@"%ld",(long)self.count-indexPath.row]];
    if(imgPreview == nil){
        UIImage *tmpImg = [self getPreviewImagesForStripBar:self.count-indexPath.row];
        if (tmpImg != nil) {
            cell.imgView.image = tmpImg ;
            [_imageDic setObject:tmpImg forKey:[NSString stringWithFormat:@"%ld",(long)self.count-indexPath.row]];
        }else{
            cell.imgView.image = [UIImage imageNamed:@"ic_program_placeholder"];
        }
      
    }
    else{
        cell.imgView.image = imgPreview;
    }
    cell.imgView.layer.cornerRadius = 2;
    cell.imgView.clipsToBounds = YES;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    if (screenSize.width == 320){
        if (indexPath.row == self.currentPage)
        {
            return CGSizeMake(28, 36);
        }
        return CGSizeMake(22, 33);
    }
    else if (screenSize.width == 375 ||screenSize.width == 414 ) {
        
        if (indexPath.row == self.currentPage)
        {
         return CGSizeMake(36, 44);
        }
        return CGSizeMake(30, 40);

    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        if (indexPath.row == self.currentPage)
        {
            return CGSizeMake(40, 48);
        }
        return CGSizeMake(34, 44);
    }
    return CGSizeMake(28, 36);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [_delegate stripBarClickedAtIndex:indexPath.row+1];
}

- (void)scrollCollectionViewAtIndex:(NSInteger)index
{
    self.currentPage = index-1;
    [self.collectionView reloadData];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self performSelector:@selector(refreshPreviewBar)
               withObject:self
               afterDelay:1.0];
  }

- (void)refreshPreviewBar
{
    [self.collectionView reloadData];
    UIImage *img1 = [_imageDic objectForKey:[NSString stringWithFormat:@"%ld",(long)self.currentPage+1]];
    UIImage *img2 = [_imageDic objectForKey:[NSString stringWithFormat:@"%ld",(long)self.currentPage]];
    if(img1 == nil || img2 == nil)
    {
    [self.collectionView reloadData];
    [self performSelector:@selector(refreshPreviewBar)
                   withObject:self
                   afterDelay:2.0];
    }
    
}

- (void)hideStrip{
    if (self.hidden == NO) // Only if visible
    {
        [UIView animateWithDuration:0.25 delay:0.0
                            options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                         animations:^(void)
         {
             self.alpha = 0.0f;
         }
                         completion:^(BOOL finished)
         {
             self.hidden = YES;
         }
         ];
    }

}
- (void)showStrip{
    if (self.hidden == YES) // Only if hidden
    {
        
        [UIView animateWithDuration:0.25 delay:0.0
                            options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                         animations:^(void)
         {
             self.hidden = NO;
             self.alpha = 1.0f;
         }
                         completion:NULL
         ];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    NSArray* visibleCellIndex = self.collectionView.indexPathsForVisibleItems;
    if (visibleCellIndex.count == 0){
        return;
    }
    NSArray *sortedIndexPaths = [visibleCellIndex   sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSIndexPath *path1 = (NSIndexPath *)obj1;
        NSIndexPath *path2 = (NSIndexPath *)obj2;
        return [path1 compare:path2];
    }];
    NSInteger num = sortedIndexPaths.count/2;
    
    NSIndexPath *indexPath = [sortedIndexPaths objectAtIndex:num];
    [_delegate stripBarClickedAtIndex:indexPath.row+1];
    
   
}

@end



#pragma mark -

//
//	ReaderPagebarThumb class implementation
//

@implementation ReaderPagebarThumb1

#pragma mark - ReaderPagebarThumb instance methods

- (instancetype)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame small:NO];
}

- (instancetype)initWithFrame:(CGRect)frame small:(BOOL)small
{
    if ((self = [super initWithFrame:frame])) // Superclass init
    {
        CGFloat value = (small ? 0.6f : 0.7f); // Size based alpha value
        
        UIColor *background = [UIColor colorWithWhite:0.8f alpha:value];
        
        self.backgroundColor = background; imageView.backgroundColor = background;
        
        imageView.layer.borderColor = [UIColor colorWithWhite:0.4f alpha:0.6f].CGColor;
        
        imageView.layer.borderWidth = 1.0f; // Give the thumb image view a border
    }
    
    return self;
}


@end

