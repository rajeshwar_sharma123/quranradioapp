//
//  PrayerReminder+CoreDataProperties.swift
//  QuranRadio
//
//  Created by daffolapmac on 29/07/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PrayerReminder {

    @NSManaged var alarmId: String?
    @NSManaged var isAlarmed: NSNumber?
    @NSManaged var prayerName: String?
    @NSManaged var prayerTime: String?

}
