//
//  ProgramScheduleEntity+CoreDataProperties.swift
//  QuranRadio
//
//  Created by daffolapmac on 30/07/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ProgramScheduleEntity {

    @NSManaged var dayId: NSNumber?
    @NSManaged var endTime: String?
    @NSManaged var imageUrl: String?
    @NSManaged var notificationDate: NSDate?
    @NSManaged var presenterName: String?
    @NSManaged var programmeId: NSNumber?
    @NSManaged var programNameArabic: String?
    @NSManaged var programNameEnglish: String?
    @NSManaged var startTime: String?

}
