//
//  ProgramScheduleManager.swift
//  QuranRadio
//
//  Created by daffolapmac on 16/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ProgramScheduleManager {
    
    class func addProgramShchedule(model:QRProgramModel){
        let managedContext  = DataController().managedObjectContext
        let entity =  NSEntityDescription.entityForName("ProgramScheduleEntity",
                                                        inManagedObjectContext: managedContext)
        let programmeDetail = NSManagedObject(entity: entity!,
                                              insertIntoManagedObjectContext: managedContext) as! ProgramScheduleEntity
        programmeDetail.dayId = model.dayId
        programmeDetail.programmeId = model.programmeId
        programmeDetail.programNameArabic = model.programNameArabic
        programmeDetail.programNameEnglish = model.programNameEnglish
        programmeDetail.imageUrl = model.image
        programmeDetail.startTime = model.startTime
        programmeDetail.endTime = model.endTime
        programmeDetail.notificationDate = model.notificationDate
        
        do
        {
            try managedContext.save()
        }
        catch
        {
            
        }
        
        let arr = ProgramScheduleManager.fetchScheduledReminderList()
        print(arr)
        
    }
    
      class func fetchScheduledReminderList() -> [ProgramScheduleEntity] {
        
       let managedContext  = DataController().managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "ProgramScheduleEntity")
        
        
        var scheduleList = [ProgramScheduleEntity]()
        do
        {
            if let fetchResults = try managedContext.executeFetchRequest(fetchRequest) as? [ProgramScheduleEntity]
            {
                scheduleList = fetchResults
                debugPrint("count: \(scheduleList.count)")
            }
            
        }
            
        catch
            
        {
            
        }
        
        return scheduleList
        
        
        
    }
   /* class func getAllSchulesDayWise() -> [AnyObject]    {
        
        let daysInArabicArray = ["السبت","الجمعة","الخميس","الأربعاء","الثلاثاء","الإثنين","الأحد"]
        var tempArray = [AnyObject]()
        
        let allSchedules = self.fetchScheduledReminderList() as NSArray
        
        for index in 0...6
        {
            let predicate : NSPredicate = NSPredicate(format: "dayId == %d",index)
            let sortedByDay = (allSchedules as NSArray).filteredArrayUsingPredicate(predicate)
            
            if(sortedByDay.count > 0)
            {
                let scheduleDic = [
                    "dayId" : index,
                    "dayName" : "\(daysInArabicArray[index])",
                    "schedules" : sortedByDay,
                    ]
                tempArray.append(scheduleDic)
            }
        }
        debugPrint("all schedules day wise: \(tempArray)")
        
        return tempArray
    } */
    
    class func getAllSchulesDayWise() -> NSMutableArray    {
        
        var daysArray : NSArray!
         if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            daysArray = ["السبت","الجمعة","الخميس","الأربعاء","الثلاثاء","الإثنين","الأحد"]
         }else{
            daysArray = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
        }
        
        let tempArray = NSMutableArray()
        
        let allSchedules = self.fetchScheduledReminderList() as NSArray
        
        for index in 0...6
        {
            let predicate : NSPredicate = NSPredicate(format: "dayId == %d",index)
            let sortedByDay = (allSchedules as NSArray).filteredArrayUsingPredicate(predicate) as NSArray
            let arry = NSMutableArray()
            if(sortedByDay.count > 0)
            {
                
//                programmeDetail.dayId = model.dayId
//                programmeDetail.programmeId = model.programmeId
//                programmeDetail.programNameArabic = model.programNameArabic
//                programmeDetail.programNameEnglish = model.programNameEnglish
//                programmeDetail.imageUrl = model.image
//                programmeDetail.startTime = model.startTime
//                programmeDetail.endTime = model.endTime

                for (var i = 0; i < sortedByDay.count; i += 1){
                    
                    let modelEntity = sortedByDay.objectAtIndex(i) as! ProgramScheduleEntity
                    let programModel = QRProgramModel()
                    programModel.dayId = Int(modelEntity.dayId!)
                    programModel.programmeId = Int(modelEntity.programmeId!)
                    programModel.programNameArabic = modelEntity.programNameArabic!
                    programModel.programNameEnglish = modelEntity.programNameEnglish
                    programModel.image = modelEntity.imageUrl
                    programModel.startTime = modelEntity.startTime
                    programModel.endTime = modelEntity.endTime
                    arry.addObject(programModel)
                }
                
                let scheduleDic = [
                    "dayId" : index,
                    "dayName" : "\(daysArray[index])",
                    "schedules" : arry,
                    ]
                tempArray.addObject(scheduleDic)
                
            }
        }
        debugPrint("all schedules day wise: \(tempArray)")
        
        return tempArray
    }
    


    
    class func alreadyAddedProgramDetails(dayId: Int, programmeID: Int) -> ProgramScheduleEntity? {
        
       let managedContext  = DataController().managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "ProgramScheduleEntity")
        let predicate : NSPredicate = NSPredicate(format: "dayId == %d && programmeId == %d",dayId,programmeID)
        fetchRequest.predicate = predicate
        
        debugPrint("day count: \(dayId)")
        
        var userDetails = [ProgramScheduleEntity]()
        do
        {
            if let fetchResults = try managedContext.executeFetchRequest(fetchRequest) as? [ProgramScheduleEntity]
            {
                userDetails = fetchResults
            }
            
        }
            
        catch
            
        {
            
        }
        
        if (userDetails.count > 0)
        {
            
            return  userDetails[0]
        }
        else
        {
            return nil
        }
    }
    class func removeProgramSchedule(dayId: Int, programmeID: Int) {
        
        let managedContext  = DataController().managedObjectContext
        
        
        let fetchRequest = NSFetchRequest(entityName: "ProgramScheduleEntity")
        let predicate : NSPredicate = NSPredicate(format: "dayId == %d && programmeId == %d",dayId,programmeID)
        fetchRequest.predicate = predicate
        var userDetails = [ProgramScheduleEntity]()
        do
        {
            if let fetchResults = try managedContext.executeFetchRequest(fetchRequest) as? [ProgramScheduleEntity]
            {
                userDetails = fetchResults
                managedContext.deleteObject(fetchResults[0])
                debugPrint("count: \(userDetails.count)")
                try  managedContext.save()
                
            }
            
        }
            
        catch
            
        {
            
        }
        
    }
    
    class func deletePastProgramScheduleFromDataBase()  {
        let managedContext  = DataController().managedObjectContext
        
        
        
        let fetchRequest = NSFetchRequest(entityName: "ProgramScheduleEntity")
        
        
        var scheduleList = [ProgramScheduleEntity]()
        do
        {
            if let fetchResults = try managedContext.executeFetchRequest(fetchRequest) as? [ProgramScheduleEntity]
            {
                scheduleList = fetchResults
                debugPrint("count: \(scheduleList.count)")
            }
            
        }
            
        catch
            
        {
            
        }

        
        
        //let allSchedules = self.fetchScheduledReminderList()
        if scheduleList.count > 0{
            
            for index in 0..<scheduleList.count {
                
                let entity = scheduleList[index]  as ProgramScheduleEntity
                
                if entity.notificationDate!.isLessThanDate(NSDate()){
                    
                    managedContext.deleteObject(entity)
                    
                   // [context deleteObject:[context objectWithID:aObjectID]];
                    
                    do {
                         try  managedContext.save()
                        
                    } catch{
                       print("Something went wrong!")
                    }
                    
                } else{
                    
                    print("greater")
                }
            }
            
        }
    }


    
}