//
//  QRAboutUsEngViewController.swift
//  QuranRadio
//
//  Created by Daffomacbook-25 on 07/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRAboutUsEngViewController: QRBaseViewViewController {

    @IBOutlet weak var textViewAboutUs: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "ABOUT US"
        setBackButton()
        setBackButtonLayout()
        textViewAboutUs.scrollRangeToVisible(NSRange(location: 0,length: 1))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
