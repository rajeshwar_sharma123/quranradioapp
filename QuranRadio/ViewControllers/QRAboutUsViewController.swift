//
//  QRAboutUsViewController.swift
//  QuranRadio
//
//  Created by daffolapmac on 24/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import MessageUI

class QRAboutUsViewController: QRBaseViewViewController {
    
    @IBOutlet weak var aboutUsLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBackButton()
        setBackButtonLayout()
        
        if Localisator.sharedInstance.currentLanguage != "DeviceLanguage"{
            // configureViewFromLocalisation()
        }
        self.title = "معلومات عنا"
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "CPDFontAL", size: 18)!]
        let paragraphStyleLine: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraphStyleLine.alignment = .Right
        paragraphStyleLine.headIndent = 10.0
        paragraphStyleLine.lineSpacing = 10.0
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: aboutUsLabel.text!)
        attributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyleLine, range: NSMakeRange(0, attributedString.length))
        
        aboutUsLabel.attributedText = attributedString
    }
    
       
}
