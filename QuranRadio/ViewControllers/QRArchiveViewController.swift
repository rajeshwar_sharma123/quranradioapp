//
//  QRArchiveViewController.swift
//  QuranRadio
//
//  Created by daffolapmac on 17/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRArchiveViewController: QRBaseViewViewController {

    @IBOutlet var tableView: UITableView!
    var allProgramList: Array<QRRadioProgramModel> = []
    var currentPage : Int = 1
    var shouldProceed : Bool = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = Localization("k_Archive_Title")
        setBackButton()
        setBackButtonLayout()
        initTableView()
        callWebServiceToGetAllPrograms()
        tableView.reomveEmptyRows()
    }
    private func initTableView(){
        
        self.tableView.enablePullToRefreshWithTarget(self, selector: #selector(QRArchiveViewController.pullToRefresh))
        
       // noDataFoundView = AONoDataFoundView.addInTableView(self.tableView, viewController: self)
    }
    
    func pullToRefresh()
    {
        self.currentPage = 1
        self.shouldProceed = true
        self.callWebServiceToGetAllPrograms()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK: Function to call service to get archive programs list
    func callWebServiceToGetAllPrograms()
    {
        if !shouldProceed
        {
            return
        }
        
        if(!self.tableView.refreshControl!.refreshing)
        {
            self.view.showLoader()
        }
        
        let requestData = [
            "page":"\(self.currentPage)"
        ]
        
        
    
    if NetworkConnectivity.isConnectedToNetwork()
    {
    
    
    QRAppServices.performPostRequest(QRServiceUrls.getCompleteUrlFor(k_Url_Archive_List), requestData: requestData, successBlock: { (request, response, headers) in
        self.view.hideLoader()
        if(self.tableView.refreshControl!.refreshing)
        {
            self.tableView.refreshControl?.endRefreshing()
            self.allProgramList.removeAll()
        }
        
        let responseData = response as! NSDictionary
        let dataArray = responseData["data"] as! NSArray
        
        self.shouldProceed = false
        self.view.hideLoader()
        
        
        for (program) in dataArray
        {
            self.allProgramList.append(QRRadioProgramModel.init(fromDictionary: program as! NSDictionary))
            
        }
        self.tableView.reloadData()
        self.currentPage += 1
        

        
    }) { (errorMessage, response, headers) in
    
    self.view.hideLoader()
    self.showAlertWithMessage(Localization("Server_Error"), andTitle: Localization("Error"))
    }
    }else{
        self.view.hideLoader()
    
        showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
        }


}
}
extension QRArchiveViewController: UITableViewDelegate , UITableViewDataSource{
    /*
     Tells the delegate that the table view is about to draw a cell for a particular row.
     */
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
                   forRowAtIndexPath indexPath: NSIndexPath)
    {
        
        if (indexPath.row == self.allProgramList.count - 1 && !tableView.refreshControl!.refreshing)
        {
            self.callWebServiceToGetAllPrograms()
        }
        
        
        // Remove separator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell!
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            cell = configureArchiveArabicCell(indexPath)
        }else{
            cell = configureArchiveEnglishCell(indexPath)
        }
        return cell
           
        
    }
    //MARK:- Custome ArchiveCell Configuration
    func configureArchiveEnglishCell(indexPath:NSIndexPath)->UITableViewCell{
        let cell = self.tableView.dequeueReusableCellWithIdentifier("QRArchiveEnglishCell") as! QRArchiveEnglishCell
        cell.programName.text = self.allProgramList[indexPath.row].programNameEnglish
        let programImageUrl = self.allProgramList[indexPath.row].programPicture.urlByAddingPercentEncoding()
        cell.programPicture.sd_setImageWithURL(programImageUrl, placeholderImage: UIImage(named: "ic_program_placeholder"))
        cell.programPicture.clipsToBounds = true
        return cell

    }
    func configureArchiveArabicCell(indexPath:NSIndexPath)->UITableViewCell{
        
        let  cell = self.tableView.dequeueReusableCellWithIdentifier("QRArchiveTableViewCell") as! QRArchiveTableViewCell
        cell.programName.text = self.allProgramList[indexPath.row].programNameArabic
        let programImageUrl = self.allProgramList[indexPath.row].programPicture.urlByAddingPercentEncoding()
        cell.programPicture.sd_setImageWithURL(programImageUrl, placeholderImage: UIImage(named: "ic_program_placeholder"))
        cell.programPicture.clipsToBounds = true
        return cell
 
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.allProgramList.count
    }
    
    //MARK:- Set Height for Row
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var id : String!
        var image : UIImage!
        var programName : String!
        var bannerImageUrl : String!
        
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            let cell =  self.tableView.cellForRowAtIndexPath(indexPath) as! QRArchiveTableViewCell
            id = allProgramList[indexPath.row].program_archive_id
            image  = cell.programPicture.image
            programName = allProgramList[indexPath.row].programNameArabic

        }else{
            let cell =  self.tableView.cellForRowAtIndexPath(indexPath) as! QRArchiveEnglishCell
             id = allProgramList[indexPath.row].program_archive_id
            image  = cell.programPicture.image
            programName = allProgramList[indexPath.row].programNameEnglish
        }
        
        bannerImageUrl =  allProgramList[indexPath.row].programPicture
        
        
        let progNowObj = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("QRProgramsNowViewController") as! QRProgramsNowViewController
        progNowObj.archive_id = id
        progNowObj.bannerImage = image
        progNowObj.programName = programName
        progNowObj.bannerImageUrl = bannerImageUrl
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.navigationController?.pushViewController(progNowObj, animated: true)
    }
}

