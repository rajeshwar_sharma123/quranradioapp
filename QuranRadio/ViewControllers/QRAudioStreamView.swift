//
//  QRAudioStreamView.swift
//  QuranRadio
//
//  Created by Daffomacbook-25 on 23/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer


class QRAudioStreamView: UIView, AVAudioPlayerDelegate, AudioPlayerDelegate {

    
    @IBOutlet weak var audioView: UIView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnMute: UIButton!
    
    @IBOutlet weak var volumeSlider: UISlider!
    
    
    var isPlaying : Bool = true
    var hasStartedOnce : Bool = false
    
    var mute: Bool = true
       
    var player: AVPlayer!
    var timer:NSTimer!
    var playerItem: AVPlayerItem!
    
    var link:String? = nil
    var linkFound:Bool = false
    
    
    let audioPlayer = AudioPlayer()
    var audioItem: AudioItem!
    
   // var equalizerView: PCSEQVisualizer!
    
    static var onceToken: dispatch_once_t = 0
    static var instance: QRAudioStreamView? = nil
    
    
    class func sharedInstanceQRAudioStreamView() -> QRAudioStreamView {
        
        dispatch_once(&onceToken) {
            instance = NSBundle.mainBundle().loadNibNamed("QRAudioStreamView", owner: nil, options: nil)[0] as? QRAudioStreamView
            
        }
        return instance!
    }

    override func awakeFromNib() {
        
        volumeSlider.value = AVAudioSession.sharedInstance().outputVolume
       
//        volumeSlider.setThumbImage(UIImage(), forState: .Normal)
//        volumeSlider.setThumbImage(UIImage(), forState: .Selected)
//        volumeSlider.setThumbImage(UIImage(), forState: .Highlighted)
//        volumeSlider.setThumbImage(UIImage(), forState: .Disabled)
        
        observeForDeviceVolumeChange()
        
        self.audioView.layer.cornerRadius = 7
        self.audioView.clipsToBounds = true
      
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(reachabilityStatusChanged), name: ReachabilityChangedNotification, object: nil)
        //self.initEqualizerView()
        
        self.callWebServiceToGetPrayerURL()
    }
    
    override func layoutSubviews() {
       
    }
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func reachabilityStatusChanged(sender: AnyObject)
    {
        if(!NetworkConnectivity.isConnectedToNetwork())
        {
            self.pauseRadio()
        }
    }
    
    
    //MARK:- IBAction Methods
    
    
    @IBAction func playAudio(sender: AnyObject) {
        
        self.callWebServiceToGetPrayerURL()
    }
    
    @IBAction func muteAudio(sender: AnyObject) {
        
        let volume = AVAudioSession.sharedInstance().outputVolume
        if mute {
            //This runs if the user wants music
            print("The button will now turn on music.")
            mute = false
            audioPlayer.volume = 0.0
            btnMute.setImage(UIImage(named: "ic_mute"), forState: .Normal)
        } else {
            //This happens when the user doesn't want music
            print("the button will now turn off music.")
            mute = true
            audioPlayer.volume = volume
            btnMute.setImage(UIImage(named: "ic_sound"), forState: .Normal)
        }
        
       volumeSlider.value = audioPlayer.volume
    }
    
    
    @IBAction func volumeChanged(sender: UISlider)
    {
        audioPlayer.volume = sender.value

        if audioPlayer.volume == 0
        {
            mute = false
            btnMute.setImage(UIImage(named: "ic_mute"), forState: .Normal)
        }
        else
        {
            mute = true
            btnMute.setImage(UIImage(named: "ic_sound"), forState: .Normal)
        }
    }
    
    func observeForDeviceVolumeChange()
    {
        let audioSession = AVAudioSession.sharedInstance()
        audioSession.addObserver(self, forKeyPath: "outputVolume", options: NSKeyValueObservingOptions.New, context: nil)
        
    }

    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>)
    {
        if(keyPath == "outputVolume")
        {
            let volume = AVAudioSession.sharedInstance().outputVolume
            volumeSlider.value = volume
            audioPlayer.volume = volume
            
            if audioPlayer.volume == 0
            {
                mute = false
                btnMute.setImage(UIImage(named: "ic_mute"), forState: .Normal)
            }
            else
            {
                mute = true
                btnMute.setImage(UIImage(named: "ic_sound"), forState: .Normal)
            }
        }
    }

    
    func playInitAudio(){
        
            if NetworkConnectivity.isConnectedToNetwork()
            {
                if isPlaying {
                    
                    if !hasStartedOnce
                    {
                        self.hasStartedOnce = true
                        // self.initRadio()
                        self.initAudioPlayer()
                    }
                    else
                    {
                        self.pauseRadio()
                    }
                    
                } else {
                    //self.playRadio()
                    self.initAudioPlayer()
                    
                }
            }else{
                
                QRUtilities.getCurrentViewController().showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
                
                
                
        }
        
        
        
        
    }
    
    
    //MARK: - Equliser Init
    
 /*   func initEqualizerView()
    {
        equalizerView =  PCSEQVisualizer(numberOfBars:18)
        equalizerView.frame = CGRectMake(btnPlay.imageView!.frame.origin.x, btnPlay.imageView!.frame.origin.y, 26, btnPlay.imageView!.frame.size.height)
        
        
        if(isPad)
        {
           // equalizerView.frame = CGRectMake(btnPlay.imageView!.frame.origin.x, btnPlay.imageView!.frame.origin.y+2, 26, btnPlay.imageView!.frame.size.height+4)
        }
        
        equalizerView.layer.cornerRadius = 4
        equalizerView.clipsToBounds = true
        equalizerView.barColor = UIColor.blackColor()
        equalizerView.backgroundColor = UIColor.whiteColor()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(equaliserBarTapped))
        equalizerView.addGestureRecognizer(tapGesture)
        
        equalizerView.start()
        
        //btnPlay.addSubview((equalizerView)!)
        equalizerView.hidden = true
    }
    
    */
//    func equaliserBarTapped(gesture: UITapGestureRecognizer)
//    {
//        self.playAudio(UIButton())
//    }
    
    
    
    //MARK: Radio Init
    
    func pauseRadio()
    {
        self.hideSpinner()
        btnPlay.setImage(UIImage(named: "ic_pause"), forState:.Normal)
        self.isPlaying = false
        
        audioPlayer.pause()
       // equalizerView.hidden = true
        
    }
    
    func playRadio()
    {
        btnPlay.setImage(UIImage(named: "ic_play"), forState:.Normal)
        self.isPlaying = true
        audioPlayer.resume()
    }
    
    
    func initAudioPlayer()
    {
       //link = "http://flagshippro.com/apps/dubai_quraan/azkars/14681536128031.mpeg"
        if link != nil {
            btnPlay.setImage(UIImage(named: "ic_play"), forState:.Normal)
            self.isPlaying = true
            let url = NSURL(string: (string: link!))
            audioItem = AudioItem(mediumQualitySoundURL: url)
            audioItem.title = "Quran Radio"
            audioPlayer.playItem(audioItem)
            audioPlayer.delegate = self
            self.btnPlay.showSpinnerWithUserInteractionDisabled(true)
        } else{
            
            QRUtilities.getCurrentViewController().showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
            
        }
        
        
    }
    
    
    
    
    // AudioPlayerDelegate methods
    
    func audioPlayer(audioPlayer: AudioPlayer, willStartPlayingItem item: AudioItem) {
        
    }
    
    func audioPlayer(audioPlayer: AudioPlayer, didChangeStateFrom from: AudioPlayerState, toState to: AudioPlayerState)
    {
        
        if(audioPlayer.state == AudioPlayerState.Buffering || to == AudioPlayerState.Buffering)
        {
           // equalizerView.hidden = true
            self.btnPlay.showSpinnerWithUserInteractionDisabled(true)
            btnPlay.setImage(UIImage(named: "ic_play"), forState:.Normal)
        }
        else if(audioPlayer.state == AudioPlayerState.Paused)
        {
            
        }
        else if(audioPlayer.state == AudioPlayerState.Playing)
        {
            //equalizerView.hidden = false
            btnPlay.hideSpinner()
            btnPlay.setImage(UIImage(named: "ic_play"), forState:.Normal)
        }
    }
    
    func audioPlayer(audioPlayer: AudioPlayer, didFindDuration duration: NSTimeInterval, forItem item: AudioItem)
    {
        
    }
    
    func audioPlayer(audioPlayer: AudioPlayer, didUpdateProgressionToTime time: NSTimeInterval, percentageRead: Float)
    {
        
        
    }
    
    
    
    //MARK:- Handling of remote control events such as from play/pause action event from lock screen
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func remoteControlReceivedWithEvent(event: UIEvent?)
    {
        if event!.type == UIEventType.RemoteControl
        {
            if event?.subtype == UIEventSubtype.RemoteControlPlay
            {
                self.playRadio()
            }
            
            if event?.subtype == UIEventSubtype.RemoteControlPause
            {
                self.pauseRadio()
            }
            
            if event?.subtype == UIEventSubtype.RemoteControlNextTrack
            {
                
            }
            
            if event?.subtype == UIEventSubtype.RemoteControlPreviousTrack
            {
                
            }
        }
    }
    
    
    func callWebServiceToGetPrayerURL(){
        
       let currentVC = QRUtilities.getCurrentViewController()
        
        if hasStartedOnce == true {
            
            self.playInitAudio()
            
        }else {
            if NetworkConnectivity.isConnectedToNetwork()
            {
                btnPlay.setImage(UIImage(named: "ic_play"), forState:.Normal)
                self.btnPlay.showSpinnerWithUserInteractionDisabled(true)
                
                QRAppServices.performPostRequest("http://flagshippro.com/apps/dubai_quraan/API/streamLink", requestData: "", successBlock: { (request, response, headers) in
                    
                    let responseData = response as! NSDictionary
                    let data = responseData["data"] as! NSDictionary
                    let link:String = data["link"] as! String
                    self.linkFound = true
                    
                    print(link)
                    
                    self.link = link
                    
                    
                    self.playInitAudio()
                    
                }) { (errorMessage, response, headers) in
                   // self.equalizerView.hidden = true
                    self.isPlaying = false
                    self.btnPlay.hideSpinner()
                    self.btnPlay.setImage(UIImage(named: "ic_pause"), forState:.Normal)
                   
                    currentVC.showAlertWithMessage(Localization("Server_Error"), andTitle: Localization("Error"))
                    
                }
            }else{
                
                //self.equalizerView.hidden = true
                self.isPlaying = false
                self.btnPlay.hideSpinner()
                self.btnPlay.setImage(UIImage(named: "ic_pause"), forState:.Normal)
                
            QRUtilities.getCurrentViewController().showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
                                
            }
        }
        
        
        
        
    }
    
    
}
