//
//  QRAzkarViewController.swift
//  QuranRadio
//
//  Created by Jenkins_New on 6/24/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
//import DropDown
import AVFoundation

class QRAzkarViewController: QRBaseViewViewController, UITableViewDataSource, UITableViewDelegate {
     private var selectedRowSet:Set<Int> = [-1]
    @IBOutlet weak var azkarListTableView: UITableView!
    @IBOutlet weak var categoryButton: UIButton!
    
    var isPlaying = false
    var currentLanguage: String!
    var azkars = [QRAzkarModel]()
    var categories = [QRAzkarCategoryModel]()
    let dropdown = DropDown()
    var pageIndex = 1
    var selectedCategory: QRAzkarCategoryModel?
   var infoBtnCheck = false
    
    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = Localization("k_Azkaar_Title")
        setBackButton()
        setBackButtonLayout()
        
        azkarListTableView.dataSource = self
        azkarListTableView.delegate = self
        azkarListTableView.tableFooterView = UIView(frame: CGRectZero)
        azkarListTableView.estimatedRowHeight = 113
        
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        currentLanguage = QRUtilities.getPrefferedLanguage()
        
        callServicetoGetAzkarCategories()
//        callServicetoGetAzkarList()
        
        
    }

    override func viewWillAppear(animated: Bool) {
       // azkarListTableView.reloadData()
        infoBtnCheck = false
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        UserDefault.setObject(false, forKey: "k_song_status")
        
        if !infoBtnCheck{
            QRAudioPlayer.sharedInstance.pause()
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- IBAction Methods
    
    @IBAction func categoryButtonTapped(sender: AnyObject)
    {
        if dropdown.hidden {
            dropdown.show()
        } else {
            dropdown.hide()
        }

    }
    
    func dropdownCellSelected(index: Int, item: String)
    {
        // remove previous category's data
        pageIndex = 1
        azkars.removeAll()
        azkarListTableView.reloadData()
        
        selectedRowSet.removeAll()
     
        isPlaying = false
        QRAudioPlayer.sharedInstance.pause()
        
        self.categoryButton.setTitle(item, forState: .Normal)
        categoryButton.setImageRightOfLabel(UIImage(named: "ic_mrn_duya")!)
        selectedCategory = self.categories[index]
        callServicetoGetAzkarListForCategory(selectedCategory!.id!)
    }

    //MARK:- BL Methods
    func chkPlaying(mynotification: NSNotification){
        
    }
    
    
    func finishedPlaying(mynotification: NSNotification){
        
    }

    func playSong(sender:UIButton)
    {
        QRAudioStreamView.sharedInstanceQRAudioStreamView().pauseRadio()
        
        let indexPath = NSIndexPath(forRow: sender.tag , inSection: 0)
        let cell =  self.azkarListTableView.cellForRowAtIndexPath(indexPath) as! QRAzkarTableViewCell
        //https://audioboom.com/boos/4420441-11-04-2016.mp3
        // issues url http://flagshippro.com/apps/dubai_quraan/azkars/14681536128031.mpeg
        //(cell.azkar?.audioUrl)!
        if(!selectedRowSet.contains(sender.tag))
        {
            selectedRowSet.removeAll()
            selectedRowSet.insert(sender.tag)
            QRAudioPlayer.sharedInstance.playAudioWithParentViewController(self, cell: cell, urlForSong: (cell.azkar?.audioUrl)!)
            isPlaying = true
            UserDefault.setObject(true, forKey: "k_song_status")
            cell.playButton.setImage(UIImage(named: "ic_pause_azkaar"), forState: .Normal)

        }
        else{
        
        if(isPlaying)
        {
            QRAudioPlayer.sharedInstance.pause()
            isPlaying = false
            cell.playButton.setImage(UIImage(named: "ic_play_azkar"), forState: .Normal)
            UserDefault.setObject(false, forKey: "k_song_status")
        }
        else
        {
            
           isPlaying = true
            QRAudioPlayer.sharedInstance.play()
            cell.playButton.setImage(UIImage(named: "ic_pause_azkaar"), forState: .Normal)
            
            UserDefault.setObject(true, forKey: "k_song_status")
        }
        }
        
                
        
        //self.azkarListTableView.reloadData()
        //self.azkarListTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
        
    }
    func  infoButtonAction(sender:UIButton)
    {
       // isPlaying = false
        infoBtnCheck = true
        var strInfo :String
        let indexPath = NSIndexPath(forRow: sender.tag , inSection: 0)
        let azkar = azkars[indexPath.row]
        if(currentLanguage == English)
        {
            strInfo = azkar.azkar_benifits_details_en!
        }else{
           strInfo = azkar.azkar_benifits_details_ar!
        }
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("QRInfoViewController") as! QRInfoViewController
        vc.infoStr = strInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func addDropDown()
    {
        var cat = [String]()
        for category in categories
        {
            if(currentLanguage == Arabic)
            {
                cat.append(category.category_ar!)
            }
            else
            {
                cat.append(category.category_en!)
            }
        }
        
        categoryButton.addDropDownList(dropdown, dataSource: cat, view: self.view)
        
        
        self.dropdownCellSelected(0, item: cat[0])
        dropdown.selectionAction = { (index: Int, item: String) in
            
            self.dropdownCellSelected(index, item: item)
            
        }
        self.dropdown.show()
        
    }
    

    
    //MARK:- UITableView Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  
        return azkars.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var reuseIdentifier = ""
        
        if(currentLanguage == English)
        {
            reuseIdentifier = "QRAzkarTableViewCell_English"
        }
        else
        {
            reuseIdentifier = "QRAzkarTableViewCell_Arabic"
        }
        
        let azkar = azkars[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) as! QRAzkarTableViewCell
        cell.bindDataWithAzkar(azkar)
       
        cell.playButton.tag = indexPath.row
        cell.infoButton.tag = indexPath.row
        cell.playButton!.addTarget(self, action: #selector(QRAzkarViewController.playSong(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        cell.infoButton!.addTarget(self, action: #selector(QRAzkarViewController.infoButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        if(selectedRowSet.contains(indexPath.row))
        {
            if(isPlaying)
            {
            cell.playButton.setImage(UIImage(named: "ic_pause_azkaar"), forState: .Normal)
                
            }else{
                cell.playButton.setImage(UIImage(named: "ic_play_azkar"), forState: .Normal)
            }
        }
        else
        {
            cell.playButton.setImage(UIImage(named: "ic_play_azkar"), forState: .Normal)
        }
        
        
        if(indexPath.row == 0)
        {
            cell.contentView.backgroundColor = UIColor(red: 254.0/255, green: 254.0/255, blue: 254.0/255, alpha: 1.0)
        }
        else
        {
            cell.contentView.backgroundColor = UIColor(red: 249.0/255, green: 249.0/255, blue: 249.0/255, alpha: 1.0)
        }

        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
                   forRowAtIndexPath indexPath: NSIndexPath)
    {
        if ((self.azkars.count%10 == 0) && indexPath.row == self.azkars.count-1)
        {
            self.callServicetoGetAzkarListForCategory(selectedCategory!.id!)
        }
        
        // Remove separator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
        
        
    }

    
    
    
    //MARK:- Service Methods

    func callServicetoGetAzkarListForCategory(categoryId: String)
    {
        if !NetworkConnectivity.isConnectedToNetwork()
        {
            if(QRUtilities.getPrefferedLanguage() == Arabic)
            {
                self.showAlertWithMessage("من فضلك، تحقق من اتصالك بالانترنيت", andTitle: "خطأ")
            }
            else
            {
                self.showAlertWithMessage("Please check your network connection.", andTitle: "Error")
            }
            
            return
        }
        

        
        self.view.showLoader()
        
        QRAppServices.performPostRequest(Url_Azkar_List, requestData: ["category":categoryId, "page":self.pageIndex], successBlock: { (request, response, headers) in
           
            self.view.hideLoader()
            
            if response!["code"] as! Int == 200
            {
                let responseData = response as! NSDictionary
                
                if let azkarList = responseData["data"] as? NSArray
                {
                    for dict in azkarList
                    {
                       let azkarModel = QRAzkarModel(fromDictionary: dict as! NSDictionary)
                        self.azkars.append(azkarModel)
                    }
                }
                
                self.azkarListTableView.reloadData()
                self.pageIndex += 1
            }
            
            }) { (errorMessage, response, headers) in
                self.view.hideLoader()
                self.showAlertWithMessage(Localization("Server_Error"), andTitle: Localization("Error"))
        }
    }

    func callServicetoGetAzkarCategories()
    {
        if !NetworkConnectivity.isConnectedToNetwork()
        {
            
            if(QRUtilities.getPrefferedLanguage() == Arabic)
            {
                self.showAlertWithMessage("من فضلك، تحقق من اتصالك بالانترنيت", andTitle: "خطأ")
            }
            else
            {
                self.showAlertWithMessage("Please check your network connection.", andTitle: "Error")
            }
            
            return
        }
        
        self.view.showLoader()
        
        QRAppServices.performPostRequest(Url_Azkar_Categories, requestData: [:], successBlock: { (request, response, headers) in
            
            self.view.hideLoader()
            
            if response!["code"] as! Int == 200
            {
                let responseData = response as! NSDictionary
                
                if let categoryList = responseData["data"] as? NSArray
                {
                    for dict in categoryList
                    {
                        let category = QRAzkarCategoryModel(fromDictionary: dict as! NSDictionary)
                        self.categories.append(category)
                    }
                }
                
                self.addDropDown()
            }
            
        }) { (errorMessage, response, headers) in
            self.view.hideLoader()
            self.showAlertWithMessage(Localization("Server_Error"), andTitle: Localization("Error"))
        }
    }

}

 
extension UIButton
{
    func addDropDownList(dropDown: DropDown,dataSource: [String], view: UIView)
    {
        dropDown.dataSource = dataSource
        dropDown.selectionAction = { (index, item) in
            self.setTitle(item, forState: .Normal)
        }
        
        dropDown.anchorView = self
        dropDown.bottomOffset = CGPoint(x: 0, y:self.bounds.height)
        dropDown.dismissMode = .Automatic
        if (dataSource.count > 0){
            dropDown.selectRowAtIndex(0)
        }
    }
    
    func setImageRightOfLabel(image: UIImage){
        if( QRUtilities.getPrefferedLanguage() == English){
            self.setImage(image, forState: UIControlState.Normal)
            if let imageWidth = self.imageView?.frame.width{
                self.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, imageWidth+5);
            }
            if let _ = self.titleLabel?.frame.width{
                let spacing = self.frame.size.width - (self.imageView!.frame.width + 10)
                self.imageEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, -spacing);
            }

        }
        else{
            self.setImage(image, forState: UIControlState.Normal)
            if let imageWidth = self.imageView?.frame.width{
                self.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, imageWidth+5);
            }
            if let _ = self.titleLabel?.frame.width{
                let spacing = self.frame.size.width - (self.titleLabel?.frame.width)! - 27
                self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, -spacing);
               // self.titleLabel?.frame.size.width
            }

        }
        
        
    }
    
    

    
}
