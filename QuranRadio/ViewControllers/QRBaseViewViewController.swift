//
//  QRBaseViewViewController.swift
//  QuranRadio
//
//  Created by Daffomacbook-25 on 23/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import CoreData
import EventKit
//import DGRunkeeperSwitch
struct globalPrayer {
    static  var prayerArray:[Prayer] = [Prayer]()
}

protocol QRBaseViewViewControllerDelegate{
    func languageChange()
    
    
}

class QRBaseViewViewController: UIViewController {
    var delegate :QRBaseViewViewControllerDelegate! = nil
    var check = false
    //var languageBarButton: UIButton!
    var backBarButton: UIButton!
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    let timeZone = "Asia/Dubai"
    var arabicsForPrayer: [String:String] = ["Fajr":"الفجر","Sunrise":"شروق الشمس","Dhuhr":"الظهر","Asr":"العصر","Sunset":"غروب الشمس","Maghrib":"المغرب","Isha":"العشاء",]
    var timingsData = [AnyObject]()
    let dataModel = DataController().managedObjectContext
    let eventStore = EKEventStore()
    var reminders: [EKReminder]!
    let prefs = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*************** Scheduling local notification for Prayer Timing(three minute before prayer time)  ******************/
    func scheduleLocalNotificationForPrayerTiming(timingsData:[AnyObject]){
        
      
        
        if let _ = NSUserDefaults.standardUserDefaults().valueForKey("isNotificationOn")
        {}
        else
        {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isNotificationOn")
            
        }
        
        for notification in UIApplication.sharedApplication().scheduledLocalNotifications! {
            UIApplication.sharedApplication().cancelLocalNotification(notification)
        }
        
        
        
        
        
       UIApplication.sharedApplication().cancelAllLocalNotifications()
        
        if NSUserDefaults.standardUserDefaults().boolForKey("isNotificationOn") == true{
            
            let dateFormatter = NSDateFormatter()
            let calendar = NSCalendar.autoupdatingCurrentCalendar()
            dateFormatter.dateFormat = "dd MM yyyy"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
            let currentDateString = dateFormatter.stringFromDate(NSDate())
            for index in 0 ..< timingsData.count{
                
                let objDate = timingsData[index]
                let timeString = objDate["timing"] as! String
                let nameString = objDate["name"] as! String
                if nameString != "Sunset"{
                    let obj = Prayer(name: nameString, time: timeString)
                    globalPrayer.prayerArray.append(obj)
                if nameString != "Sunrise" && NSUserDefaults.standardUserDefaults().objectForKey("switchValue") as! String == "1" {
                        
                        let objDate = timingsData[index]
                        let timeString = objDate["timing"] as! String
                        let nameString = objDate["name"] as! String
                        sechedulNotificationForAlarm(nameString, prayerTime: timeString)
                    
                    /*let dateString = "\(currentDateString) \(timeString)"
                        print(dateString)
                        // let dateString = "09 06 2016 11:24 AM"
                        dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                        dateFormatter.dateFormat = "dd MM yyyy hh:mm a"
                        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
                         var scheduleTime = dateFormatter.dateFromString(dateString)
                        scheduleTime = calendar.dateByAddingUnit(.Minute, value: -3, toDate: scheduleTime!, options:[])
                        
                        let localNotification:UILocalNotification = UILocalNotification()
                    if #available(iOS 8.2, *) {
                        if Localisator.sharedInstance.currentLanguage == "English_en"{
                            localNotification.alertTitle = "Dubai Quran Radio"
                        }else{
                       localNotification.alertTitle = "إذاعة دبي للقرآن الكريم"
                        }
                        
                    } else {
                        // Fallback on earlier versions
                    }
                        
                        localNotification.alertBody = getLocalNotificationAlertMessage(nameString)
                        localNotification.soundName = UILocalNotificationDefaultSoundName
                        localNotification.repeatInterval = NSCalendarUnit.Day
                        print(scheduleTime)
                        var userInfo = [String:AnyObject]()
                        let keyValue = 1
                        userInfo["tag"] = keyValue
                        userInfo["prayerName"] = nameString
                        userInfo["prayerTime"] = timeString
                        localNotification.userInfo = userInfo
                        localNotification.timeZone = NSTimeZone.systemTimeZone()
                        localNotification.fireDate = scheduleTime
                        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)*/
                        
                    }
                }
            }
            
           seedPrayerList()
        }
    }
    
    func getLocalNotificationAlertMessage(name:String) -> String {
        
        if Localisator.sharedInstance.currentLanguage == "English_en"{
            switch name {
            case "Fajr":
                return"Azan AlFajr After 3 Minuties"
            case "Dhuhr":
                return"Azan AlDhur After 3 Minuties"
            case "Asr":
                return"Azan AlAsr After 3 Minuties"
            case "Maghrib":
                return"Azan AlMaghrib After 3 Minuties"
            case "Isha":
                return"Azan AlIshaa After 3 Minuties"
            default:
                ""
            }
            return ""

        }
        else{
            switch name {
            case "Fajr":
                return"سيحين موعد آذان الفجر بعد ثلاث دقائق"
            case "Dhuhr":
                return"سيحين موعد آذان الظهر بعد ثلاث دقائق"
            case "Asr":
                return"سيحين موعد آذان العصر بعد ثلاث دقائق"
            case "Maghrib":
                return"سيحين موعد آذان المغرب بعد ثلاث دقائق"
            case "Isha":
                return"سيحين موعد آذان العشاء بعد ثلاث دقائق"
            default:
                ""
            }
            return ""

        }
        
    }
    func getLocalNotificationAlarmMessage(name:String) -> String {
        
        if Localisator.sharedInstance.currentLanguage == "English_en"{
            switch name {
            case "Fajr":
                return"Azan AlFajr"
            case "Dhuhr":
                return"Azan AlDhur"
            case "Asr":
                return"Azan AlAsr"
            case "Maghrib":
                return"Azan AlMaghrib "
            case "Isha":
                return"Azan AlIshaa"
            default:
                ""
            }
            return ""
            
        }
        else{
            switch name {
            case "Fajr":
                return"آذان الفجر"
            case "Dhuhr":
                return"آذان الظهر"
            case "Asr":
                return"آذان العصر"
            case "Maghrib":
                return"آذان المغرب"
            case "Isha":
                return"آذان العشاء"
            default:
                ""
            }
            return ""
            
        }
        
    }

    /************ Scheduling local notification for alarm if prayer time alarm status is true **********/
    func sechedulNotificationForAlarm(prayerName:String,prayerTime:String){
        if  alarmStatus(prayerName) {
            
            if NSUserDefaults.standardUserDefaults().boolForKey("isNotificationOn") == true{
                
                let dateFormatter = NSDateFormatter()
               // let calendar = NSCalendar.autoupdatingCurrentCalendar()
                dateFormatter.dateFormat = "dd MM yyyy"
                dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
                let currentDateString = dateFormatter.stringFromDate(NSDate())
                let nameString = prayerName
                //|| nameString != "Sunrise"
                if nameString != "Sunset"{
                    
                    let timeString = prayerTime
                    let nameString = prayerName
                    let dateString = "\(currentDateString) \(timeString)"
                    //  let dateString = "09 06 2016 01:13 PM"
                    dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                    dateFormatter.dateFormat = "dd MM yyyy hh:mm a"
                    dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
                    let scheduleTime = dateFormatter.dateFromString(dateString)
                    let localNotification:UILocalNotification = UILocalNotification()
                    if #available(iOS 8.2, *) {
                        localNotification.alertTitle = " "
                        localNotification.alertBody = getLocalNotificationAlarmMessage(nameString)
                    } else {
                        // Fallback on earlier versions
                        localNotification.alertBody = getLocalNotificationAlarmMessage(nameString)
                    }

                    localNotification.repeatInterval = NSCalendarUnit.Day
                    localNotification.soundName = "QuranAPP.wav"
                    var userInfo = [String:AnyObject]()
                    let keyValue = 2
                    userInfo["tag"] = keyValue
                    userInfo["prayerName"] = nameString
                    userInfo["prayerTime"] = timeString
                    localNotification.userInfo = userInfo
                    localNotification.timeZone = NSTimeZone.systemTimeZone()
                    localNotification.fireDate = scheduleTime
                    UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                }
            }
        }
    }
    
    
    func getImage(name:String) -> UIImage {
        let imageView = UIImageView(image: UIImage(named: name))
        imageView.contentMode = .Center
        imageView.contentMode = .ScaleAspectFit
        imageView.clipsToBounds = true
        return imageView.image!
    }
    func setBackButton(){
        
        let buttonImage = UIImage(named: "ic_back");
        backBarButton = UIButton(type: UIButtonType.Custom)
        backBarButton.frame = CGRectMake(0, 0, 30,  30) ;
        backBarButton.setImage(buttonImage, forState: UIControlState.Normal)
        backBarButton.addTarget(self, action:#selector(QRBaseViewViewController.backBtnClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        //backBarButton.backgroundColor = UIColor.greenColor()

    }
    func hideBackButton(){
        self.backBarButton.hidden = true;
    }
    /************ set custom back button **********/
    func setBackButtonLayout()  {
    let negativeSpacer = UIBarButtonItem.init(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
        negativeSpacer.width = -18;
    let backBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: backBarButton)
        if Localisator.sharedInstance.currentLanguage == "English_en"{
            

            self.navigationItem.setLeftBarButtonItems([negativeSpacer,backBarButtonItem], animated: false);
            backBarButton.transform = CGAffineTransformIdentity
           
            self.navigationItem.rightBarButtonItems = nil
        }else{
            self.navigationItem.setRightBarButtonItems([negativeSpacer,backBarButtonItem], animated: false);
            backBarButton.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
            self.navigationItem.setHidesBackButton(true, animated:true);
        }
        
    }
    
    func backBtnClicked(sender:UIButton!)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    /************ Its gives next prayer timing **********/
    func getNextPrayer() -> Prayer{
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MM yyyy"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        let currentDateString = dateFormatter.stringFromDate(NSDate())
        
        
        if globalPrayer.prayerArray.count > 0 {
            for prayer in globalPrayer.prayerArray{
                let timeString = prayer.PrayerTime
                let dateString = "\(currentDateString) \(timeString)"
                dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                dateFormatter.dateFormat = "dd MM yyyy hh:mm a"
                dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
                let prayerDate = dateFormatter.dateFromString(dateString)
                
                if prayerDate!.isGreaterThanDate(NSDate()){
                    print(prayer.prayerName)
                    return prayer
                }
            }
        }
        
        return Prayer()
    }
    
    // Core Data Methodds
    
    func seedPrayerList(){
        
        for prayer in globalPrayer.prayerArray{
            if validateRecord(prayer.prayerName) {
                
                self.updateRecord(prayer.prayerName, prayerTime: prayer.PrayerTime)
            } else {
                
                do {
                    let ent = NSEntityDescription.entityForName("PrayerReminder", inManagedObjectContext: dataModel)
                    let newPrayer = PrayerReminder(entity: ent!, insertIntoManagedObjectContext: dataModel)
                    newPrayer.prayerName = prayer.prayerName
                    newPrayer.prayerTime = prayer.PrayerTime
                    newPrayer.isAlarmed = false
                    
                    print("Saved")
                    try dataModel.save()
                } catch{
                    fatalError("Error while saving data :- \(error)")
                }
            }
        }
    }
    
    
    func validateRecord(prayerName:String) -> Bool {
        
        let fetchRequest = NSFetchRequest(entityName: "PrayerReminder")
        print(prayerName)
        let predicate = NSPredicate(format: "prayerName = %@", prayerName)
        fetchRequest.predicate = predicate
        
        
        do {
            let fetchResults = try dataModel.executeFetchRequest(fetchRequest) as! [PrayerReminder]
            
            print(fetchResults.count)
            if fetchResults.count > 0 {
                print("already favd")
                return true
            }
        } catch{
            
            fatalError("Error :- \(error)")
        }
        
        return false
    }
    
    
    func updateRecord(prayerName:String,prayerTime:String){
        
        let fetchRequest = NSFetchRequest(entityName: "PrayerReminder")
        let predicate = NSPredicate(format: "prayerName = %@", prayerName)
        fetchRequest.predicate = predicate
        
        
        do {
            let fetchResults = try dataModel.executeFetchRequest(fetchRequest) as! [PrayerReminder]
            
            if fetchResults.count > 0 {
                let managedObject = fetchResults[0]
                managedObject.setValue(prayerTime, forKey: "prayerTime")
                
                try dataModel.save()
                
                
                if alarmStatus(prayerName) {
                  //  removeReminder(prayerName)
                   // saveNewReminder(prayerTime)
                }
            }
            
        } catch{
            fatalError("Error :- \(error)")
        }
        
    }
    
    
    func updateAlarm(prayerName:String,prayerTime:String){
        
        let fetchRequest = NSFetchRequest(entityName: "PrayerReminder")
        let predicate = NSPredicate(format: "prayerName = %@", prayerName)
        fetchRequest.predicate = predicate
        
        
        do {
            let fetchResults = try dataModel.executeFetchRequest(fetchRequest) as! [PrayerReminder]
            print(fetchResults.count)
            if fetchResults.count > 0 {
                let managedObject = fetchResults[0]
                
                if alarmStatus(prayerName) {
                    managedObject.setValue(false, forKey: "isAlarmed")
                } else {
                    managedObject.setValue(true, forKey: "isAlarmed")
                }
                
                
                
                try dataModel.save()
            }
            
        } catch{
            fatalError("Error :- \(error)")
        }
    }
    
    
    
    func updateAlarmId(prayerName:String, uniqueAlarmId:String){
        let fetchRequest = NSFetchRequest(entityName: "PrayerReminder")
        let predicate = NSPredicate(format: "prayerName = %@", prayerName)
        fetchRequest.predicate = predicate
        
        
        do {
            let fetchResults = try dataModel.executeFetchRequest(fetchRequest) as! [PrayerReminder]
            print(fetchResults.count)
            if fetchResults.count > 0 {
                let managedObject = fetchResults[0]
                
                managedObject.setValue(uniqueAlarmId, forKey: "alarmId")
                
                try dataModel.save()
            }
            
        } catch{
            fatalError("Error :- \(error)")
        }
    }
    
    
    func getAlarmId(prayerName:String)->String{
        let fetchRequest = NSFetchRequest(entityName: "PrayerReminder")
        let predicate = NSPredicate(format: "prayerName = %@", prayerName)
        fetchRequest.predicate = predicate
        
        
        do {
            let fetchResults = try dataModel.executeFetchRequest(fetchRequest) as! [PrayerReminder]
            print(fetchResults.count)
            print(fetchResults[0].prayerName)
            
            let alarm = fetchResults[0]
            print(fetchResults[0].alarmId)
            if fetchResults.count > 0 {
                
                return alarm.alarmId!
            }
            
        } catch{
            fatalError("Error :- \(error)")
        }
        
        return ""
    }
    
    func getAllRecords(){
        
    }
    
    func alarmStatus(prayerName:String) -> Bool{
        
        let fetchRequest = NSFetchRequest(entityName: "PrayerReminder")
        let predicate = NSPredicate(format: "prayerName = %@", prayerName)
        fetchRequest.predicate = predicate
        
        do {
            let fetchResults = try dataModel.executeFetchRequest(fetchRequest) as! [PrayerReminder]
            
            if fetchResults.count > 0 {
                return (fetchResults[0].isAlarmed?.boolValue)!
            }
            
        } catch{
            fatalError("Error :- \(error)")
        }
        
        return false
    }
    
    
    
    func deleteAllRecords(){
        let fetchRequest = NSFetchRequest(entityName: "PrayerReminder")
        if #available(iOS 9.0, *) {
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            do {
                try dataModel.executeRequest(deleteRequest)
                print("deleted")
            } catch _ as NSError {
                // TODO: handle the error
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    // MARK: - Reminder Methods
    
    func saveNewReminder(prayerName: String){
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MM yyyy"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        let currentDateString = dateFormatter.stringFromDate(NSDate())
        
        let dateString = "\(currentDateString) \(prayerName)"
        dateFormatter.timeZone = NSTimeZone.systemTimeZone()
        dateFormatter.dateFormat = "dd MM yyyy hh:mm a"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        
        print(dateString)
        
        eventStore.requestAccessToEntityType(EKEntityType.Event, completion: {
            granted, error in
            if (granted) && (error == nil) {
                print("granted \(granted)")
                print("error  \(error)")
                
                let event:EKEvent = EKEvent(eventStore: self.eventStore)
                event.title = prayerName
                let eventDate = dateFormatter.dateFromString(dateString)
                
                event.startDate = eventDate!
                event.allDay = true
                
                event.calendar = self.eventStore.defaultCalendarForNewEvents
                
                let alarm:EKAlarm = EKAlarm(relativeOffset: -60)
                event.alarms = [alarm]
                
                do {
                    try self.eventStore.saveEvent(event, span: EKSpan.ThisEvent, commit: true)
                    print("Saved Event")
                } catch{
                    fatalError("event can't saved with error \(error)")
                }
                
                
                
            }
        })
        
        
        
    }
    
    
    
    func removeReminder(prayerName:String){
        
        let predicate = eventStore.predicateForRemindersInCalendars([])
        eventStore.fetchRemindersMatchingPredicate(predicate) { reminders in
            for reminder in reminders! {
                print(reminder.title)
                
                
            }}
        
        
        // What about Calendar entries?
        let startDate=NSDate().dateByAddingTimeInterval(-60*60*24)
        let endDate=NSDate().dateByAddingTimeInterval(60*60*24*3)
        let predicate2 = eventStore.predicateForEventsWithStartDate(startDate, endDate: endDate, calendars: nil)
        print("startDate:\(startDate) endDate:\(endDate)")
        let eV = eventStore.eventsMatchingPredicate(predicate2) as [EKEvent]!
        
        if eV != nil {
            for i in eV {
                print("Title  \(i.title)" )
                print("stareDate: \(i.startDate)" )
                print("endDate: \(i.endDate)" )
                
                if i.title == prayerName {
                    print("YES" )
                    // Uncomment if you want to delete
                    
                    do {
                        try eventStore.removeEvent(i, span: EKSpan.ThisEvent, commit: true)
                        print("alarm removed")
                    }
                    catch{
                        fatalError("Delete event is cancel with error \(error)")
                    }
                }
            }
        }
    }
    
    func dateComponentFromNSDate(date: NSDate)-> NSDateComponents{
        
        let calendarUnit: NSCalendarUnit = [.Hour, .Day, .Month, .Year]
        let dateComponents = NSCalendar.currentCalendar().components(calendarUnit, fromDate: date)
        return dateComponents
    }
    
    
    func initializePrayerArrayWithData() -> Bool{
        let prayerFetch = NSFetchRequest(entityName: "PrayerReminder")
        
        do {
            let fetchPrayer = try dataModel.executeFetchRequest(prayerFetch) as! [PrayerReminder]
            print(fetchPrayer.count)
            print(fetchPrayer)
            
            if fetchPrayer.count > 0 {
                for i in 0 ..< fetchPrayer.count{
                    let obj = Prayer(name: fetchPrayer[i].prayerName!, time: fetchPrayer[i].prayerTime!)
                    globalPrayer.prayerArray.append(obj)
                }
                return true
            } else {
                return false
            }
            
            
        } catch{
            fatalError("Error while initalize the data")
        }
    }
    
    
    
    func getDateFromTime(timeString:String)->NSDate{
        
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MM yyyy"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        let currentDateString = dateFormatter.stringFromDate(NSDate())
        print(currentDateString)
        let dateString = "\(currentDateString) \(timeString)"
        print(dateString)
        dateFormatter.dateFormat = "dd MM yyyy hh:mm a"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        print(dateFormatter.dateFromString(dateString))
        return dateFormatter.dateFromString(dateString)!
    }
    
    func get24HourFormat(date:NSDate)->String{
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        let currentDateString = dateFormatter.stringFromDate(date)
        print(currentDateString)
        return currentDateString
        
    }
    
    func get12HourFormat(date:NSDate)->String{
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        let currentDateString = dateFormatter.stringFromDate(date)
        print(currentDateString)
        return currentDateString
        
    }
    
    
    func convertTimeIn12HourFormat(date: String)->String
    {
        let dateString = date
        let df = NSDateFormatter()
        df.dateFormat = "HH:mm:ss"
        df.locale = NSLocale(localeIdentifier: "en_US")
        let date = df.dateFromString(dateString)
        df.dateFormat = "hh:mma"
        df.AMSymbol = "AM"
        df.PMSymbol = "PM"
        let stringFromDate = df.stringFromDate(date!)
        
        return stringFromDate
    }
    
       
}
