//
//  QRGooglePlacesViewController.swift
//  QuranRadio
//
//  Created by daffolapmac on 20/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import GoogleMaps
class QRGooglePlacesViewController: QRBaseViewViewController,GMSMapViewDelegate,CLLocationManagerDelegate {

    @IBOutlet var mapView: GMSMapView!
    var locationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var flag : Bool!
    var markerList = [GMSMarker]()
    var placesArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //setNavigationBarColor()
        mapView.delegate = self
        mapView.myLocationEnabled = true
        
        //self.title = "MOSQUES NEARBY"
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        mapView.settings.myLocationButton = false
        mapView.settings.compassButton = true
        flag = true
       // self.navigationController?.navigationBar.hideBottomHairline()
                
        setBackButton()
        if Localisator.sharedInstance.currentLanguage != "DeviceLanguage"{
            configureViewFromLocalisation()
        }

        let currentLang: AnyObject? = NSLocale.preferredLanguages()[0]
        print(currentLang)
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "CPDFontAL", size: 18)!]
       //
    
    }
    
    // MARK:- BL methods
    
    func showLoaderOnMap(map: GMSMapView)
    {
        for view in map.subviews
        {
            if view.isKindOfClass(NSClassFromString("GMSUISettingsView")!)
            {
                view.showLoader()
                break
            }
        }
    }
    
    func hideLoaderOnMap(map: GMSMapView)
    {
        for view in map.subviews
        {
            if view.isKindOfClass(NSClassFromString("GMSUISettingsView")!)
            {
                view.hideLoader()
                break
            }
        }
    }
    
    
    // MARK:- GMSMapViewDelegate Method
    
    func mapView(mapView: GMSMapView, didChangeCameraPosition position: GMSCameraPosition)
    {
        //print("start")
        
    }
    func mapView(mapView: GMSMapView, idleAtCameraPosition position: GMSCameraPosition){
       
    }
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {

        
        switch CLLocationManager.authorizationStatus() {
        case .AuthorizedAlways, .AuthorizedWhenInUse: break
        // ...
        case .NotDetermined:
            manager.requestWhenInUseAuthorization()
        case .Restricted, .Denied:
            let alertController = UIAlertController(
                title: Localization("Location_Disabled"),
                message: Localization("Location_Disabled_Message"),
                preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: Localization("cancel"), style: .Cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: Localization("Open_Settings"), style: .Default) { (action) in
                if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            alertController.addAction(openAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        currentLocation =  manager.location
        if flag==true{
            getPlaceDetail()
            setCameraPosition()
            flag = false
        }
        
    }
    
    /************ Calling Goggle Maps places api for places detail **********/
    
    func getPlaceDetail()
    {
        self.showLoaderOnMap(mapView)
        
        QRAppServices.getPlacesWithLocation(currentLocation, placeType: "mosque", radius: kRadius, successBlock: { (responseData) -> () in
            
            print(responseData)
            let tmpPlaces : NSArray = responseData["results"] as! NSArray
            self.placesArray.addObjectsFromArray(tmpPlaces as [AnyObject])
            self.drawMarker(self.placesArray)
            
            self.hideLoaderOnMap(self.mapView)
            
        }) { (errorMessage) -> () in
            if Localisator.sharedInstance.currentLanguage == "English_en"{
                self.showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
                
            } else {
                self.showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
            }
            self.hideLoaderOnMap(self.mapView)
        }
        
        
    }
   /************ Draw Mosque marker on map  **********/
    func drawMarker(placesArr:NSMutableArray) {
        for  placeDetail in placesArray{
            let geometry = placeDetail["geometry"] as! NSDictionary
            let locationTmp = geometry["location"] as! NSDictionary
            let location = CLLocation(latitude: locationTmp["lat"] as! CLLocationDegrees, longitude: locationTmp["lng"] as! CLLocationDegrees)
            let marker = GMSMarker()
            //let time = location
            
           
            marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
            marker.appearAnimation = kGMSMarkerAnimationPop
            marker.icon = UIImage(named: "ic_gps")
            marker.title = "\(placeDetail["name"] as! String), \(placeDetail["vicinity"] as! String)"
            marker.map = mapView
            self.markerList.append(marker)
            
        }
        
        fitAllMarkers()
        
        
    }
    
    func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
        print("tap")
        if marker.snippet == nil{
        self.getLocationDetail(marker)
        }
    
        return false
    }
    
    
    func getLocationDetail(marker:GMSMarker){
        self.showLoaderOnMap(mapView)
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(currentLocation.coordinate.latitude),\( currentLocation.coordinate.longitude)&destination=\(marker.position.latitude),\( marker.position.longitude)&mode=transit&key=\(Google_Browser_Key)"
        
       
        
        QRAppServices.getServiceRequest(urlString: url, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            let response = responseData as! NSDictionary
            self.hideLoaderOnMap(self.mapView)
            print(response)
            
            if (response.objectForKey("routes") as! NSArray).count == 0{
                return
            }
            
            let routes = (response.objectForKey("routes") as! NSArray).firstObject as! NSDictionary
            let legs = (routes.objectForKey("legs") as! NSArray).firstObject as! NSDictionary
            let distance = legs.objectForKey("distance") as! NSDictionary
            let duration = legs.objectForKey("duration") as! NSDictionary
            
            let distanceStr = distance.objectForKey("text") as! String
            let durationStr = duration.objectForKey("text") as! String
            
            let snipest = "Distance: \(distanceStr), Duration: \(durationStr)"
            
            marker.snippet = snipest
            self.mapView.selectedMarker = marker
            
            
        }) { (errorMessage) -> () in
            
            self.hideLoaderOnMap(self.mapView)
            self.showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
        }
        

    }
    
    
    /************ fit all marker within screen size **********/
    func fitAllMarkers() {
        var bounds = GMSCoordinateBounds()
        
        for marker in markerList {
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        mapView.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(bounds))
    }
     func setCameraPosition() {
        
        let camera : GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: 13)
        self.mapView.animateToCameraPosition(camera)
        
    }
    
     func configureViewFromLocalisation() {
        setBackButtonLayout()
        self.title = Localization("Mosques_Nearby")
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUserLocation(){
       locationManager.delegate = self
      
    }
 
}
