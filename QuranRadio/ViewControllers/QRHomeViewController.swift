//
//  QRHomeViewController.swift
//  QuranRadio
//
//  Created by daffolapmac on 20/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

func DegreesToRadians (value:Double) -> Double {
    return value * M_PI / 180.0
}

func RadiansToDegrees (value:Double) -> Double {
    return value * 180.0 / M_PI
}

class QRHomeViewController: QRBaseViewViewController, CLLocationManagerDelegate,UIScrollViewDelegate {
    
    
    var scrollView: UIScrollView!
    var swipeFirstViewController : QRSwipeFirstViewController!
    var swipeSecondViewController : QRSwipeSecondViewController!
    
   
    @IBOutlet var pageView: UIView!
    @IBOutlet var lblNextPrayer: UILabel!
    @IBOutlet var lblAboutUs: QRLabel!
    @IBOutlet var lblHolyQuran: QRLabel!
    @IBOutlet var lblMosqueNearby: QRLabel!
    @IBOutlet var lblPrayerTime: QRLabel!
    var rightBarButton: UIButton!
    @IBOutlet weak var lblPrayer_Time: QRLabel!
    @IBOutlet weak var lblPrayerName: QRLabel!
    @IBOutlet var audioView:QRAudioStreamView!
    @IBOutlet weak var lblDateIslamic: QRLabel!
     @IBOutlet var lblDateIslamic1: QRLabel!
    @IBOutlet weak var compass: UIImageView!
    let screenSize: CGRect = UIScreen.mainScreen().bounds
   // var programScheduleButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    var kabahLocation : CLLocation?
    var latitude  : Double?
    var longitude : Double?
    var distanceFromKabah : Double?
    var needleAngle : Double?
    let locationManger = CLLocationManager()
   // var pageViewController: UIPageViewController!
    var pageControl:UIPageControl = UIPageControl()

    @IBOutlet var lblPrayerTimeHeightConstraint: NSLayoutConstraint!
    @IBOutlet var lblPrayerNameHeightConstraint: NSLayoutConstraint!
    @IBOutlet var myView: UIView!
//    private(set) lazy var orderedViewControllers: [UIViewController] = {
//        return [self.newColoredViewController("QRSwipeFirstViewController"),
//                self.newColoredViewController("QRSwipeSecondViewController")]
//    }()
//    private func newColoredViewController(name: String) -> UIViewController {
//        return UIStoryboard(name: "Main", bundle: nil) .
//            instantiateViewControllerWithIdentifier(name)
//    }

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
              
        //setPageViewController()
        setScroolView()
        
        //setLanguageButton()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(QRHomeViewController.receiveLanguageChangedNotification(_:)), name: kNotificationLanguageChanged, object: nil)
        /************ Set app language **********/
        if QRUtilities.getPrefferedLanguage().isEmpty || QRUtilities.getPrefferedLanguage() == Arabic {
            
            SetLanguage(arrayLanguages[kArabic])
            check = false
            
        }
        else {
            SetLanguage(arrayLanguages[kEnglish])
        }
        
        setCustomSize()
        setNavigationBarColor()
        self.audioView = QRAudioStreamView.sharedInstanceQRAudioStreamView()
        
        self.getData()
        self.initCompass()
        if NSUserDefaults.standardUserDefaults().objectForKey("tag") != nil{
            if NSUserDefaults.standardUserDefaults().objectForKey("tag") as! String == "1"{
                self.navigationController?.pushViewController((self.storyboard?.instantiateViewControllerWithIdentifier("QRGooglePlacesViewController"))!, animated: false)
                NSUserDefaults.standardUserDefaults().removeObjectForKey("tag")
            }
        }
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        
        dateToHijri(NSDate())
        initNextPrayer()
        
        
    }
    
    


    func setCustomSize(){
        if screenWidth == 320{
            lblPrayerTime.font = UIFont(name: (lblPrayerTime.font?.fontName)!, size: 40)
        }
        if screenHeight == 480{
            lblPrayerNameHeightConstraint.constant = 2
            lblPrayerTimeHeightConstraint.constant = -2
        }

    }
    override func viewDidAppear(animated: Bool) {
        needleAngle = 0.0
        self.locationManger.startUpdatingHeading()
        kabahLocation = CLLocation(latitude: 21.42 , longitude: 39.83)
        self.locationManger.delegate = self
        var height:CGFloat = 50
        if screenHeight == 480{
           height = 35
        }else  if screenHeight == 568{
            height = 40
        } else if isPad{
            height = 70
        }
        self.audioView.frame = CGRectMake(0, screenHeight-height-72, screenWidth, height)
        self.view.addSubview(audioView)
        self.audioView.bringSubviewToFront(audioView)
        
        // animatePageViewController()
    }
    
    // MARK: - Notification methods
    func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
            
        }
    }
    
    func configureViewFromLocalisation() {
        
        //setLanguageButtonLayout()
        self.title = Localization("Home_Title")
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "CPDFontAL", size: 18)!]
        lblNextPrayer.text = Localization("Next_Prayer")
        lblPrayer_Time.text = Localization("Prayer_Timing")
        lblMosqueNearby.text = Localization("Mosques_Nearby_Home")
        lblHolyQuran.text = Localization("Holy_Quran")
        lblAboutUs.text = Localization("About_Us_Home")
        initNextPrayer()
        
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    
    
    func dateToHijri(date:NSDate) {
        
      
      
        let monthsInArabic = ["مُحَرَّم","صَفَر","رَبيع الأوّل","رَبيع الثاني","جُمادى الأولى","جُمادى الآخرة","رَجَب","شَعْبان","رَمَضان","شَوّال","ذو القعدة","ذو الحجة"]
        
        
        let monthsInEnglish = ["Muharram", "Ṣafar", "Rabī‘ al-awwal", "Rabī‘ ath-thānī", "Jumādá al-ūlá", "Jumādá al-ākhirah", "Rajab", "Sha‘bān", "Ramaḍān", "Shawwāl", "Dhū al-Qa‘dah", "Dhū al-Ḥijjah"]
        
        let GregorianDate = date
        let islamic = NSCalendar(identifier: NSCalendarIdentifierIslamicUmmAlQura)
        let components = islamic?.components(NSCalendarUnit(rawValue: UInt.max), fromDate: GregorianDate)
       
        var month = ""
        if( QRUtilities.getPrefferedLanguage() == English)
        {
            month = monthsInEnglish[(components?.month)!-1]
        }
        else
        {
           month = monthsInArabic[(components?.month)!-1]
        }

        
       // let month = components?.calendar?.monthSymbols[(components?.month)!-1]
        let era = components?.calendar?.eraSymbols[(components?.era)!]
        //let result = "\(month) \(components!.day), \(components!.year) \(era!)"
       // var tmp = "\(components!.day), \(components!.year) \(era!)"
        
        self.lblDateIslamic1.text = month
        self.lblDateIslamic.text = "\(components!.day), \(components!.year) \(era!)"
        
        //return result
    }
    
    
    func getData(){
        
        if let lastDateValue =  prefs.valueForKey(LAST_DATE_FETCHED_PRAYER_TIME) {
            
            let calendar: NSCalendar = NSCalendar.currentCalendar()
            let lastDateComponents: NSDateComponents = calendar.components([.Year, .Month, .Day], fromDate: lastDateValue as! NSDate)
            
            let currentDateComponents = calendar.components([.Year, .Month, .Day], fromDate: NSDate())
            
            
            if lastDateComponents.month == currentDateComponents.month && lastDateComponents.year == currentDateComponents.year && lastDateComponents.day == currentDateComponents.day{
                
                if !self.initializePrayerArrayWithData() {
                    self.callWebServicetoGetPrayerTimings()
                } else {
                    self.initNextPrayer()
                }
                
                
            } else {
                self.callWebServicetoGetPrayerTimings()
            }
            
        } else {
            self.callWebServicetoGetPrayerTimings()
        }
        
        
    }
    
     /**************** getting next prayer details ******************/
    func initNextPrayer(){
        var prayer = getNextPrayer()
        if prayer.prayerName == "" {
            if globalPrayer.prayerArray.count == 0{
                return
            }
            prayer = globalPrayer.prayerArray[0]
        }
        
        if prayer.PrayerTime.characters.count != 0{
            lblPrayerTime.text = get24HourFormat(getDateFromTime(prayer.PrayerTime))
        }
        lblPrayerName.text = Localization(prayer.prayerName).uppercaseString
    }
    
    
    func initCompass(){
        
        kabahLocation = CLLocation(latitude: 21.42 , longitude: 39.83)
        self.locationManger.delegate = self
        self.locationManger.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManger.requestWhenInUseAuthorization()
        self.locationManger.startUpdatingLocation()
        self.locationManger.startUpdatingHeading()
    }
    
    
    
    // MARK: Location Manager Delegate
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        
        
        self.latitude = location?.coordinate.latitude
        self.longitude = location?.coordinate.longitude
        
        self.locationManger.startUpdatingLocation()
        needleAngle     = self.setLatLonForDistanceAndAngle(location!)
        
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error " + error.localizedDescription)
    }
    
    func setLatLonForDistanceAndAngle(userlocation: CLLocation) -> Double
    {
        let lat1 = DegreesToRadians(userlocation.coordinate.latitude)
        let lon1 = DegreesToRadians(userlocation.coordinate.longitude)
        let lat2 = DegreesToRadians(kabahLocation!.coordinate.latitude)
        let lon2 = DegreesToRadians(kabahLocation!.coordinate.longitude)
        
        distanceFromKabah = userlocation.distanceFromLocation(kabahLocation!)
        let dLon = lon2 - lon1;
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        var radiansBearing = atan2(y, x)
        if(radiansBearing < 0.0)
        {
            radiansBearing += 2*M_PI;
        }
        
        return radiansBearing
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
        let needleDirection   = -newHeading.trueHeading;
        let compassDirection  = -newHeading.magneticHeading;
        
       // self.needle.transform = CGAffineTransformMakeRotation(CGFloat(((Double(needleDirection) * M_PI) / 180.0) + needleAngle!))
        self.compass.transform = CGAffineTransformMakeRotation(CGFloat((Double(compassDirection) * M_PI) / 180.0))
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.locationManger.delegate = nil
    }
    
    // MARK: Custom Button Action
    
    @IBAction func pdfReaderButtonAction(sender: AnyObject) {
        let pdfReaderVC = self.storyboard?.instantiateViewControllerWithIdentifier("QRPdfReaderViewController") as! QRPdfReaderViewController
        self.navigationController?.pushViewController(pdfReaderVC, animated: true)
    }
    
    
    @IBAction func mapButtonAction(sender: AnyObject) {
        let googlePlacesViewController = self.storyboard?.instantiateViewControllerWithIdentifier("QRGooglePlacesViewController") as! QRGooglePlacesViewController
        self.navigationController?.pushViewController(googlePlacesViewController, animated: true)
        
    }
    
    @IBAction func prayerButtonAction(sender: AnyObject) {
        let prayerVC = self.storyboard?.instantiateViewControllerWithIdentifier("QRPrayerListViewController") as! QRPrayerListViewController
        self.navigationController?.pushViewController(prayerVC, animated: true)
    }
    @IBAction func aboutUsButtonAction(sender: AnyObject) {
        
        if Localisator.sharedInstance.currentLanguage != "English_en"{
            let aboutVC = self.storyboard?.instantiateViewControllerWithIdentifier("QRAboutUsViewController") as! QRAboutUsViewController
            self.navigationController?.pushViewController(aboutVC, animated: true)
        } else {
            let aboutVC = self.storyboard?.instantiateViewControllerWithIdentifier("QRAboutUsEngViewController") as! QRAboutUsEngViewController
            self.navigationController?.pushViewController(aboutVC, animated: true)
        }
        
    }
    
    // MARK: Get Current Time Stamp
    
    /*
     *
     *This function get the current time stamp according the time zone passed in url
     */
    
//    func getCurrentTimeStamp(){
//        
//        prefs.setValue(NSDate(), forKey: LAST_DATE_FETCHED_PRAYER_TIME)
//        
//        QRAppServices.performGetRequestWithoutParameter("http://api.aladhan.com/currentTimestamp?zone=\(timeZone)",
//                                                        
//                                                        successBlock: { (response, headers) -> Void in
//                                                            
//                                                            let responseData = response as! NSDictionary
//                                                            let dataVal = responseData["data"] as! NSNumber
//                                                            self.callWebServicetoGetPrayerTimings()
//                                                            
//                                                            
//            },errorBlock:  { (errorMessage, response, headers) -> Void in
//                
//                print(errorMessage)
//                
//                
//                self.showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
//        })
//    }
    
    /**************** Calling webservice for prayer timings ******************/
    func callWebServicetoGetPrayerTimings()  {
        
        self.view.showLoader()
        globalPrayer.prayerArray.removeAll(keepCapacity: false)
        QRAppServices.performGetRequestWithoutParameter("http://flagshippro.com/apps/aloula/API/prayer_timings",
                                                        
                                                        successBlock: {(response, headers) -> Void in
                                                            
                                                            print(response)
                                                            if response["code"] as! Int == 200{
                                                                let responseData = response as! NSDictionary
                                                                let data = responseData["data"] as! NSDictionary
                                                                let timings_data = data["timings"] as! NSDictionary
                                                                
                                                                for(key , value) in timings_data{
                                                                    
                                                                    let timing = [
                                                                        "timing": "\(value)".getDateFormTimeString(),
                                                                        "minutes": "\(value)".getMinutesFromTimeString(),
                                                                        "name": "\(key)",
                                                                        "arabicName" :"\(self.arabicsForPrayer[key as! String]!)"
                                                                    ]
                                                                    
                                                                    self.timingsData.append(timing)
                                                                }
                                                                let descriptor: NSSortDescriptor = NSSortDescriptor(key: "minutes", ascending: true)
                                                                self.timingsData = (self.timingsData as NSArray).sortedArrayUsingDescriptors([descriptor])
                                                                
                                                                print(self.timingsData)
                                                                self.scheduleLocalNotificationForPrayerTiming(self.timingsData)
                                                                self.initNextPrayer()
                                                            }
                                                            self.view.hideLoader()
                                                            
                                                            
            }, errorBlock: {(errorMessage, response, headers) -> Void in
                
                print(errorMessage)
                self.view.hideLoader()
                
                self.showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
                
               // self.showAlertWithMessage(kError_NoInternetConnection, andTitle: "خطأ")
                
        })
        
    }
    
    
    func setScroolView(){
        self.scrollView = UIScrollView()
        self.scrollView.delegate = self
        self.scrollView.contentSize = CGSizeMake(screenWidth*2, pageView.frame.size.height)
        
        self.scrollView.showsHorizontalScrollIndicator = false
        pageView.addSubview(self.scrollView)
        self.scrollView.pagingEnabled = true
        
         swipeFirstViewController = self.storyboard?.instantiateViewControllerWithIdentifier("QRSwipeFirstViewController") as! QRSwipeFirstViewController
        
        self.addChildViewController(swipeFirstViewController)
        scrollView.addSubview(swipeFirstViewController.view)
        swipeFirstViewController.didMoveToParentViewController(self)
        
        
         swipeSecondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("QRSwipeSecondViewController") as! QRSwipeSecondViewController
        
        self.addChildViewController(swipeSecondViewController)
        scrollView.addSubview(swipeSecondViewController.view)
        swipeSecondViewController.didMoveToParentViewController(self)
        setPageController()
        NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(QRHomeViewController.swipe), userInfo: nil, repeats: false)
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView){
         print(scrollView.currentPage)
         pageControl.currentPage = scrollView.currentPage - 1
    }

    func setPageController() {
        pageControl.currentPageIndicatorTintColor = ColorFromHexaCode(k_Red_Color)
        pageControl.pageIndicatorTintColor = UIColor.blackColor()
        pageControl.currentPage = 0;
        //pageControl.backgroundColor = UIColor.yellowColor()
        pageControl.numberOfPages = 2;
        pageControl.userInteractionEnabled = false
       
        pageView.addSubview(pageControl)

    }
    
    
    func swipe() {
        UIView.animateWithDuration(0.5) {
            self.scrollView.contentOffset = CGPoint(x: 150, y: 0)
            
        }
        NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(QRHomeViewController.swipeReverse), userInfo: nil, repeats: false)
        
    }
    
    func swipeReverse() {
        UIView.animateWithDuration(0.5) {
            self.scrollView.contentOffset = CGPoint(x: 0, y: 0)
            
        }
    }

    
    
    override func viewDidLayoutSubviews() {
       
        var y : CGFloat = 16
        
        if screenHeight == 480{
            y = 10
        }
        else  if screenWidth == 375 || screenWidth == 414{
            y = 26
        } else if isPad{
            y = 43
        }
        
        pageControl.frame = CGRectMake(pageView.frame.size.width/2-15, pageView.frame.size.height-y, 30, 15)
        
        
        scrollView.frame = CGRectMake(0, 0, screenWidth, pageView.frame.size.height)
        
        
        
        self.scrollView.contentSize = CGSizeMake(screenWidth*2, scrollView.frame.size.height)
        

        swipeFirstViewController.view.frame = CGRectMake(0, 0, screenWidth, pageView.frame.size.height)
        swipeSecondViewController.view.frame = CGRectMake(screenWidth, 0, screenWidth, pageView.frame.size.height)
       // pageControl.backgroundColor = UIColor.greenColor()
    }
    
    

}
