//
//  QRInfoViewController.swift
//  QuranRadio
//
//  Created by daffolapmac on 30/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRInfoViewController: QRBaseViewViewController {
    @IBOutlet var lblInfoDetail: UILabel!
    var infoStr : String!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setBackButton()
        setBackButtonLayout()
        self.title = Localization("k_Info_Title")
        lblInfoDetail.text = infoStr
       
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            lblInfoDetail.textAlignment = .Right
        }else{
             lblInfoDetail.textAlignment = .Left
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
