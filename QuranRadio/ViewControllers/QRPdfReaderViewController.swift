//
//  QRPdfReaderViewController.swift
//  QuranRadio
//
//  Created by daffolapmac on 20/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import MBProgressHUD
class QRPdfReaderViewController: QRBaseViewViewController,UIWebViewDelegate,ReaderViewControllerDelegate,NSURLSessionDownloadDelegate,UIDocumentInteractionControllerDelegate {

    @IBOutlet var progressView: CircleProgressView!
    var downloadTask: NSURLSessionDownloadTask!
    var backgroundSession: NSURLSession!
    var documentFileName : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setBackButton()
        if Localisator.sharedInstance.currentLanguage != "DeviceLanguage"{
            configureViewFromLocalisation()
        }
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "CPDFontAL", size: 18)!]

        let pdf  = NSBundle.mainBundle().pathForResource("Quraan", ofType: "pdf")
           // initPdfViewController(pdf! as String)
    

        if NetworkConnectivity.isConnectedToNetwork()
        {
          callServiceToGetPdfLink()
            
            
        }
        else
        {
            self.showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
        }

    }
    
    func callServiceToGetPdfLink()
    {
        self.view.showLoader()
    
        QRAppServices.getServiceRequest(urlString: Url_Pdf_link, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            let response = responseData as! NSDictionary
             print(response)
            if response["code"] as! Int == 200 {
                let linkDic = response["data"] as! NSDictionary
              self.getQuranPdfFile(linkDic["link"] as! String)
            }
            
        }) { (errorMessage) -> () in
            
            self.view.hideLoader()
            self.showAlertWithMessage(Localization("Server_Error"), andTitle: Localization("Error"))
        }

    }
    
    func getQuranPdfFile(urlStr:String){
       
        documentFileName = (urlStr as NSString).lastPathComponent
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = NSFileManager()
        let destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/\(documentFileName)"))
        
        if fileManager.fileExistsAtPath(destinationURLForFile.path!){
            
            initPdfViewController(destinationURLForFile.path!)
            
        }
        else{
            
        let backgroundSessionConfiguration = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier("backgroundSession")
        backgroundSession = NSURLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        let url = NSURL(string:urlStr )!
        downloadTask = backgroundSession.downloadTaskWithURL(url)
        downloadTask.resume()
        progressView.hidden = false
            
        }
     }
    
   
    func initPdfViewController(pdfPath:String){
        
        let filePath = pdfPath
        
        let document = ReaderDocument.withDocumentFilePath(filePath, password: "")
        if document != nil {
            let readerViewController: ReaderViewController = ReaderViewController(readerDocument: document)
            readerViewController.delegate = self
            readerViewController.modalTransitionStyle = .CrossDissolve
            readerViewController.modalPresentationStyle = UIModalPresentationStyle.FullScreen
            let navController = UINavigationController(rootViewController: readerViewController)
            navController.navigationItem.title = ""
            
            self.addChildViewController(readerViewController)
            self.view.addSubview(readerViewController.view)
            readerViewController.didMoveToParentViewController(self)
            
            
        }
        else {
           // NSLog("%s [ReaderDocument withDocumentFilePath:'%@' password:'%@'] failed.", #function, filePath, phrase!)
        }
    }
    
    
    func dismissReaderViewController(viewController: ReaderViewController!) {
        
    }

    func configureViewFromLocalisation() {
        
        setBackButtonLayout()
        self.title = Localization("Holy_Quran_Title")
    }
   
    func URLSession(session: NSURLSession,
                    downloadTask: NSURLSessionDownloadTask,
                    didFinishDownloadingToURL location: NSURL){
        
        progressView.hidden = true
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = NSFileManager()
        let destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/\(documentFileName)"))
      
        if fileManager.fileExistsAtPath(destinationURLForFile.path!){
            initPdfViewController(destinationURLForFile.path!)
        }
        else{
            do {
                try fileManager.moveItemAtURL(location, toURL: destinationURLForFile)
                initPdfViewController(destinationURLForFile.path!)
            }catch{
                print("An error occurred while moving file to destination url")
            }
        }
    }
   
    func URLSession(session: NSURLSession,
                    downloadTask: NSURLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                                 totalBytesWritten: Int64,
                                 totalBytesExpectedToWrite: Int64){
        
        
        self.progressView.progress = Double(Double(totalBytesWritten)/Double(totalBytesExpectedToWrite))
       
    }
    
    
    func URLSession(session: NSURLSession,
                    task: NSURLSessionTask,
                    didCompleteWithError error: NSError?){
        
        cancelSession()
        if (error != nil) {
            print(error?.description)
        }else{
            print("The task finished transferring data successfully")
        }
    }
    
    func documentInteractionControllerViewControllerForPreview(controller: UIDocumentInteractionController) -> UIViewController{
        return self
    }

   deinit{
    cancelSession()
    }

    func cancelSession(){
        if downloadTask != nil{
            downloadTask.cancel()
            downloadTask = nil
        }
        if self.backgroundSession != nil{
            self.backgroundSession.finishTasksAndInvalidate()
        }

        
    }
    override func viewWillDisappear(animated: Bool) {
        cancelSession()
    }
}
