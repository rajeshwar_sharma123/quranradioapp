//
//  QRPrayerListViewController.swift
//  QuranRadio
//
//  Created by Daffomacbook-25 on 23/05/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import EventKit
//import DGRunkeeperSwitch
class QRPrayerListViewController: QRBaseViewViewController,UITableViewDelegate,UITableViewDataSource, prayerNotificationDelegate {
    
    var notificationCheck = false
    @IBOutlet weak var lblPrayerTime: QRLabel!
    @IBOutlet weak var lblPrayerName: QRLabel!
    @IBOutlet weak var tblPrayerList: UITableView!
    //var  notificationBtn : UIButton!
    @IBOutlet var lblNextPrayer: UILabel!
    var nextPrayer:Prayer?
    var isBold = false
    var nextBoldIndext:Int?
    var index:Int?
    
    @IBOutlet var lblPrayerTimeHeightConstraint: NSLayoutConstraint!
    @IBOutlet var lblPrayerNameHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarColor()
        tblPrayerList.dataSource = self
        tblPrayerList.delegate = self
        setBackButton()
        //setNotificationButton()
        //setNotificationBtnLayout()
        if Localisator.sharedInstance.currentLanguage != "DeviceLanguage"{
            configureViewFromLocalisation()
        }
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "CPDFontAL", size: 18)!]
        let nibName = UINib(nibName: "QRPrayerListTableViewCell", bundle:nil)
        self.tblPrayerList.registerNib(nibName, forCellReuseIdentifier: "QRPrayerListTableViewCell")
        nextBoldIndext = nextBoldIndex()
        self.initPrayerDetails()
        setCustomSize()
        
    if(( NSUserDefaults.standardUserDefaults().objectForKey("DefaultAlarm")) == nil && globalPrayer.prayerArray.count != 0){
        
            setPrayerAlarm()
        }
    }
    
    func setPrayerAlarm(){
         NSUserDefaults.standardUserDefaults().setObject(true, forKey: "DefaultAlarm")
        for index in 0..<globalPrayer.prayerArray.count {
            let prayer = globalPrayer.prayerArray[index]
            setPrayerForFirstTime(prayer, alarmStatus: alarmStatus(prayer.prayerName))
        }

    }
    
    func setPrayerForFirstTime(prayer:Prayer, alarmStatus:Bool) {
        
        
        
        if NSUserDefaults.standardUserDefaults().boolForKey("isNotificationOn") == true && !alarmStatus{
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd MM yyyy"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
            let currentDateString = dateFormatter.stringFromDate(NSDate())

            self.updateAlarm(prayer.prayerName, prayerTime: prayer.PrayerTime)
            let nameString = prayer.prayerName
            if  nameString != "Sunrise" {
                let timeString = prayer.PrayerTime
                let nameString = prayer.prayerName
                let dateString = "\(currentDateString) \(timeString)"
                dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                dateFormatter.dateFormat = "dd MM yyyy hh:mm a"
                dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
                let scheduleTime = dateFormatter.dateFromString(dateString)
                let localNotification:UILocalNotification = UILocalNotification()
                
                if #available(iOS 8.2, *) {
                    localNotification.alertTitle = " "
                    localNotification.alertBody = getLocalNotificationAlarmMessage(nameString)
                } else {
                    // Fallback on earlier versions
                    localNotification.alertBody = getLocalNotificationAlarmMessage(nameString)
                }
                
                
                localNotification.repeatInterval = NSCalendarUnit.Day
                localNotification.soundName = "QuranAPP.wav"
                var userInfo = [String:AnyObject]()
                let keyValue = 2
                userInfo["tag"] = keyValue
                userInfo["prayerName"] =  nameString
                userInfo["prayerTime"] = timeString
                localNotification.userInfo = userInfo
                localNotification.timeZone = NSTimeZone.systemTimeZone()
                localNotification.fireDate = scheduleTime
                UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
            }
        }
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        tblPrayerList.reloadData()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    func setCustomSize(){
        if screenWidth == 320{
            lblPrayerTime.font = UIFont(name: (lblPrayerTime.font?.fontName)!, size: 40)
          
        }
        if screenHeight == 480{
            lblPrayerNameHeightConstraint.constant = 0
            lblPrayerTimeHeightConstraint.constant = -4
        }
    }
    
    /************ scheduling local notification on basis of notification toggel button **********/
    func scheduleLocalNotificationForPrayerTimingOnSwitchChanged(){
        
        if NSUserDefaults.standardUserDefaults().boolForKey("isNotificationOn") == true{
            
            let dateFormatter = NSDateFormatter()
            let calendar = NSCalendar.autoupdatingCurrentCalendar()
            dateFormatter.dateFormat = "dd MM yyyy"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
            let currentDateString = dateFormatter.stringFromDate(NSDate())
            for index in 0 ..< globalPrayer.prayerArray.count{
                
                let objDate = globalPrayer.prayerArray[index]
                let nameString = objDate.prayerName
                if nameString != "Sunset"{
                    let objDate = globalPrayer.prayerArray[index]
                    let timeString = objDate.PrayerTime
                    let nameString = objDate.prayerName
                    sechedulNotificationForAlarm(nameString, prayerTime: timeString)
                  
                    
                    /*let dateString = "\(currentDateString) \(timeString)"
                    dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                    dateFormatter.dateFormat = "dd MM yyyy hh:mm a"
                    dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
                    var scheduleTime = dateFormatter.dateFromString(dateString)
                    scheduleTime = calendar.dateByAddingUnit(.Minute, value: -3, toDate: scheduleTime!, options:[])
                    let localNotification:UILocalNotification = UILocalNotification()
                    
                    if #available(iOS 8.2, *) {
                        localNotification.alertTitle = " "
                        localNotification.alertBody = getLocalNotificationAlarmMessage(nameString)
                    } else {
                        // Fallback on earlier versions
                        localNotification.alertBody = getLocalNotificationAlarmMessage(nameString)
                    }

                    localNotification.repeatInterval = NSCalendarUnit.Day
                    localNotification.soundName = UILocalNotificationDefaultSoundName
                    var userInfo = [String:AnyObject]()
                    let keyValue = 1
                    userInfo["tag"] = keyValue
                    userInfo["prayerName"] = nameString
                    userInfo["prayerTime"] = timeString
                    localNotification.userInfo = userInfo
                    localNotification.timeZone = NSTimeZone.systemTimeZone()
                    localNotification.fireDate = scheduleTime
                    UIApplication.sharedApplication().scheduleLocalNotification(localNotification)*/
                    
                }
            }
        }}
    
    
    //MARK: - Next Bold
    
    func nextBoldIndex()->Int{
        var count = 0
        
        if globalPrayer.prayerArray.count > 0 {
            for prayer in globalPrayer.prayerArray{
                
                let prayerDate = getDateFromTime(prayer.PrayerTime)
                
                if prayerDate.isGreaterThanDate(NSDate()){
                    break
                }
                count += 1
            }
        }
        return count
        
    }
    
    func configureViewFromLocalisation() {
        setBackButtonLayout()
        self.title = Localization("Prayer_Timings_Title")
        lblNextPrayer.text = Localization("Next_Prayer")
        tblPrayerList.reloadData()
    }
    
    
    
    func initPrayerDetails(){
        
        if globalPrayer.prayerArray.count > 0 {
            nextPrayer = getNextPrayer()
            
            if nextPrayer!.prayerName == "" {
                nextPrayer = globalPrayer.prayerArray[0]
            }
            
            self.lblPrayerName.text = Localization(nextPrayer!.prayerName).uppercaseString
            
            if nextPrayer!.PrayerTime.characters.count != 0{
                self.lblPrayerTime.text = get24HourFormat(getDateFromTime(nextPrayer!.PrayerTime))
            }
        } else{
            
            if NetworkConnectivity.isConnectedToNetwork() == true {
                self.callWebServicetoGetPrayerTimings()
                
                return
            }
            
            self.showAlert()
            
        }
    }
    
    //MARK: - Prayer Delegate
    
    func prayerAlamTapped(index: Int) {
        self.index = index
        
        let prayer = globalPrayer.prayerArray[index]
        
        print(index)
        
        createEvent(prayer, alarmStatus: alarmStatus(prayer.prayerName))
        
    }
    
    /************ enabel/disabel alarm for prayer times   **********/
    func createEvent(prayer:Prayer, alarmStatus:Bool){
        if alarmStatus {
            if NSUserDefaults.standardUserDefaults().objectForKey("switchValue") as! String == "0"{
                let cell:QRPrayerListTableViewCell = self.tblPrayerList.cellForRowAtIndexPath(NSIndexPath(forItem: self.index!, inSection: 0)) as! QRPrayerListTableViewCell
                cell.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_grey"), forState: .Normal)
                self.updateAlarm(prayer.prayerName, prayerTime: prayer.PrayerTime)
                return
            }
            
            let app:UIApplication = UIApplication.sharedApplication()
            
            if prayer.prayerName == "Sunrise"{
                let cell:QRPrayerListTableViewCell = self.tblPrayerList.cellForRowAtIndexPath(NSIndexPath(forItem: self.index!, inSection: 0)) as! QRPrayerListTableViewCell
                cell.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_grey"), forState: .Normal)
                self.updateAlarm(prayer.prayerName, prayerTime: prayer.PrayerTime)
                return
            }
            
            
            for oneEvent in app.scheduledLocalNotifications! {
                
                let notification = oneEvent as UILocalNotification
                let userInfoCurrent = notification.userInfo! as! [String:AnyObject]
                let prayerNameTmp =  userInfoCurrent["prayerName"] as! String
                let prayerTag  =  userInfoCurrent["tag"] as! Int
                print(prayerTag)
                print(prayerNameTmp)
                if prayerNameTmp == prayer.prayerName && prayerTag == 2 {
                    //Cancelling local notification
                    app.cancelLocalNotification(notification)
                    let cell:QRPrayerListTableViewCell = self.tblPrayerList.cellForRowAtIndexPath(NSIndexPath(forItem: self.index!, inSection: 0)) as! QRPrayerListTableViewCell
                    cell.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_grey"), forState: .Normal)
                    self.updateAlarm(prayer.prayerName, prayerTime: prayer.PrayerTime)
                    break;
                }
            }
            
            
        } else {
            if NSUserDefaults.standardUserDefaults().objectForKey("switchValue") as! String == "0"{
                let cell:QRPrayerListTableViewCell = tblPrayerList.cellForRowAtIndexPath(NSIndexPath(forItem: index!, inSection: 0)) as! QRPrayerListTableViewCell
                cell.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_red"), forState: .Normal)
                self.updateAlarm(prayer.prayerName, prayerTime: prayer.PrayerTime)
                return
            }
            
            if NSUserDefaults.standardUserDefaults().boolForKey("isNotificationOn") == true{
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd MM yyyy"
                dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
                let currentDateString = dateFormatter.stringFromDate(NSDate())
                let cell:QRPrayerListTableViewCell = tblPrayerList.cellForRowAtIndexPath(NSIndexPath(forItem: index!, inSection: 0)) as! QRPrayerListTableViewCell
                cell.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_red"), forState: .Normal)
                self.updateAlarm(prayer.prayerName, prayerTime: prayer.PrayerTime)
                let nameString = prayer.prayerName
                if  nameString != "Sunrise" {
                    let timeString = prayer.PrayerTime
                    let nameString = prayer.prayerName
                    let dateString = "\(currentDateString) \(timeString)"
                    dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                    dateFormatter.dateFormat = "dd MM yyyy hh:mm a"
                    dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
                    let scheduleTime = dateFormatter.dateFromString(dateString)
                    let localNotification:UILocalNotification = UILocalNotification()
                    if #available(iOS 8.2, *) {
                        localNotification.alertTitle = " "
                        localNotification.alertBody = getLocalNotificationAlarmMessage(nameString)
                    } else {
                        // Fallback on earlier versions
                        localNotification.alertBody = getLocalNotificationAlarmMessage(nameString)
                    }

                    localNotification.repeatInterval = NSCalendarUnit.Day
                    localNotification.soundName = "QuranAPP.wav"
                    var userInfo = [String:AnyObject]()
                    let keyValue = 2
                    userInfo["tag"] = keyValue
                    userInfo["prayerName"] =  nameString
                    userInfo["prayerTime"] = timeString
                    localNotification.userInfo = userInfo
                    localNotification.timeZone = NSTimeZone.systemTimeZone()
                    localNotification.fireDate = scheduleTime
                    UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                }
            }
        }
    }
    
    
    
    
    func setNewAlarm(prayer:Prayer){
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MM yyyy"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        let currentDateString = dateFormatter.stringFromDate(NSDate())
        
        let timeString = prayer.PrayerTime
        
        let dateString = "\(currentDateString) \(timeString)"
        dateFormatter.timeZone = NSTimeZone.systemTimeZone()
        dateFormatter.dateFormat = "dd MM yyyy hh:mm a"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        var prayerDate = dateFormatter.dateFromString(dateString)
        prayerDate = NSDate()
        let calendar = NSCalendar.autoupdatingCurrentCalendar()
        prayerDate = calendar.dateByAddingUnit(.Minute, value: +2, toDate: prayerDate!, options:[])
        
        if prayerDate!.compare(NSDate()) == .OrderedAscending {
            // if the time chosen is such that the date is in the past, go to tomorrow
            prayerDate = NSCalendar.autoupdatingCurrentCalendar().dateByAddingUnit(.Day, value: 1, toDate: prayerDate!, options: [])!
        }
        
    }
    
    
    
    func addEventToCalender( program:Prayer)
    {
        print(program.PrayerTime)
        
        print(getDateFromTime(program.PrayerTime))
        
        let reminderDate = getDateFromTime(program.PrayerTime)
        
        
        if(EKEventStore.authorizationStatusForEntityType(EKEntityType.Reminder) == EKAuthorizationStatus.Authorized)
        {
            
            let reminder = EKReminder(eventStore: self.eventStore)
            reminder.title = "\(program.prayerName) [\(program.PrayerTime)] Quran Radio"
            
            let recur = EKRecurrenceRule(recurrenceWithFrequency: .Daily, interval: 1, end: nil)
            // let now: NSDate = NSDate().dateByAddingTimeInterval(60*2)
            let alarm = EKAlarm(absoluteDate: reminderDate)
            
            
            
            let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            let components: NSDateComponents = calendar.components([.Year, .Month, .Day], fromDate: reminderDate)
            
            reminder.dueDateComponents = components
            reminder.calendar = self.eventStore.defaultCalendarForNewReminders()
            reminder.addRecurrenceRule(recur)
            
            
            reminder.addAlarm(alarm)
            
            do
            {
                
                if self.index != nil {
                    print(index)
                    let cell:QRPrayerListTableViewCell = tblPrayerList.cellForRowAtIndexPath(NSIndexPath(forItem: index!, inSection: 0)) as! QRPrayerListTableViewCell
                    cell.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_red"), forState: .Normal)
                    try self.eventStore.saveReminder(reminder, commit: true)
                    let identifier = reminder.calendarItemIdentifier
                    print("Saved Event:  \(identifier)")
                    self.updateAlarm(program.prayerName, prayerTime: program.PrayerTime)
                    self.updateAlarmId(program.prayerName, uniqueAlarmId: identifier)
                    self.tblPrayerList.reloadData()
                    return
                }
                
            }
            catch
            {
                print("Doesnot save in reminder")
                
            }
        }
        else
        {
            self.eventStore.requestAccessToEntityType(EKEntityType.Reminder , completion: {
                (granted, error) in
                if (granted) && (error == nil)
                {
                    print("granted \(granted)")
                    print("error \(error)")
                    let recur = EKRecurrenceRule(recurrenceWithFrequency: .Daily, interval: 1, end: nil)
                    let reminder = EKReminder(eventStore: self.eventStore)
                    reminder.title = "\(program.prayerName) Quran Radio"
                    // let now: NSDate = NSDate().dateByAddingTimeInterval(60*2)
                    let alarm = EKAlarm(absoluteDate: reminderDate)
                    
                    reminder.addAlarm(alarm)
                    
                    let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
                    let components: NSDateComponents = calendar.components([.Year, .Month, .Day], fromDate: reminderDate)
                    
                    reminder.dueDateComponents = components
                    reminder.calendar = self.eventStore.defaultCalendarForNewReminders()
                    reminder.addRecurrenceRule(recur)
                    
                    do
                    {
                        if self.index != nil {
                            print(self.index)
                            let cell:QRPrayerListTableViewCell = self.tblPrayerList.cellForRowAtIndexPath(NSIndexPath(forItem: self.index!, inSection: 0)) as! QRPrayerListTableViewCell
                            cell.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_red"), forState: .Normal)
                            try self.eventStore.saveReminder(reminder, commit: true)
                            let identifier = reminder.calendarItemIdentifier
                            print("Saved Event:  \(identifier)")
                            self.updateAlarm(program.prayerName, prayerTime: program.PrayerTime)
                            self.updateAlarmId(program.prayerName, uniqueAlarmId: identifier)
                            self.tblPrayerList.reloadData()
                            return
                        }
                    }
                    catch
                    {
                        
                        
                    }
                }
            })
            
            
        }
        
        
        if (EKEventStore.authorizationStatusForEntityType(EKEntityType.Reminder)) == EKAuthorizationStatus.Denied || (EKEventStore.authorizationStatusForEntityType(EKEntityType.Reminder)) == EKAuthorizationStatus.NotDetermined {
            self.showSettingAlert()
        }
        
        
        
        
        
        
        
    }
    
    
    func removeEventToCalendar(prayer:Prayer){
        
        let uniqueId = getAlarmId(prayer.prayerName)
        print(uniqueId)
        
        
        if(EKEventStore.authorizationStatusForEntityType(EKEntityType.Reminder) == EKAuthorizationStatus.Authorized)
        {
            if uniqueId != "" {
                let reminder:EKReminder = eventStore.calendarItemWithIdentifier(uniqueId) as! EKReminder
                
                do{
                    let cell:QRPrayerListTableViewCell = tblPrayerList.cellForRowAtIndexPath(NSIndexPath(forItem: index!, inSection: 0)) as! QRPrayerListTableViewCell
                    cell.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_grey"), forState: .Normal)
                    self.updateAlarm(prayer.prayerName, prayerTime: prayer.PrayerTime)
                    try eventStore.removeReminder(reminder, commit: true)
                    return
                }catch{
                    fatalError("erro while removing reminder")
                }
                
                
            }
            
        }
        else
        {
            self.eventStore.requestAccessToEntityType(EKEntityType.Reminder , completion: {
                (granted, error) in
                if (granted) && (error == nil)
                {
                    if uniqueId != "" {
                        let reminder:EKReminder = self.eventStore.calendarItemWithIdentifier(uniqueId) as! EKReminder
                        
                        do{
                            let cell:QRPrayerListTableViewCell = self.tblPrayerList.cellForRowAtIndexPath(NSIndexPath(forItem: self.index!, inSection: 0)) as! QRPrayerListTableViewCell
                            cell.btnPrayerAlarm.setImage(UIImage(named: "ic_watch_grey"), forState: .Normal)
                            self.updateAlarm(prayer.prayerName, prayerTime: prayer.PrayerTime)
                            try self.eventStore.removeReminder(reminder, commit: true)
                            return
                        }catch{
                            fatalError("erro while removing reminder")
                        }
                        
                        
                    }
                    
                }
            })
            
            
        }
        
        if (EKEventStore.authorizationStatusForEntityType(EKEntityType.Reminder)) == EKAuthorizationStatus.Denied || (EKEventStore.authorizationStatusForEntityType(EKEntityType.Reminder)) == EKAuthorizationStatus.NotDetermined {
            self.showSettingAlert()
        }
        
        
        
        
    }
    
    
    
    func showSettingAlert(){
        
        let alertController = UIAlertController(
            title: Localization("Reminder_Disabled"),
            message: Localization("Reminder_Disabled_Message"),
            preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: Localization("cancel"), style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: Localization("Open_Settings"), style: .Default) { (action) in
            if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.sharedApplication().openURL(url)
            }
        }
        alertController.addAction(openAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    
    //MARK: - Table View Delegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        return 55
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return globalPrayer.prayerArray.count
    }
    
    
    //MARK: Table View Datasource
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("QRPrayerListTableViewCell") as! QRPrayerListTableViewCell
        
        let prayerObj = globalPrayer.prayerArray[indexPath.row]
        
        print(prayerObj.prayerName)
        print(prayerObj.PrayerTime)
        
        cell.selectionStyle = .None
        cell.delegate = self
        
        print(alarmStatus(prayerObj.prayerName))
        
        print(nextBoldIndex())
        
        if nextBoldIndex() == indexPath.row+1{
            isBold = true
        }else{
            isBold = false
        }
        
        cell.setCell(prayerObj.prayerName, time: prayerObj.PrayerTime, index: indexPath.row, isAlarmed: alarmStatus(prayerObj.prayerName), isBold: isBold)
        
        return cell
    }
    
    
    
    
    /**************** Calling webservice for prayer timings ******************/
    func callWebServicetoGetPrayerTimings()  {
        
        self.view.showLoader()
        
        globalPrayer.prayerArray.removeAll(keepCapacity: false)
        
        QRAppServices.performGetRequestWithoutParameter("http://flagshippro.com/apps/aloula/API/prayer_timings",
                                                        
                                                        successBlock: {(response, headers) -> Void in
                                                            
                                                            print(response)
                                                            if response["code"] as! Int == 200{
                                                                let responseData = response as! NSDictionary
                                                                let data = responseData["data"] as! NSDictionary
                                                                let timings_data = data["timings"] as! NSDictionary
                                                                
                                                                for(key , value) in timings_data{
                                                                    
                                                                    let timing = [
                                                                        "timing": "\(value)".getDateFormTimeString(),
                                                                        "minutes": "\(value)".getMinutesFromTimeString(),
                                                                        "name": "\(key)",
                                                                        "arabicName" :"\(self.arabicsForPrayer[key as! String]!)"
                                                                    ]
                                                                    
                                                                    self.timingsData.append(timing)
                                                                }
                                                                let descriptor: NSSortDescriptor = NSSortDescriptor(key: "minutes", ascending: true)
                                                                self.timingsData = (self.timingsData as NSArray).sortedArrayUsingDescriptors([descriptor])
                                                                
                                                                print(self.timingsData)
                                                                self.scheduleLocalNotificationForPrayerTiming(self.timingsData)
                                                                
                                                                if(( NSUserDefaults.standardUserDefaults().objectForKey("DefaultAlarm")) == nil && globalPrayer.prayerArray.count != 0){
                                                                    
                                                                    self.setPrayerAlarm()
                                                                }
                                                                
                                                                if globalPrayer.prayerArray.count > 0 {
                                                                    self.nextPrayer = self.getNextPrayer()
                                                                    
                                                                    if self.nextPrayer!.prayerName == "" {
                                                                        self.nextPrayer = globalPrayer.prayerArray[0]
                                                                    }
                                                                    
                                                                    self.lblPrayerName.text = Localization(self.nextPrayer!.prayerName).uppercaseString
                                                                    
                                                                    if self.nextPrayer!.PrayerTime.characters.count != 0{
                                                                        self.lblPrayerTime.text = self.get24HourFormat(self.getDateFromTime(self.nextPrayer!.PrayerTime))
                                                                    }
                                                                }
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                self.tblPrayerList.reloadData()
                                                                
                                                            }
                                                            
                                                            self.view.hideLoader()
                                                            
                                                            
            }, errorBlock: {(errorMessage, response, headers) -> Void in
                
                print(errorMessage)
                self.view.hideLoader()
                
                self.showAlert()
                
        })
        
        
    }
    
    
    func showAlert(){
        
        let alertController:UIAlertController?
        
        alertController = UIAlertController(title: Localization("Error"), message: Localization("No_data"), preferredStyle: .Alert)
        
        let OKAction = UIAlertAction(title: Localization("Ok"), style: .Default) { (action) in
            self.navigationController?.popViewControllerAnimated(true)
        }
        alertController!.addAction(OKAction)
        
        self.presentViewController(alertController!, animated: true){
            
        }
    }
    
}