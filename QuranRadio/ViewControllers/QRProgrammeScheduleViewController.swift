//
//  QRProgrammeScheduleViewController.swift
//  QuranRadio
//
//  Created by daffolapmac on 15/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
//import HMSegmentedControl
import MGSwipeTableCell
class QRProgrammeScheduleViewController: QRBaseViewViewController,UITableViewDelegate, UITableViewDataSource,MGSwipeTableCellDelegate {
    var segmentedControl:  HMSegmentedControl?
    var allProgramSchedArray: QRProgramListModel!
   // var programFilterButton: UIButton!
    var schedulesFromService: Array<AnyObject> = []
    var selectedDay : Int!
    var currentDay = 0
    var commonDayId = 0
    @IBOutlet var tableView: UITableView!
    
   
    @IBOutlet var lblProgramNowName: QRLabel!
    @IBOutlet var lblOnAirNow: UILabel!
    @IBOutlet var segmentedBarView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setBackButton()
       // setProgramFilterButton()
        self.selectedDay  = 0
        self.selectedDay = getCurrentDayID()
        commonDayId = getCommonCurrentDayID()
        currentDay = self.selectedDay
        tableView.reomveEmptyRows()
        configureViewFromLocalisation()
        setSegmentedControll()
        self.callServiceToGetProgramSchedulesOnScheduleScreen(selectedDay)
    }
    
    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
    }
    
//    func setProgramFilterButton(){
//        
//        
//        programFilterButton = UIButton(type: UIButtonType.Custom)
//        programFilterButton.frame = CGRectMake(0, 0, 60,  30) ;
//        programFilterButton.setTitle("filter", forState: .Normal)
//        programFilterButton.addTarget(self, action:#selector(QRProgrammeScheduleViewController.filterBtnClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
//        let scheduleProgramBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: programFilterButton)
//        
//        
//        if Localisator.sharedInstance.currentLanguage == "English_en"{
//            self.navigationItem.setRightBarButtonItem(scheduleProgramBarButtonItem, animated: false);
//            
//        }else{
//        self.navigationItem.setLeftBarButtonItem(scheduleProgramBarButtonItem, animated: false);
//        }
//
//        
//    }
    func filterBtnClicked(sender:UIButton!)
    {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("QRProgramFilterViewController")
        self.navigationController?.pushViewController(vc!, animated: true)
    }

    
    
    func configureViewFromLocalisation() {
        setBackButtonLayout()
        self.title = Localization("k_Program_Schedule_Title")
        lblOnAirNow.text = Localization("k_On_Air_Now")
        
    }
    func setSegmentedControll(){ //k_Sunday الأحد  الإثنين الثلاثاء
        
        var daysArray : [AnyObject]!
        var font = UIFont(name: "ProximaNova-Bold", size: 13)
        if Localisator.sharedInstance.currentLanguage == "English_en"{
            daysArray = ["SUN","MON","TUE","WED","THU","FRI","SAT"]
            
        }else{
       daysArray = ["السبت","الجمعة","الخميس","الأربعاء","الثلاثاء","الإثنين","الأحد"]
            font = UIFont(name: "ProximaNova-Bold", size: 15)
        }
       segmentedControl = HMSegmentedControl(sectionTitles: daysArray)
       segmentedControl!.backgroundColor = UIColor.clearColor()
        segmentedControl!.selectionIndicatorColor = ColorFromHexaCode(k_Bright_Red_Color)
        segmentedControl!.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown
        segmentedControl!.frame = CGRectMake(0,0,self.view.frame.width,segmentedBarView.frame.size.height)
        segmentedControl?.selectionStyle = HMSegmentedControlSelectionStyleBox
        segmentedControl!.selectionIndicatorBoxOpacity = 1
        //segmentedControl!.sectionSelectedImages = UIImage(named: "ic_segment_bg")
        
        
        if let font = font {
            segmentedControl?.titleTextAttributes = [NSFontAttributeName : font, NSForegroundColorAttributeName : UIColor.blackColor()]
        }
        if let font = font {
            segmentedControl?.selectedTitleTextAttributes = [NSFontAttributeName : font, NSForegroundColorAttributeName : UIColor.whiteColor()]
        }
        segmentedControl!.addTarget(self, action: #selector(QRProgrammeScheduleViewController.segmentedControlChangedValue(_:)),forControlEvents: .ValueChanged)
        
        segmentedControl!.selectedSegmentIndex = selectedDay
        
        segmentedBarView.addSubview(segmentedControl!)
        
    }
    
    
    func segmentedControlChangedValue(sender: UISegmentedControl)
    {
 
        selectedDay  = sender.selectedSegmentIndex
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
          commonDayId = 6 - sender.selectedSegmentIndex
        }else{
           
            commonDayId =  sender.selectedSegmentIndex
        }
        
        self.initProgramSchedulesOnTheDay(self.selectedDay!)
        
    }
    
    func callServiceToGetProgramSchedulesOnScheduleScreen(day:Int){
        self.view.showLoader()
        
        QRAppServices.getServiceRequest(urlString: QRServiceUrls.getCompleteUrlFor(Url_Program_Schedule), successBlock: { (responseData) -> () in
            self.view.hideLoader()
            let response = responseData as! NSDictionary
            
            print(response)
            if response["code"] as! Int == 200 {
                
                self.allProgramSchedArray = QRProgramListModel(fromDictionary: responseData["data"] as! NSDictionary)
                self.initProgramSchedulesOnTheDay(day)
                self.getTodaysCurrentProgrammName(day)
            }
            
            
            
        }) { (errorMessage) -> () in
            
            self.view.hideLoader()
            self.showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
        }
        
    }
    
    func getTodaysCurrentProgrammName(day:Int){
        var programName = ""
        var arr : Array<AnyObject> = []
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            arr =  (allProgramSchedArray.allDayPrograms[6-day] as? Array<AnyObject>)!
        }else{
            arr =  (allProgramSchedArray.allDayPrograms[day] as? Array<AnyObject>)!
            
        }
        
        for i in 0..<arr.count {
            let  programModel = arr[i] as! QRProgramModel
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd MM yyyy"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
            let currentDateString = dateFormatter.stringFromDate(NSDate())
            
            
            var  timeString = self.convertTimeIn12HourFormat(programModel.startTime!)
            var  dateString = "\(currentDateString) \(timeString)"
            print(dateString)
            dateFormatter.timeZone = NSTimeZone.systemTimeZone()
            dateFormatter.dateFormat = "dd MM yyyy hh:mma"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
            let startTime = dateFormatter.dateFromString(dateString)
            

            timeString = self.convertTimeIn12HourFormat(programModel.endTime!)
             dateString = "\(currentDateString) \(timeString)"
            let endTime = dateFormatter.dateFromString(dateString)
            
            let currentTime  = NSDate()
            if currentTime.isGreaterThanDate(startTime!) && currentTime.isLessThanDate(endTime!){
                
                if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
                    
                    programName = programModel.programNameArabic
                }else{
                    programName = programModel.programNameEnglish
                    
                }
                
                
            }

            
        }
        
        if programName.isEmpty{
            lblProgramNowName.text = Localization("k_No_Program_Available").uppercaseString
        }else{
            lblProgramNowName.text = programName.uppercaseString
        }
        
 
    }
    
    func initProgramSchedulesOnTheDay(day:Int)
    {
        if let schedule = allProgramSchedArray?.allDayPrograms[day]
        {
            if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
                self.schedulesFromService =  (allProgramSchedArray.allDayPrograms[6-day] as? Array<AnyObject>)!
            }else{
                self.schedulesFromService =  (allProgramSchedArray.allDayPrograms[day] as? Array<AnyObject>)!
                
            }
        }
        
        self.tableView.reloadData()
    }
    
    func getCurrentDayID()->Int
    {
        var daysInTheWeek : Array=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            daysInTheWeek = daysInTheWeek.reverse()
        }
        
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayOfWeek = dateFormatter.stringFromDate(date)
        return daysInTheWeek.indexOf (dayOfWeek)!
    }
    func getCommonCurrentDayID()->Int
    {
        var daysInTheWeek : Array=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            daysInTheWeek = daysInTheWeek.reverse()
        }
        
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayOfWeek = dateFormatter.stringFromDate(date)
        
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            return  6 - daysInTheWeek.indexOf (dayOfWeek)!
        }else{
            return daysInTheWeek.indexOf (dayOfWeek)!
        }
        
        
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- TableView DataSource & Delegates Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.schedulesFromService.count
    }
    /*
     Tells the delegate that the table view is about to draw a cell for a particular row.
     */
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
                   forRowAtIndexPath indexPath: NSIndexPath)
    {
        
        // Remove separator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var reuseIdentifier = "QRProgramScheduleEnglishCell" 
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            reuseIdentifier = "QRProgramScheduleArabicCell"
        }
        var cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) as! QRProgramScheduleCell!
        
        if cell == nil
        {
            
            cell = QRProgramScheduleCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: reuseIdentifier)
        }
        cell.selectionStyle = .None
       // cell.delegate = self
        let  programModel = schedulesFromService[indexPath.row] as! QRProgramModel
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            cell.lblProgramName.text = programModel.programNameArabic
           // cell.lblTime.text = self.convertTimeIn12HourFormat(programModel.endTime!)+"-"+self.convertTimeIn12HourFormat(programModel.startTime!)
        }
        else{
            cell.lblProgramName.text = programModel.programNameEnglish
            //cell.lblTime.text = self.convertTimeIn12HourFormat(programModel.startTime!)+"-"+self.convertTimeIn12HourFormat(programModel.endTime!)
        }
        cell.lblTime.text = self.convertTimeIn12HourFormat(programModel.startTime!)+"-"+self.convertTimeIn12HourFormat(programModel.endTime!)
        let url = NSURL(string: programModel.image)
        
        let img = UIImage(named: "ic_program_placeholder")
        cell.programImgView.sd_setImageWithURL(url, placeholderImage: img) {
            (img, err, cacheType, imgUrl) -> Void in
            // Do awesome things
            print("cached data")
            
        }
       cell.btnReminder.addTarget(self, action:#selector(QRProgrammeScheduleViewController.reminderButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        programModel.dayId = commonDayId
        let program = ProgramScheduleManager.alreadyAddedProgramDetails( programModel.dayId!,programmeID: programModel.programmeId!)
        if( program != nil)
        {
            
            cell.btnReminder.setImage(UIImage(named:"ic_watch_red"), forState: .Normal)
        }
        else
        {
           
            cell.btnReminder.setImage(UIImage(named:"ic_watch_grey"), forState: .Normal)
        }
        return cell
    }
    func swipeTableCell(cell: MGSwipeTableCell, tappedButtonAtIndex index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool
    {
        
        
        return true
    }
    
    func removeProgramScheduleNotification(programModel:QRProgramModel) {
        let app:UIApplication = UIApplication.sharedApplication()
        for oneEvent in app.scheduledLocalNotifications! {
            let notification = oneEvent as UILocalNotification
            let userInfoCurrent = notification.userInfo! as! [String:AnyObject]
            
            let tag =  userInfoCurrent["tag"] as! Int
            if tag == 3{
            
            let dayId =  userInfoCurrent["dayId"] as! Int
            let programId  =  userInfoCurrent["programmeId"] as! Int
            if dayId == programModel.dayId && programId == programModel.programmeId{
                //Cancelling local notification
                app.cancelLocalNotification(notification)
                 ProgramScheduleManager.removeProgramSchedule(programModel.dayId!, programmeID: programModel.programmeId!)
              break;
            }
            }
        }

    }
    func setProgramScheduleNotification(programModel:QRProgramModel) {
        let dateFormatter = NSDateFormatter()
        let calendar = NSCalendar.autoupdatingCurrentCalendar()
        dateFormatter.dateFormat = "dd MM yyyy"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        let currentDateString = dateFormatter.stringFromDate(NSDate())
        
        
        let timeString = self.convertTimeIn12HourFormat(programModel.startTime!)
        let dateString = "\(currentDateString) \(timeString)"
        print(dateString)
        // let dateString = "09 06 2016 11:24 AM"
        dateFormatter.timeZone = NSTimeZone.systemTimeZone()
        dateFormatter.dateFormat = "dd MM yyyy hh:mma"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        var scheduleTime = dateFormatter.dateFromString(dateString)
        scheduleTime = calendar.dateByAddingUnit(.Minute, value: -15, toDate: scheduleTime!, options:[])
        let daysToAdd = getDaysToAdd()
        
        
        scheduleTime = scheduleTime?.addDays(daysToAdd)
        if scheduleTime!.isLessThanDate(NSDate()){
            
          scheduleTime = scheduleTime?.addDays(7)
        } else{
            
            print("greater")
        }
        
        programModel.notificationDate = scheduleTime
        
        let localNotification:UILocalNotification = UILocalNotification()
        localNotification.alertAction = "name"
        localNotification.alertBody = programModel.programNameEnglish
        localNotification.soundName = UILocalNotificationDefaultSoundName
        //localNotification.repeatInterval = NSCalendarUnit.Day
        print(scheduleTime)
        var userInfo = [String:AnyObject]()
        userInfo["tag"] = 3
        userInfo["dayId"] = programModel.dayId
        userInfo["programmeId"] = programModel.programmeId
        localNotification.userInfo = userInfo
        localNotification.timeZone = NSTimeZone.systemTimeZone()
        localNotification.fireDate = scheduleTime
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        
        
    }

    func getDaysToAdd() -> Int {
        var daysToAdd = 0
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            if self.selectedDay < currentDay{
                daysToAdd = currentDay - self.selectedDay
            } else if self.selectedDay > currentDay{
                daysToAdd = 7 + currentDay - self.selectedDay
            }
        }else{
            if self.selectedDay < currentDay{
                daysToAdd = 7 - currentDay + self.selectedDay
            } else if self.selectedDay > currentDay{
                daysToAdd =  self.selectedDay - currentDay
            }
        }
        return daysToAdd

    }
    func reminderButtonAction(sender: UIButton) {
        let  cell = sender.superview?.superview as! QRProgramScheduleCell
        let  indexPath = tableView.indexPathForCell(cell)
        let programModel: QRProgramModel = self.schedulesFromService[(indexPath?.row)!] as! QRProgramModel
        programModel.dayId = commonDayId
        
        let program = ProgramScheduleManager.alreadyAddedProgramDetails(programModel.dayId!,programmeID: programModel.programmeId!)
        if( program != nil)
        {
            removeProgramScheduleNotification(programModel)
            
            showAlertWithMessage(Localization("k_Reminder_Removed"), andTitle: "")
            
        }
        else
        {
            self.setProgramScheduleNotification(programModel)
            ProgramScheduleManager.addProgramShchedule(programModel)
            
            showAlertWithMessage(Localization("k_Reminder_Added"), andTitle: "")
        }
        self.tableView.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: UITableViewRowAnimation.None)
        

        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
