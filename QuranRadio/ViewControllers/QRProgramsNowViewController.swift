//
//  QRProgramsNowViewController.swift
//  QuranRadio
//
//  Created by daffolapmac on 17/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import AVFoundation

class QRProgramsNowViewController: QRBaseViewViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ImageViewForBanner: UIImageView!
    private  var chkBtnPressed:Int = 0
    private  var isPlaying = false
    private var selectedRowSet:Set<Int> = [-1]
    var archive_id:String = ""
    var bannerImage: UIImage!
    var programName: String!
    var allProgramList: Array<QRArchiveModel> = []
    var shouldProceed = true
    var currentPage = 1

    var bannerImageUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        setMultilineNavigationTitle()
        setBackButton()
        setBackButtonLayout()
        self.initTableView()
        self.callWebServiceToGetArchiveData()
        

    }
   
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        ImageViewForBanner.sd_setImageWithURL(NSURL(string: bannerImageUrl), placeholderImage: UIImage(named: "ic_program_placeholder"))
        ImageViewForBanner.contentMode = .Center
        ImageViewForBanner.clipsToBounds = true
   }
    
    override func viewWillDisappear(animated: Bool) {
        
        QRAudioPlayer.sharedInstance.stop()
        self.tableView.reloadData()
        self.selectedRowSet.removeAll()
        UserDefault.removeObjectForKey("SongDuration")
        UserDefault.setObject(false, forKey: "k_song_status")
        
    }
    
    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func setMultilineNavigationTitle(){
        let label = UILabel(frame: CGRectMake(0, 0, 400, 44))
        label.backgroundColor = UIColor.clearColor()
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.Center
        label.text = programName.uppercaseString
        self.navigationItem.titleView = label

    }
    //MARK:- Initialization methods
    
    private func initTableView(){
        
        self.tableView.tableFooterView = UIView.init(frame: CGRectZero)
        self.tableView.enablePullToRefreshWithTarget(self, selector: #selector(QRProgramsNowViewController.pullToRefresh))
        
       
    }
    
    func pullToRefresh()
    {
        QRAudioPlayer.sharedInstance.pause()
        self.selectedRowSet.removeAll()
        UserDefault.removeObjectForKey("SongDuration")
        self.tableView.reloadData()
        
        self.shouldProceed = true
        self.currentPage = 1
        self.callWebServiceToGetArchiveData()
    }
    //MARK:- BL methods
    
    func footerRadioButtonTapped()
    {
        self.isPlaying = false
        self.tableView.reloadData()
    }
    
    /**
     Function to set layout for selected cell
     
     - parameter cell:
     */
    func setLayoutForProgramsNowSelectedCell(cell: QRProgramsNowCell){
        cell.backgroundColor = ColorFromHexaCode(k_Bright_Red_Color)
        cell.songProgressLabel.hidden = false
        cell.dateLabel.textColor = UIColor.whiteColor()
        cell.songDurationLabel.hidden = false
        cell.progressBar.hidden = false
        cell.programNowLabel.textColor =  UIColor.whiteColor()

        
        if self.isPlaying
        {
            //ic_pause_red
            cell.playButton.setBackgroundImage(UIImage(named: "ic_stop"), forState: UIControlState.Normal)
            
            //cell.bindProgressData()
        }
        else
        {//ic_play_red
            cell.playButton.setBackgroundImage(UIImage(named: "ic_play_red"), forState: UIControlState.Normal)
        }
        
    }
    

    
    /**
     Function to set layout for unselected cell
     
     - parameter cell:
     */
    
    func setLayoutForProgramsNowUnSelectedCell(cell: QRProgramsNowCell){
        
        cell.backgroundColor = UIColor.whiteColor()
        cell.programNowLabel.textColor =  UIColor.blackColor()
        cell.songProgressLabel.hidden = true
        cell.songDurationLabel.hidden = true
        //cell.songProgressLabel.textColor = UIColor.blackColor()
        cell.progressBar.hidden = true
        cell.selectionStyle = .None
        cell.dateLabel.textColor = UIColor.blackColor()
        cell.playButton!.addTarget(self, action: #selector(QRProgramsNowViewController.playSongProgramsNow(_:)), forControlEvents: UIControlEvents.TouchDown)
        cell.playButton.setBackgroundImage(UIImage(named: "ic_play_red"), forState: UIControlState.Normal)
    }

    
    //MARK:- Audio related methods
    
    func playSongProgramsNow(sender:UIButton){
        
        let indexPath = NSIndexPath(forRow: sender.tag , inSection: 0)
        let cell =  self.tableView.cellForRowAtIndexPath(indexPath) as! QRProgramsNowCell
        
        if(!selectedRowSet.contains(sender.tag))
        {
            selectedRowSet.removeAll()
            selectedRowSet.insert(sender.tag)
            self.isPlaying = true
            cell.playButton.setBackgroundImage(UIImage(named: "ic_pause_red"), forState: UIControlState.Normal)
            self.tableView.reloadData()
            UserDefault.setObject(true, forKey: "k_song_status")
            QRAudioPlayer.sharedInstance.playAudioWithParentViewController(self, cell: cell, urlForSong: cell.url)
            
        }
        else
        {
            if let _ = QRAudioPlayer.sharedInstance.player
            {
                if self.isPlaying {
                    self.isPlaying = false
                    QRAudioPlayer.sharedInstance.pause()
                    cell.playButton.setBackgroundImage(UIImage(named: "ic_play_red"), forState: UIControlState.Normal)
                    UserDefault.setObject(false, forKey: "k_song_status")
                    
                } else {
                    QRAudioPlayer.sharedInstance.play()
                    self.isPlaying = true
                    cell.playButton.setBackgroundImage(UIImage(named: "ic_stop"), forState: UIControlState.Normal)
                    UserDefault.setObject(true, forKey: "k_song_status")
                }
            }}
        
    }//End of play song function definition
    



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func chkPlaying(mynotification: NSNotification){
        
        let stoppedPlayerItem:AVPlayerItem = mynotification.object as! AVPlayerItem
        QRAudioPlayer.sharedInstance.play()
        NSNotificationCenter.defaultCenter().removeObserver(stoppedPlayerItem)
    }
    
    
    func finishedPlaying(mynotification: NSNotification){
        
        let stoppedPlayerItem:AVPlayerItem = mynotification.object as! AVPlayerItem
        stoppedPlayerItem.seekToTime(kCMTimeZero)
        NSNotificationCenter.defaultCenter().removeObserver(QRAudioPlayer.sharedInstance.playerItem)
        self.tableView.reloadData()
        self.selectedRowSet.removeAll()
    }
    
    //MARK:- Service methods
    
    func callWebServiceToGetArchiveData()
    {
        if !shouldProceed
        {
            return
        }
        
        if(!self.tableView.refreshControl!.refreshing)
        {
            self.view.showLoader()
        }
        
        
        let requestData = [
            "program_archive_id": "\(self.archive_id)",
            "page":"\(self.currentPage)"
        ]
        if NetworkConnectivity.isConnectedToNetwork()
        {
            
            
        QRAppServices.performPostRequest(QRServiceUrls.getCompleteUrlFor(k_Url_Program_Archive_List), requestData: requestData, successBlock: { (request, response, headers) in
            self.view.hideLoader()
            if(self.tableView.refreshControl!.refreshing)
            {
                self.tableView.refreshControl?.endRefreshing()
                self.allProgramList.removeAll()
                
            }
            
            let responseData = response as! NSDictionary
            let dataArray = responseData["data"] as! NSArray
            
            if dataArray.count == 0 || dataArray.count < 8
            {
                self.shouldProceed = false
                self.view.hideLoader()
                
            }
            for (program) in dataArray
            {
                self.allProgramList.append(QRArchiveModel.init(fromDictionary: program as! NSDictionary))
            }
            
            self.tableView.reloadData()
            self.currentPage += 1
            
            
        }) { (errorMessage, response, headers) in
            
            self.view.hideLoader()
            self.showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
        }
    } else{
    
    self.view.hideLoader()
    
    showAlertWithMessage(Localization("No_Internet_Connection"), andTitle: Localization("Error"))
    }

    }
    

}// End of Class Definition


//MARK:- UITableView methods

extension QRProgramsNowViewController: UITableViewDataSource , UITableViewDelegate,QRProgramsNowCellDelegate{
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (self.allProgramList.count > 0 || (currentPage == 1 && self.allProgramList.count == 0))
        {
            tableView.backgroundView?.hidden = true
        }
        else
        {
            tableView.backgroundView?.hidden = false
        }
        
        return self.allProgramList.count
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        //QRProgramsNowCell
        
        var reuseIdentifier = "QRProgramsNowEnglish"
        if Localisator.sharedInstance.currentLanguage == "Arabic_ar"{
            reuseIdentifier = "QRProgramsNowArabic"
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! QRProgramsNowCell
        cell.delegate = self
        let dateString = allProgramList[indexPath.row].date
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateObj = dateFormatter.dateFromString(dateString)
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        cell.url = self.allProgramList[indexPath.row].mp3
        cell.dateLabel.text = dateFormatter.stringFromDate(dateObj!)
        if(selectedRowSet.contains(indexPath.row)){
            self.setLayoutForProgramsNowSelectedCell(cell)
            
        }else{
            self.setLayoutForProgramsNowUnSelectedCell(cell)
        }
        cell.playButton!.tag = indexPath.row
        cell.programNowLabel.text = self.allProgramList[indexPath.row].title;
        
        return cell
    }
     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.row == self.allProgramList.count - 1 && !tableView.refreshControl!.refreshing)
        {
            self.callWebServiceToGetArchiveData()
            
        }
        // Remove separator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }

    }
    
    //MARK:- QRProgramsNowCellDelegate Method
    
    func updateSongProgressInfo(progressTime: String, progress: Float) {
    
        if selectedRowSet.count == 0{
            return
        }
    
        let indexPath = NSIndexPath(forRow: selectedRowSet[selectedRowSet.startIndex.advancedBy(0)], inSection: 0)
        let cell = tableView.cellForRowAtIndexPath(indexPath) as? QRProgramsNowCell
        
        if cell != nil{
            cell!.songProgressLabel.text = progressTime
            cell?.progressBar.progress = progress
            if UserDefault.objectForKey(k_Song_Duration) != nil{
            cell!.songDurationLabel.text = UserDefault.objectForKey(k_Song_Duration) as? String
                
                
                
            }
            
        }
        
        
    }
}


