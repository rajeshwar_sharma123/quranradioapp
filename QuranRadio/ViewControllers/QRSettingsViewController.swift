//
//  QRSettingsViewController.swift
//  QuranRadio
//
//  Created by Jenkins_New on 6/23/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRSettingsViewController: QRBaseViewViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var lblVersionName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var upperLineView: UIView!
   
    var currentLanguage: String!
    
    //MARK:- LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

//        upperLineView.frame = CGRectMake(0, 0, screenWidth, 0.5)
        
        self.navigationController!.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "CPDFontAL", size: 18)!]
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.bounces = false
        
        setBackButton()
        setScreenTitle()
    
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        currentLanguage = QRUtilities.getPrefferedLanguage()
        tableView.reloadData()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setScreenTitle()
    {
        self.title = Localization("k_Settings_Title")
        let versionNumber = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String

        lblVersionName.text = " \(Localization("k_Version_Name")) \(versionNumber)"
        setBackButtonLayout()
    }
    
   //MARK:- IBAction Methods
    @IBAction func switchTapped(sender: AnyObject)
    {
        if(sender.tag == 0) //language changed
        {
            let languages = Localisator.sharedInstance.getArrayAvailableLanguages()
            
            if Localisator.sharedInstance.currentLanguage == "English_en"
            {
                SetLanguage(languages[2])
                QRUtilities.setPrefferedLanguage(Arabic)
                
                currentLanguage = Arabic
            }
            else{
                SetLanguage(languages[1])
                QRUtilities.setPrefferedLanguage(English)
                
                currentLanguage = English
            }
            
            setScreenTitle()
            setLocalnotificationAlertMessageOnLanguageBasis()
        }
        else if(sender.tag == 1) //Notification switch tapped
        {
            azanAudioSwitchTapped()
        }
        
        tableView.reloadData()
    }
    
    
    func setLocalnotificationAlertMessageOnLanguageBasis(){
    /*    let app:UIApplication = UIApplication.sharedApplication()
        for oneEvent in app.scheduledLocalNotifications! {
            let notification = oneEvent as UILocalNotification
            let userInfoCurrent = notification.userInfo! as! [String:AnyObject]
            let prayerNameTmp =  userInfoCurrent["prayerName"] as! String
            let prayerTag  =  userInfoCurrent["tag"] as! Int
            print(prayerTag)
            print(prayerNameTmp)
            if  prayerTag == 1 {
                if #available(iOS 8.2, *) {
                    if Localisator.sharedInstance.currentLanguage == "English_en"{
                        notification.alertTitle = "Dubai Quran Radio"
                    }else{
                        notification.alertTitle = "إذاعة دبي للقرآن الكريم"
                    }
                    
                }
                notification.alertBody = getLocalNotificationAlertMessage(prayerNameTmp)
            
            }
        }*/
        
        let switchValue = NSUserDefaults.standardUserDefaults().objectForKey("switchValue") as! String
        if(switchValue == "0")
        {
            return;
        }
        
        let scheduledNotifications = UIApplication.sharedApplication().scheduledLocalNotifications!
        if(scheduledNotifications.count > 0)
        {
            for notification in  scheduledNotifications
            {
                let dict = notification.userInfo! as NSDictionary
                
                
                if dict.valueForKey("tag") as! Int != 3 {
                    UIApplication.sharedApplication().cancelLocalNotification(notification)
                    
                }
                
            }
            
        }
       self.scheduleLocalNotificationForPrayerTimingOnSwitchChanged()

    }
    
    
    
    func azanAudioSwitchTapped()
    {
        let switchValue = NSUserDefaults.standardUserDefaults().objectForKey("switchValue") as! String
        if(switchValue == "1")
        {
            let scheduledNotifications = UIApplication.sharedApplication().scheduledLocalNotifications!
            if(scheduledNotifications.count > 0)
            {
                for notification in  scheduledNotifications
                {
                    let dict = notification.userInfo! as NSDictionary
                    
                        
                        if dict.valueForKey("tag") as! Int != 3 {
                           UIApplication.sharedApplication().cancelLocalNotification(notification)
                            
                        }
                    
                }
              
            }
            
            NSUserDefaults.standardUserDefaults().setObject("0", forKey: "switchValue")
        }
        else
        {
        
            NSUserDefaults.standardUserDefaults().setObject("1", forKey: "switchValue")
            self.scheduleLocalNotificationForPrayerTimingOnSwitchChanged()
        }
        
        tableView.reloadData()
    }
    
    /************ scheduling local notification on basis of notification toggel button **********/
    func scheduleLocalNotificationForPrayerTimingOnSwitchChanged()
    {
        if NSUserDefaults.standardUserDefaults().boolForKey("isNotificationOn") == true{
            
            let dateFormatter = NSDateFormatter()
            let calendar = NSCalendar.autoupdatingCurrentCalendar()
            dateFormatter.dateFormat = "dd MM yyyy"
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
            let currentDateString = dateFormatter.stringFromDate(NSDate())
            for index in 0 ..< globalPrayer.prayerArray.count{
                
                let objDate = globalPrayer.prayerArray[index]
                let nameString = objDate.prayerName
                if nameString != "Sunset"{
                    let objDate = globalPrayer.prayerArray[index]
                    let timeString = objDate.PrayerTime
                    let nameString = objDate.prayerName
                    if nameString != "Sunrise"{
                    
                    sechedulNotificationForAlarm(nameString, prayerTime: timeString)
                  
                        
                /*    let dateString = "\(currentDateString) \(timeString)"
                    dateFormatter.timeZone = NSTimeZone.systemTimeZone()
                    dateFormatter.dateFormat = "dd MM yyyy hh:mm a"
                    dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
                    var scheduleTime = dateFormatter.dateFromString(dateString)
                    scheduleTime = calendar.dateByAddingUnit(.Minute, value: -3, toDate: scheduleTime!, options:[])
                    let localNotification:UILocalNotification = UILocalNotification()
                    
                        if #available(iOS 8.2, *) {
                            if Localisator.sharedInstance.currentLanguage == "English_en"{
                                localNotification.alertTitle = "Dubai Quran Radio"
                            }else{
                                localNotification.alertTitle = "إذاعة دبي للقرآن الكريم"
                            }
                            
                        } else {
                            // Fallback on earlier versions
                        }
                        
                    localNotification.alertBody = getLocalNotificationAlertMessage(nameString)
                    localNotification.repeatInterval = NSCalendarUnit.Day
                    localNotification.soundName = UILocalNotificationDefaultSoundName
                    var userInfo = [String:AnyObject]()
                    let keyValue = 1
                    userInfo["tag"] = keyValue
                    userInfo["prayerName"] = nameString
                    userInfo["prayerTime"] = timeString
                    localNotification.userInfo = userInfo
                    localNotification.timeZone = NSTimeZone.systemTimeZone()
                    localNotification.fireDate = scheduleTime
                    UIApplication.sharedApplication().scheduleLocalNotification(localNotification)*/
                    }
                }
            }
        }
    }
    

    

    //MARK:- UITableView Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var reuseIdentifier = ""
        
        if(currentLanguage == English)
        {
            reuseIdentifier = "QRSettingsTableViewCell"
        }
        else
        {
            reuseIdentifier = "QRSettingsArabicTableViewCell"
        }
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) as! QRSettingsTableViewCell
        cell.bindDataWithIndexPath(indexPath, currentLanguage: currentLanguage)
      
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
                   forRowAtIndexPath indexPath: NSIndexPath)
    {
        // Remove separator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }

}
