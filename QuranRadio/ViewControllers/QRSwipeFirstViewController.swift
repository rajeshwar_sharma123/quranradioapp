//
//  QRSwipeFirstViewController.swift
//  QuranRadio
//
//  Created by daffolapmac on 22/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRSwipeFirstViewController: UIViewController {
    @IBOutlet var lblAzkaar: QRLabel!
    @IBOutlet var lblHolyQuran: QRLabel!
    @IBOutlet var lblMosqueNearby: QRLabel!
    @IBOutlet weak var lblPrayer_Time: QRLabel!
    
    @IBOutlet weak var imgPrayer: UIImageView!
    
    @IBOutlet weak var imgMosque: UIImageView!
    @IBOutlet weak var imgQuran: UIImageView!
    @IBOutlet weak var imgAzkaar: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureViewFromLocalisation()
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(QRSwipeFirstViewController.receiveLanguageChangedNotification(_:)), name: kNotificationLanguageChanged, object: nil)
    }
    // MARK: - Notification methods
    func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
            
        }
    }
    func configureViewFromLocalisation() {
       
        lblPrayer_Time.text = Localization("Prayer_Timing")
        lblMosqueNearby.text = Localization("Mosques_Nearby_Home")
        lblHolyQuran.text = Localization("Holy_Quran")
        lblAzkaar.text = Localization("k_Azkaar_Home")
        
    }

    
    // MARK: Custom Button Action
    
    @IBAction func pdfReaderButtonAction(sender: AnyObject) {
        
         pushPopAnimation(self.imgQuran, identifierName: "QRPdfReaderViewController")
        
        
    }
    
    
    @IBAction func mapButtonAction(sender: AnyObject) {
        
        pushPopAnimation(self.imgMosque, identifierName: "QRGooglePlacesViewController")
        
        
        
    }
    
    @IBAction func prayerButtonAction(sender: AnyObject) {
        
        
        
        
        
        pushPopAnimation(self.imgPrayer, identifierName: "QRPrayerListViewController")
        
        
        
        
        
       
    }
    @IBAction func azkaarButtonAction(sender: AnyObject) {
        
       
        pushPopAnimation(self.imgAzkaar, identifierName: "QRAzkarViewController")
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pushPopAnimation( imageView:UIView, identifierName:String){
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            
            
            
            imageView.center.x = imageView.center.x + 4
            imageView.center.y = imageView.center.y + 4
            imageView.frame.size.height = imageView.frame.size.height - 10
            imageView.frame.size.width = imageView.frame.size.width - 10
            
            
        }) { (Bool) -> Void in
            
            
            let prayerVC = (self.storyboard?.instantiateViewControllerWithIdentifier(identifierName))! as UIViewController
            self.navigationController?.pushViewController(prayerVC, animated: true)
            
            
            
            UIView.animateWithDuration(0.1 , animations: { () -> Void in
                imageView.center.x = imageView.center.x - 4
                imageView.center.y = imageView.center.y - 4
                imageView.frame.size.height = imageView.frame.size.height + 10
                imageView.frame.size.width = imageView.frame.size.width + 10
            }){ (Bool) -> Void in
                
            }
            
            
            
            
        }
    }

}
