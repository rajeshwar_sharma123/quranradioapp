//
//  QRSwipeSecondViewController.swift
//  QuranRadio
//
//  Created by daffolapmac on 22/06/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit

class QRSwipeSecondViewController: UIViewController {
    @IBOutlet var lblSchedule: QRLabel!
    @IBOutlet var lblArchive: QRLabel!
    @IBOutlet var lblAboutUs: QRLabel!
    @IBOutlet weak var lblSetting: QRLabel!
    
    
    @IBOutlet weak var imgSchedule: UIImageView!
    
    @IBOutlet weak var imgArchive: UIImageView!
    @IBOutlet weak var imgAboutUs: UIImageView!
    
    @IBOutlet weak var imgSetting: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureViewFromLocalisation()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(QRSwipeFirstViewController.receiveLanguageChangedNotification(_:)), name: kNotificationLanguageChanged, object: nil)
    }
    
    // MARK: - Notification methods
    func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
            
        }
    }
    func configureViewFromLocalisation() {
        
        lblSchedule.text = Localization("k_Schedule_Home")
        lblArchive.text = Localization("k_Archive_Home")
        lblAboutUs.text = Localization("About_Us_Home")
        lblSetting.text = Localization("k_Settings_Home")
        
       
        
    }

    // MARK: Custom Button Action
    
    @IBAction func scheduleButtonAction(sender: AnyObject) {
        
        pushPopAnimation(imgSchedule, identifierName: "QRProgrammeScheduleViewController")
        
    }
    
    
    @IBAction func archiveButtonAction(sender: AnyObject) {
        
        pushPopAnimation(imgArchive, identifierName: "QRArchiveViewController")
        
       

        
    }
    
   
    @IBAction func aboutUsButtonAction(sender: AnyObject) {
        
        if Localisator.sharedInstance.currentLanguage != "English_en"{
            
            
            pushPopAnimation(imgAboutUs, identifierName: "QRAboutUsViewController")
            
           
        } else {
            
            pushPopAnimation(imgAboutUs, identifierName: "QRAboutUsEngViewController")
            
           
        }
        
    }
    @IBAction func settingButtonAction(sender: AnyObject) {
        
        
         pushPopAnimation(imgSetting, identifierName: "QRSettingsViewController")
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func pushPopAnimation( imageView:UIView, identifierName:String){
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            
            
            
            imageView.center.x = imageView.center.x + 4
            imageView.center.y = imageView.center.y + 4
            imageView.frame.size.height = imageView.frame.size.height - 10
            imageView.frame.size.width = imageView.frame.size.width - 10
            
            
        }) { (Bool) -> Void in
            
            
            let prayerVC = (self.storyboard?.instantiateViewControllerWithIdentifier(identifierName))! as UIViewController
            self.navigationController?.pushViewController(prayerVC, animated: true)
            
            
            
            UIView.animateWithDuration(0.1 , animations: { () -> Void in
                imageView.center.x = imageView.center.x - 4
                imageView.center.y = imageView.center.y - 4
                imageView.frame.size.height = imageView.frame.size.height + 10
                imageView.frame.size.width = imageView.frame.size.width + 10
            }){ (Bool) -> Void in
                
            }
            
            
            
            
        }
    }

}
